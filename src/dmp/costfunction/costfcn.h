/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DMP_COSTFUNCTION_H
#define DMP_COSTFUNCTION_H

#include "dmp/general/helpers.h"

#include <dmp/general/vec.h>

#include <map>
#include <utility>
#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

#include <dmp/representation/trajectory.h>

namespace DMP
{
class CostFunction;
typedef boost::shared_ptr<CostFunction> CostFunctionPtr;

class CostFunction
{
public:
    CostFunction()
    {
        hist = make_pair(0, DVec2d());
    }
    CostFunction(const double& quadTerm):
        R(quadTerm)
    {
        hist = make_pair(0, DVec2d());
    }

    virtual double getStateDependentCost(double timestamp, const DVec2d& x)
    {
        double cost = 0;

        if(hist.first != 0)
        {

            DVec histStates = traj.getStates(hist.first, 0);
            DVec states = traj.getStates(timestamp,0);

            for(size_t i = 0; i < states.size(); ++i)
            {
                double diff = states[i] - histStates[i];
                double curDiff = x[i][0] - hist.second[i][0];
                cost += (diff - curDiff) * (diff - curDiff);
            }

            cost = sqrt(cost);
        }

        hist = make_pair(timestamp, x);
        return cost;
    }

    virtual double getTerminalCost(const DVec2d& x, const DVec& goal)
    {
        double dist = 0;

        for(size_t i = 0; i < goal.size(); ++i)
        {
            dist += x[i][1] * x[i][1] + (x[i][0] - (goal[i]))*(x[i][0] - (goal[i]));
        }

        return sqrt(dist);

        //            return 0;
    }

    virtual double getQuadPenalty(const Eigen::VectorXf& theta) {return theta.transpose() * theta;}

    virtual double getCost(const DVec& timestamps, const DVec& goal, double T, const DMP::SampledTrajectoryV2& traj);

//    virtual double getManiCost(MMM::MotionPtr motion, const std::string& kcname, const DVec& goal, double T);

    double R;

    SampledTrajectoryV2 traj;
    std::pair<double, DVec2d> hist;
private:

};
}

#endif
