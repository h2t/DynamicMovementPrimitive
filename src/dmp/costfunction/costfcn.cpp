/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/
#include "costfcn.h"

#include <dmp/general/exception.h>

#ifdef SIMOX_FOUND
#include "VirtualRobot/IK/PoseQualityExtendedManipulability.h"
#include "VirtualRobot/Workspace/Manipulability.h"
#endif
using namespace DMP;

double CostFunction::getCost(const DVec& timestamps, const DVec& goal, double T, const DMP::SampledTrajectoryV2& traj)
{
    double cost = 0;
    hist = make_pair(0,DVec2d());
    for(double timestamp : timestamps)
    {
        DVec states = traj.getStates(timestamp,0);
        DVec velstates = traj.getStates(timestamp,1);
        DVec2d x;
        x.resize(states.size());
        for(size_t i = 0; i < x.size(); ++i)
        {
            x[i].resize(2);
            x[i][0] = states[i];
            x[i][1] = velstates[i];
        }

        if(timestamp < T)
        {
            cost += getStateDependentCost(timestamp, x);
        }
        else
        {
            cost += getTerminalCost(x, goal);
        }
    }

    return cost;
}

//#ifdef MMM_FOUND
//double CostFunction::getManiCost(MMM::MotionPtr motion, const std::string& kcname, const DVec& goal, double T)
//{
//    VirtualRobot::RobotPtr robot = MMM::SimoxTools::buildModel(motion->getModel(),false);
//    VirtualRobot::RobotNodeSetPtr robotNodeSet =
//            VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, "robotnodeset", motion->getJointNames(), "", "", true);

//    double cost = 0;
//    for(size_t i = 0; i < motion->getNumFrames(); ++i)
//    {
//        MMM::MotionFramePtr frame = motion->getMotionFrame(i);
//        robot->setGlobalPose(frame->getRootPose(),true);
//        robotNodeSet->setJointValues(frame->joint);

//        VirtualRobot::RobotNodeSetPtr kc = robot->getRobotNodeSet(kcname);

//        if(frame->timestep < T)
//        {
//            VirtualRobot::PoseQualityManipulabilityPtr measure(new VirtualRobot::PoseQualityManipulability(kc));
////            VirtualRobot::PoseQualityExtendedManipulabilityPtr measure(new VirtualRobot::PoseQualityExtendedManipulability(kc));
//            double maniIndex = measure->getPoseQuality();

//            std::cout << "maniIndex: " << maniIndex << std::endl;
//            cost += 1 - maniIndex;
//        }

//    }

//    return cost;
//}
//#endif
