/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DMP_PCA_H
#define DMP_PCA_H

#include <vector>

#include <Eigen/Core>
#include <Eigen/Eigen>

namespace DMP {

    class PCA
    {
    public:
        typedef std::pair<double, int> ValueAndIndex;
        typedef std::vector<ValueAndIndex> ValuesAndIndices;
        /**
         * @brief PCA
         * @param data rows = data's dimensions, cols = number of points
         */
        PCA(const Eigen::MatrixXd &dataPoints);

        ValuesAndIndices getXBestDimensions(int x = -1);
        const Eigen::EigenSolver<Eigen::MatrixXd>::EigenvalueType &getEigenValues() const;
        Eigen::EigenSolver<Eigen::MatrixXd>::EigenvectorsType getEigenVectors();
        /**
         * @brief computes the PCA for the given set of data points
         * @param data rows = data's dimensions, cols = number of points
         */
        void compute(const Eigen::MatrixXd& dataPoints);
    protected:
        Eigen::MatrixXd data;
        Eigen::EigenSolver<Eigen::MatrixXd> solver;
        ValuesAndIndices vi;
    };

} // namespace DMP

#endif // DMP_PCA_H
