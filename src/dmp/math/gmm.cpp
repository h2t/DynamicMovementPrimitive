#include "gmm.h"

using namespace DMP;
using namespace Dist;

GaussianMixtureModel::GaussianMixtureModel(int n_comp, int n_dim, double scaling)
{
    K = n_comp;
    dim = n_dim;

    for(int k= 0; k < K; ++k)
    {
        mc.push_back(1.0/(double)K);
        GaussianPtr gc(new Gaussian(scaling * randn(dim), eye(dim, dim)));
        gclist.push_back(gc);
    }
}

GaussianMixtureModel::GaussianMixtureModel(int n_comp, int n_dim, vec scalingVec)
{
    K = n_comp;
    dim = n_dim;

    for(int k= 0; k < K; ++k)
    {
        mc.push_back(1.0/(double)K);
        vec mu = scalingVec % randn(dim);
        GaussianPtr gc(new Gaussian(mu, eye(dim, dim)));
        gclist.push_back(gc);
    }
}

void GaussianMixtureModel::runKmeans(const mat &X, int max_iter)
{
    mat means;

    mat data = X.t();
    bool status = kmeans(means, data, K, random_subset, max_iter, true);
    if(!status)
    {
        std::cout << "Kmeans failed" << std::endl;
    }

    for(int k = 0; k < K; ++k)
    {
        gclist[k]->mu = means.col(k);
    }
}

void GaussianMixtureModel::runEM(const mat &X, int max_iter)
{
    int i = 0;

    double lldiff = 100;
    double oldll = calcDataLogLikelihood(X);
    int n_data = X.n_rows;
    mat data = X.t();
    mat gamma = zeros<mat>(n_data, K);
    while(i < max_iter && lldiff > 1e-5)
    {
        // E-step
        for(int j = 0; j < n_data; j++)
        {
            vec x = data.col(j);
            double de = 0;
            for(int l = 0; l < K; l++)
            {
                de += mc[l] * gclist[l]->evaluate(x);
            }

            for(int k = 0; k < K; k++)
            {
                double nu = mc[k] * gclist[k]->evaluate(x);
                gamma(j,k) = nu / de;
            }
        }
        // M-step
        rowvec Ms = sum(gamma);

        for(int k = 0; k < K; k++)
        {
            mc[k] = Ms[k] / (double)n_data;

            vec mu_s = zeros<vec>(dim);
            mat Sig_s = zeros<mat>(dim, dim);
            for(int j = 0; j < n_data; j++)
            {
                mu_s = mu_s + gamma(j,k) * data.col(j);

            }
            mu_s = mu_s / Ms[k];

            for(int j = 0; j < n_data; j++)
            {
                Sig_s = Sig_s + gamma(j,k) * (data.col(j) - mu_s) * (data.col(j) - mu_s).t();
            }
            Sig_s = Sig_s / Ms[k];

            gclist[k]->mu = mu_s;
            gclist[k]->Sig = Sig_s;
        }

        // calculate likelihood
        double ll = calcDataLogLikelihood(X);
        lldiff = fabs(ll - oldll);
        oldll = ll;

        std::cout << "iter: " << i << " likelihood: " << ll << std::endl;
        i++;
    }
}

void GaussianMixtureModel::runArmaEM(const mat &X, int max_kmeans_iter, int max_em_iter)
{
    gmm_full model;
    bool status = model.learn(X.t(), K, eucl_dist, random_subset, max_kmeans_iter, max_em_iter, 1e-10, true);
    if(!status)
    {
        std::cout << "runArmaEM failed" << std::endl;
    }

    from_arma_gmm(model);

}

double GaussianMixtureModel::evaluate(const vec &x)
{
    if(!is_valid())
    {
        std::cerr << "not a valid GMM: (please check the size of the components)";
    }

    double sum_gauss = 0;
    for(int i = 0; i < K; ++i)
    {
        sum_gauss += gclist[i]->evaluate(x) * mc[i];
    }

    return sum_gauss;
}

std::vector<vec> GaussianMixtureModel::sample(int num)
{
    std::vector<int> sortedInd;
    std::vector<double> sortedmc = sortWithIndex(mc, sortedInd);

    std::vector<vec > res;
    for(int i = 0; i < num; ++i)
    {
        double rn = randu<double>();

        double cumSum = 0;
        int ind = 0;
        for(size_t k = 0; k < sortedmc.size(); ++k)
        {
            cumSum += sortedmc[k];
            if(rn <= cumSum)
            {
                ind = sortedInd[k];
                break;
            }
        }

        GaussianPtr gc = gclist[ind];
        res.push_back(gc->sample()[0]);
    }

    return res;
}

void GaussianMixtureModel::print()
{
    std::cout << "===> GMM,  K:  " << K << std::endl;
    for(int i = 0; i < K; ++i)
    {
        std::cout << i << "-th component: " << std::endl;
        std::cout << "mu: " << gclist[i]->mu.t() << std::endl;
//        std::cout << "Sig: " << gclist[i]->Sig << std::endl;
    }
}

double GaussianMixtureModel::calcDataLogLikelihood(const mat &X)
{
    mat data = X.t();
    double sum_log = 0;
    for(int i = 0; i < data.n_cols; ++i)
    {
        vec x = data.col(i);
        sum_log += calcLogLikelihood(x);
    }

    return sum_log / data.n_cols;
}

double GaussianMixtureModel::calcLogLikelihood(const vec &x)
{
    if(!is_valid())
    {
        std::cerr << "not a valid GMM: (please check the size of the components)";
    }

    double sum_gauss = 0;
    for(int i = 0; i < K; ++i)
    {
        sum_gauss += gclist[i]->evaluate(x) * mc[i];
    }
    return log(sum_gauss);
}

bool GaussianMixtureModel::is_valid()
{
    bool is_valid = ((int)mc.size() == K && (int)gclist.size() == K);
    return is_valid;
}

std::vector<double> GaussianMixtureModel::sortWithIndex(const std::vector<double> &list, std::vector<int> &index)
{
    std::vector<std::pair<double, int> > toSort;
    for(size_t i = 0; i < list.size(); ++i)
    {
        toSort.push_back(std::make_pair(list[i], i));
    }

    std::sort(toSort.begin(), toSort.end(), GaussianMixtureModel::compare);
    index.clear();
    std::vector<double> res;
    for(size_t i = 0; i < toSort.size(); ++i)
    {
        index.push_back(toSort[i].second);
        res.push_back(toSort[i].first);
    }

    return res;
}

void GaussianMixtureModel::from_arma_gmm(const gmm_full &model)
{
    for(int k = 0; k < K; ++k)
    {
        gclist[k]->mu = model.means.col(k);
        gclist[k]->Sig = model.fcovs.at(k);
    }
}



