#ifndef DUALNUMBER_H
#define DUALNUMBER_H

#include <Eigen/Geometry>
#include <dmp/general/vec.h>
#include <dmp/general/helpers.h>

using namespace Eigen;

namespace DMP {
class DualQuaterniond
{
public:
    DualQuaterniond(){}

    DualQuaterniond(const Quaterniond &a, const Quaterniond &b);
    DualQuaterniond(const Vector3d& w, const Vector3d& v);
    DualQuaterniond(const DVec& data);
    DualQuaterniond(double rw, double rx, double ry, double rz, double tw, double tx, double ty, double tz);

    DualQuaterniond operator+ (const DualQuaterniond& op2) {
        return DualQuaterniond(r + op2.r, d + op2.d);
    }

    DualQuaterniond operator- (const DualQuaterniond& op2) {
        return DualQuaterniond(r - op2.r, d - op2.d);
    }

    DualQuaterniond operator* (const DualQuaterniond& op2){
        return DualQuaterniond(r * op2.r, r * op2.d + d * op2.r);
    }

    DualQuaterniond operator* (const double& scale){
        return DualQuaterniond(r * scale, d * scale);
    }

    bool operator== (const DualQuaterniond& op2) {
        if(r == op2.r && d == op2.d)
            return true;

        return false;
    }

    DualQuaterniond conjugate(){
        return DualQuaterniond(r.conjugate(), d.conjugate());
    }


    static DualQuaterniond dqlog(const DualQuaterniond &a){
        return DualQuaterniond(quatlog(a.r), a.r.conjugate() * a.d);
    }

    static DualQuaterniond dqlog(const DualQuaterniond &a, const DualQuaterniond &b){
        Quaterniond rp = quatlog(a.r * b.r);
        Quaterniond at = getTranslation(a);
        Quaterniond bt = getTranslation(b);
        Quaterniond dp = 0.5 * Quaterniond(0, at.x() + bt.x(), at.y() + bt.y(), at.z() + bt.z());
        return DualQuaterniond(rp,dp);
    }

    static DualQuaterniond dqexp(const DualQuaterniond &a){
        return DualQuaterniond(quatexp(a.r), quatexp(a.r) * a.d);
    }

    static DualQuaterniond dqexp(const DualQuaterniond &v, const DualQuaterniond &b){
        Quaterniond rp = quatexp(v.r) * b.r;
        Quaterniond vt = v.d;
        Quaterniond bt = getTranslation(b);
        Quaterniond tp = bt + vt;

        Quaterniond dp = 0.5 * tp * rp;
        return DualQuaterniond(rp, dp);
    }

    static Quaterniond getTranslation(const DualQuaterniond &a);
    Quaterniond r;
    Quaterniond d;
    Quaterniond t;

    DVec norm();
    DualQuaterniond normalize();

    bool isUnit();

    void print();
    static inline DualQuaterniond Identity() { return DualQuaterniond(Quaterniond::Identity(), Quaterniond(0,0,0,0)); }
    static inline DualQuaterniond Zero() { return DualQuaterniond(Quaterniond(0,0,0,0), Quaterniond(0,0,0,0)); }
};






}



#endif
