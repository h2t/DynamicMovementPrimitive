#ifndef GMM_H
#define GMM_H

#include <vector>

#include <Eigen/Core>
#include <Eigen/Eigen>
#include <armadillo>
#include <boost/shared_ptr.hpp>
#include "dist.h"
using namespace arma;

namespace DMP {
    class GaussianMixtureModel
    {
    public:
        GaussianMixtureModel(int n_comp, int n_dim, double scaling=1.0);
        GaussianMixtureModel(int n_comp, int n_dim, vec scalingVec);

        void runKmeans(const mat& X, int max_iter=10);
        void runEM(const mat& X, int max_iter=10);
        void runArmaEM(const mat& X, int max_kmeans_iter = 10, int max_em_iter=10);

        double evaluate(const vec& x);
        std::vector<vec > sample(int num=1);

        void print();

    protected:
        int K;
        int dim;
        std::vector<double> mc;
        std::vector<Dist::GaussianPtr > gclist;
        double calcDataLogLikelihood(const mat& X);
        double calcLogLikelihood(const vec& x);
        bool is_valid();
        std::vector<double> sortWithIndex(const std::vector<double>& list, std::vector<int>& index);
        static bool compare(std::pair<double, int> a, std::pair<double,int> b)
        {
            return (a.first > b.first);
        }

        void from_arma_gmm(const gmm_full& model);
    };

    class VariationalGaussianMixtureModel: GaussianMixtureModel
    {
    public:
        VariationalGaussianMixtureModel(int init_n_comp, int n_dim):GaussianMixtureModel(init_n_comp, n_dim){}

//        void runVariationalEM(const mat& X, int max_iter=1000);
//        double evaluate(const vec& x);

    private:


    };

} // namespace DMP

#endif // GMM_H
