/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef TRAJDATACONTAINER_H
#define TRAJDATACONTAINER_H

#include "systemstatus.h"

#include <boost/unordered_map.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>

namespace DMP
{
    //    struct TrajData
    //    {
    //        double timestamp;
    //        mutable Vec< SystemStatusPtr > data; // mutable so that it can be changed
    //    };

    //    // structs for indices
    //    struct TagTimestamp{};
    //    struct TagOrdered{};

    //    // Trajectory data container
    //    typedef boost::multi_index::multi_index_container<
    //    TrajData,
    //    boost::multi_index::indexed_by<
    //        boost::multi_index::hashed_unique<boost::multi_index::tag<TagTimestamp>, boost::multi_index::member<TrajData, double, &TrajData::timestamp> >,
    //        boost::multi_index::ordered_unique<boost::multi_index::tag<TagOrdered>, boost::multi_index::member<TrajData, double, &TrajData::timestamp> > // this index represents timestamp incremental order
    //        >
    //    > TrajDataContainer;
    //    typedef typename boost::multi_index::index<TrajDataContainer,TagTimestamp>::type timestamp_view;
    //    typedef typename boost::multi_index::index<TrajDataContainer,TagOrdered>::type ordered_view;
}

#endif // TRAJDATACONTAINER_H
