#ifndef DMPCONFIG_H
#define DMPCONFIG_H

#include <Eigen/Dense>
#include <dmp/representation/systemstatus.h>
#include <dmp/representation/viapoint.h>
#include <dmp/representation/trajectory.h>
#include <dmp/representation/dmp/dmpinterface.h>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
namespace DMP
{

class MPConfig
{
public:
    MPConfig(){}
    MPConfig(const std::string& configFile)
    {
        load(configFile);
    }

    MPConfig(DMPInterfacePtr dmpPtr)
    {
        getConfigFromMP(dmpPtr);
    }

    void save(const std::string& filename)
    {
        std::ofstream ofs(filename);
        boost::archive::xml_oarchive ar(ofs);
        ar << boost::serialization::make_nvp("vmpconfig", *this);
    }

    void load(const std::string& filename)
    {
        std::ifstream ifs(filename);
        boost::archive::xml_iarchive ai(ifs);
        ai >>  boost::serialization::make_nvp("vmpconfig", *this);
    }


    void getConfigFromMP(DMPInterfacePtr dmpPtr)
    {
        SampledTrajectoryV2 traj0 = dmpPtr->getTrainingTraj(0);
        Vec<DMPState > initialState;
        DVec goals;
        for(size_t i = 0; i < traj0.dim(); i++)
        {
            DMPState state;
            state.pos = traj0.begin()->getPosition(i) ;
            state.vel = 0;
            initialState.push_back(state);
            goals.push_back(traj0.rbegin()->getPosition(i));
        }

        this->initialState = initialState;
        this->goal = goals;
        this->dmpType = dmpPtr->getDMPType();
    }

    Vec<DMPState >  initialState;
    DVec goal;
    std::string motionName;
    std::string modelfilename;
    std::string nodeSetName;
    std::string dmpType;
    double timeDuration;
    std::map<double, DMP::DVec > viapoints;
private:

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int /*file_version*/)
    {
        ar& boost::serialization::make_nvp("dmpType", dmpType);
        ar& boost::serialization::make_nvp("motionName", motionName);
        ar& boost::serialization::make_nvp("nodeSetName", nodeSetName);
        ar& boost::serialization::make_nvp("modelfilename", modelfilename);
        ar& boost::serialization::make_nvp("initialState", initialState);
        ar& boost::serialization::make_nvp("goal",goal);
        ar& boost::serialization::make_nvp("timeDuration", timeDuration);
    }
};

typedef boost::shared_ptr<MPConfig> MPConfigPtr;

}




#endif
