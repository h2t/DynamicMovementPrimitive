/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "adaptive3rdorderdmp.h"

using namespace DMP;





Vec<_3rdOrderDMP>
AdaptiveGoal3rdOrderDMP::calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<_3rdOrderDMP> &initialStates, DVec& canonicalValues, double temporalFactor)
{
    DVec newCurrentGoals;

    if (currentGoals.size() == 0)
    {
        currentGoals = goal;
        adaptiveGoal = boost::shared_ptr<AdaptiveGoal>(new AdaptiveGoal(goal));
    }

    adaptiveGoal->setNewGoals(goal);
    adaptiveGoal->integrate(t, currentGoals, newCurrentGoals);
    currentGoals = newCurrentGoals;
    return DMP3rdOrder::calculateTrajectoryPoint(t, currentGoals, tInit, initialStates, canonicalValues, temporalFactor);
}




AdaptiveGoal::AdaptiveGoal(const DVec& initalGoals, float goalChangeFactor) :
    ODE(initalGoals.size(), 1e-2),
    goalChangeFactor(goalChangeFactor),
    goals(initalGoals)

{
}

void AdaptiveGoal::flow(double t, const DVec& x, DVec& out)
{
    if (out.size() != goals.size())
    {
        throw Exception("Number of goals and ODE size do not match: ") << goals.size() << "!=" <<  out.size();
    }

    for (size_t i = 0; i < goals.size(); ++i)
    {
        out.at(i) = goalChangeFactor * (goals.at(i) - x.at(i));
    }
}

void AdaptiveGoal::setNewGoals(const DVec& newGoals)
{
    if (newGoals.size() != goals.size())
    {
        throw Exception("Number of goals do not match: ") << goals.size() << "!=" <<  newGoals.size();
    }

    goals = newGoals;
}
