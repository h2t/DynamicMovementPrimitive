/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "basicdmp.h"

#include <dmp/general/exception.h>
#include <dmp/general/helpers.h>

#include <limits>

#include <segmentation/characteristicsegmentation.h>

#include <iostream>


using namespace DMP;




///////////////////////////////////////////////////////////////////////////
// Implementation
///////////////////////////////////////////////////////////////////////////


BasicDMP::BasicDMP(int kernelSize, double D, double tau) :
    K((D / 2) * (D / 2)),
    D(D)
{
    FunctionApproximationInterfacePtr approximater(new LWR<GaussKernel>(kernelSize));

    setFunctionApproximator(approximater);
    canonicalSystem.reset(new CanonicalSystem(tau));

    ODE::setEpsabs(1e-5);
    canonicalSystem->setEpsabs(1e-5);
    amplitudes.push_back(1.0);
}


void BasicDMP::learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories)
{
    if (trainingTrajectories.size() > 1)
    {
        throw Exception() << "this DMP can only be trained with one trajectory (but with several dimensions)";
    }

    if (trainingTrajectories.size() == 0)
    {
        return;
    }

    trainingTraj = trainingTrajectories;

    __trajDim = trainingTraj.at(0).dim();
    SampledTrajectoryV2& trainingTrajectory = trainingTraj[0];


    for(size_t i = 0; i < trainingTrajectory.dim(); i++)
    {
        if(training_startPositions.size() <= i)
            training_startPositions.push_back(trainingTrajectory.begin()->getPosition(i));
        else
            training_startPositions[i] = trainingTrajectory.begin()->getPosition(i);

        if(training_goals.size() <= i)
            training_goals.push_back(trainingTrajectory.rbegin()->getPosition(i));
        else
            training_goals[i] = trainingTrajectory.rbegin()->getPosition(i);
    }
                // normalize timestamps so that they start at 0 and end at 1
//    trainingTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0.0, 1.0);
    trainingTrajectory.differentiateDiscretly(2);
    DVec normTimestamps = trainingTrajectory.getTimestamps();

    Vec<DVec> uValuesOrig;
    canonicalSystem->integrate(normTimestamps, canonicalSystem->getInitValues(), uValuesOrig);
//    canonicalSystem->integrateWithEulerMethod(normTimestamps, canonicalSystem->getInitValues(), uValuesOrig);
    DVec uValues;
    DoubleMap uValuesPlotMap;

    for (unsigned int i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
        uValuesPlotMap[normTimestamps.at(i)] = uValuesOrig[i][0];
    }

    std::cout << "uvalus size: " << uValues.size() << std::endl;
    if (getShowPlots())
    {
        plot(uValuesPlotMap, "plots/canonical_system_%d.gp", true, 0);
    }

    unsigned int trajDim = trainingTrajectory.dim();
    SampledTrajectoryV2 traj;

    // guarantee the size of amplitudes
    while(amplitudes.size() < trajDim)
        amplitudes.push_back(1.0);

    for (unsigned int dim = 0; dim < trajDim; dim++)
    {
        DVec values = trainingTrajectory.getDimensionData(dim, 0);

        // guarantee scaling terms
//        if(amplitudes.size() <= dim || amplitudes[dim] == 0)
//        {
//            double y0 = values.at(0);
//            double yT = values.back();
//            double dy = yT - y0 + 1e-3;

//            if(amplitudes.size() <= dim)
//                amplitudes.push_back(1.0);
//            else
//                amplitudes[dim] = 1.0;
//        }


        DoubleMap perturbationForceSamples = calcPerturbationForceSamples(dim, uValues, trainingTrajectory);
        _trainFunctionApproximator(dim, perturbationForceSamples);

        if (getShowPlots())
        {
            DoubleMap learnedData;
            DVec perturbationSamples;
            DVec timestamps;
            for(auto & perturbationForceSample : perturbationForceSamples)
            {
                timestamps.push_back(perturbationForceSample.first);
                perturbationSamples.push_back(perturbationForceSample.second);
            }
            for (unsigned int i = 0; i < uValues.size(); i++)
            {
                learnedData[uValues[i]] = _getPerturbationForce(dim, uValues[i]);

            }
            traj.addDimension(timestamps, perturbationSamples);
            Vec<DoubleMap> curves;
            curves.push_back(perturbationForceSamples);
            curves.push_back(learnedData);
            plot(curves, "plots/learningData_%d.gp", true);
        }
    }
//    traj.removeDimension(6);
//    traj.removeDimension(5);
//    traj.removeDimension(4);
//    traj.removeDimension(3);
//    CharacteristicSegmentation seg(traj, 0.1, 6);
//    seg.calc();
//    seg.plot();

}



#define uInv(x) -log(x)*6.90776
//#define uInv(x) x


void BasicDMP::_trainFunctionApproximator(unsigned int trajDimIndex, const DoubleMap& perturbationForceSamples)
{
    FunctionApproximationInterfacePtr approximator = _getFunctionApproximator(trajDimIndex);
    DoubleMap perturbationForceSamplesInv;
    DoubleMap::const_iterator it = perturbationForceSamples.begin();

    // u(t) has a lot of values near uMin (e.g. 0.001) -> baaad for lwpr ->
    // use inverse of u(t) to distribute them regularly
    for (; it != perturbationForceSamples.end(); ++it)
    {
        perturbationForceSamplesInv[ uInv(it->first) ] = it->second;
    }

    DVec timestamps;
    DVec values;

    for (it = perturbationForceSamplesInv.begin(); it != perturbationForceSamplesInv.end(); ++it)
    {
        timestamps.push_back(it->first);
        values.push_back(it->second);
    }

    approximator->learn(timestamps, values);

    if (getShowPlots())
    {
        // evaluate quality of training data
        DoubleMap::iterator itInv = perturbationForceSamplesInv.begin();
        it = perturbationForceSamples.begin();
        DoubleMap errors;

        for (; it != perturbationForceSamples.end(); ++itInv, it++)
        {
            errors[it->first] = it->second - _getPerturbationForce(trajDimIndex, it->first) ;
        }

        plot(errors, "learningError.gp", true, 0.0);
    }
}




const SampledTrajectoryV2& BasicDMP::getTrainingTraj(unsigned int trajSampleIndex) const
{
    return trainingTraj.at(trajSampleIndex);

}



void BasicDMP::flow(double t, const DVec& x, DVec& out)
{

    double u = x.at(0);

    DVec canonicalState(1);
    canonicalState[0] = u;

    canonicalSystem->flow(t, canonicalState, out);
    double tau = canonicalSystem->getTau();

//    unsigned int dimensions = getTrainingTraj(0).dim();
    unsigned int dimensions = __trajDim;

    if (dimensions > goals.size())
    {
        std::cout << "goals size: " << goals.size() << std::endl;
        std::cout << "dimensions: " << dimensions << std::endl;
        throw Exception("There are less trajectory goals than dimensions");
    }

    if (dimensions > startPositions.size())
    {
        throw Exception("There are less trajectory start positions than dimensions");
    }

    while(dimensions > amplitudes.size())
    {
        amplitudes.push_back(1.0);
    }


    unsigned int offset = canonicalState.size();
    unsigned int curDim = 0;

    while (curDim < dimensions)
    {
        double force = _getPerturbationForce(curDim, u);
//        bool scaling = true;
//        double scaleFactor = 1.0;
//        std::map<unsigned int, bool>::iterator it = scalingActivated.find(curDim);

//        if (it != scalingActivated.end() && !it->second)
//        {
//            scaling = false;
//        }

//        if (scaling)
//        {
//            scaleFactor = (goals[curDim] - startPositions[curDim]);
////            force *= scaleFactor;
//        }

        double scaleFactor = amplitudes[curDim];
        if(automaticScaling)
        {
            double epsilon = 1e-5;
            double origDist = training_goals[curDim] - training_startPositions[curDim] + epsilon;
            double currDist = goals[curDim] - startPositions[curDim];
            scaleFactor = currDist / origDist;
        }

        unsigned int i = curDim * 2 + offset;

        double pos = x[i];
        double vel = x[i + 1];

        out[i++] = 1.0 / tau * (vel);


        out[i++] = 1.0 / tau * (K  * (goals[curDim] - pos) - (D  * vel) + scaleFactor * force * u);
//        std::cout << "t: " << t << " u: " << u << " pos: " << pos << " vel: " << vel << " force: " << force << " resulting Acc: " << out[i - 1] << std::endl;
        curDim++;
    }
}


double
BasicDMP::_getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const
{
    //    return 0;
    if (canonicalStates.at(0) == 0)
    {
        throw Exception("The canonical state must not be 0!");
    }

    return (*_getFunctionApproximator(trajDim))(uInv(canonicalStates.at(0))).at(0);
}





DoubleMap BasicDMP::calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj) const
{
    DoubleMap result;


    if (exampleTraj.size() == 0)
    {
        throw Exception("Sample traj is empty");
    }

    if (canonicalValues.size() != exampleTraj.size())
    {
        throw Exception("The amount of the canonicalValues do not match the exampleTrajectory");
    }

    // start position of example traj
    double x0 = exampleTraj.begin()->getPosition(trajDim);
    // end position of example traj
    double xT = exampleTraj.rbegin()->getPosition(trajDim);
    bool scaling = true;

//    if (xT == x0)
//    {
//        scaling = scalingActivated[trajDim] = false;
//    }

    double tau = canonicalSystem->getTau();

    DVec::const_iterator itCanon = canonicalValues.begin();
    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();

    int i = 0;

//    while(trajDim > amplitudes.size())
//    {
//        amplitudes.push_back(1.0);
//    }
//    FunctionApproximationInterfacePtr approximator = _getFunctionApproximator(trajDim);
//    approximator->createKernels(uInv(*canonicalValues.begin()), uInv(*canonicalValues.rbegin()));
//    approximator->setForgettingFactor(0.99);


    for (; itCanon != canonicalValues.end(); ++itCanon, ++i, itTraj++)
    {
        const double& pos = itTraj->getPosition(trajDim);
        const double& vel = itTraj->getDeriv(trajDim, 1);
        const double& acc = itTraj->getDeriv(trajDim, 2);
//        double scaleFactor = 1.0;


//        if (scaling)
//        {
//            scaleFactor =  (xT - x0);
//        }
        //        double value = (-K  * scaleFactor * (xT - pos) +  scaleFactor * D * vel  + tau * acc) / *itCanon;
        double value = (-K  * (xT - pos) +   D * vel  + tau * acc) / *itCanon;
//        value /= amplitudes[trajDim];
//        double value = (-K  * (xT - pos) +  D * vel  + tau * acc / scaleFactor) / *itCanon;
        checkValue(value);
        result[*itCanon] =  value;
        //         std::cout << *itCanon << "[" << result[*itCanon] << "] calc. from ( -" << K << " * ("<<xT<<" - " << pos<<" ) +  "<<D<<  "*" << vel << " + "<< tau <<" * "<<acc<<" ) / ( "<< xT <<" - "<< x0 << ")" << std::endl;
        //        std::cout << *itCanon << ": " << result[*itCanon] << std::endl;

//        approximator->ilearn(uInv(*itCanon), value);
    }


    return result;
}



Vec<DMPState>
BasicDMP::calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor)
{
    if (startPositions.size() == 0)
    {
        //        throw Exception("There is no start position set. If you use calculateTrajectoryPoint directly, you must set the start position before (setStartPositions()). ");
        std::cout << "No Start Positions set, assuming current state as start position" << std::endl;
        setStartPositions(currentStates);
    }

//    __trajDim = getTrainingTraj(0).dim();
    ODE::setDim(1 + __trajDim * 2);
//    ODE::setDim(1 + __trajDim * 2);


    DVec initialODEConditions = canonicalValues;
    DVec initialStatesVec = SystemStatus::convertStatesToArray(currentStates);
    initialODEConditions.insert(initialODEConditions.end(), initialStatesVec.begin(), initialStatesVec.end());

    setTemporalFactor(temporalFactor);
    setGoals(goal);

    DVec rawResult;
    ODE::integrate(t, tInit, initialODEConditions, rawResult, false);

//    ODE::integrateWithEulerMethod(t, tInit, initialODEConditions, rawResult);

    canonicalValues.assign(rawResult.begin(), rawResult.begin() + 1);
    rawResult.erase(rawResult.begin());
    Vec<DMPState> result = SystemStatus::convertArrayToStates<DMPState>(rawResult);


    return result;

}

void BasicDMP::setStartPositions(const Vec<DMPState> &initialStates)
{
    DVec tempStartPositions;

    for (unsigned int i = 0; i < initialStates.size(); i++)
    {
        tempStartPositions.push_back(initialStates[i].pos);
    }

    setStartPositions(tempStartPositions);
}

SampledTrajectoryV2 BasicDMP::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{
    if (getShowPlots())
    {
        Vec<DVec> uValuesOrig;
        canonicalSystem->setTau(temporalFactor);
//        canonicalSystem->integrate(timestamps, canonicalSystem->getInitValues(), uValuesOrig);
        canonicalSystem->integrateWithEulerMethod(timestamps, canonicalSystem->getInitValues(), uValuesOrig);
        DoubleMap uValuesPlotMap;

        for (unsigned int i = 0; i < uValuesOrig.size(); ++i)
        {
            uValuesPlotMap[timestamps.at(i)] = uValuesOrig[i][0];
        }

        plot(uValuesPlotMap, "plots/canonical_system_sloweddown%d.gp", true, 0);
    }

    setStartPositions(initialStates);

    return DMPInterfaceTemplate<DMPState>::calculateTrajectory(timestamps, goal, initialStates, initialCanonicalValues, temporalFactor);
}





double BasicDMP::evaluateReproduction(double initialCanonicalValue, double& maxErrorOut, bool showPlot)
{
    DVec timestamps;
    double meanError = 0.0;
    timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + getTrainingTraj(0).size()));
    DVec trainTimestamps = getTrainingTraj(0).getTimestamps();
    double goal = getTrainingTraj(0).rbegin()->getPosition(0);
    SampledTrajectoryV2 newTraj = calculateTrajectory(
                                      timestamps,
                                      goal,
                                      DMPState(getTrainingTraj(0).begin()->getPosition(0), getTrainingTraj(0).begin()->getDeriv(0, 1)),
                                      initialCanonicalValue,
                1);
    std::cout << "start: " << getTrainingTraj(0).begin()->getPosition(0) << " startVel: " << getTrainingTraj(0).begin()->getDeriv(0, 1) << " goal: " << goal << std::endl;

    DVec statesGen = newTraj.getDimensionData(0, 0);
    DVec statesOrig = getTrainingTraj(0).getDimensionData(0, 0);

    //    if(statesGen.size() != statesOrig.size())
    //        throw Exception("generated and original trajectories do not have the same sample count") << statesGen.size() << " vs. " << statesOrig.size();
    //    if(statesGen.size() != timestamps.size())
    //        throw Exception("trajectory timestamps and positions sample count do not match:") ;
    DoubleMap errorMap;
    double minTrajValue = std::numeric_limits<double>::max();
    double maxTrajValue = std::numeric_limits<double>::min();
    maxErrorOut = 0.0;

    for (unsigned int i = 0; i < statesGen.size() && i < statesOrig.size(); ++i)
    {
        const double error = fabs(statesGen[i] - statesOrig[i]);

        if (error > maxErrorOut)
        {
            maxErrorOut = error;
        }

        errorMap[timestamps[i]] = error;
        meanError += error;

        if (minTrajValue > statesOrig[i])
        {
            minTrajValue = statesOrig[i];
        }

        if (maxTrajValue < statesOrig[i])
        {
            maxTrajValue = statesOrig[i];
        }

    }

    if (showPlot)
    {
        Vec< SampledTrajectoryV2 > trajectories;

        trajectories.push_back(SampledTrajectoryV2::normalizeTimestamps(getTrainingTraj(0)));
        trajectories.push_back(newTraj);
        //        SampledTrajectory::plot(trajectories,"plots/trajectories_%d_dim%d.gp", true);
        plot(errorMap, "plots/traj_reproduction_error_%d.gp", true, 0);
        DoubleMap newTrajData;
        DVec timestamps = newTraj.getTimestamps();
        DVec positions = newTraj.getDimensionData(0, 0);

        Vec<DVec> plots;
        plots.push_back(timestamps);
        plots.push_back(getTrainingTraj(0).getDimensionData(0, 0));
        plots.push_back(newTraj.getDimensionData(0, 0));
        plotTogether(plots, "plots/trajComparison_%d.gp");
        plots.clear();
        plots.push_back(timestamps);
        plots.push_back(getTrainingTraj(0).getDimensionData(0, 1));
        plots.push_back(newTraj.getDimensionData(0, 1));
        plotTogether(plots, "plots/trajComparison_vel_%d.gp");
        plots.clear();
        plots.push_back(timestamps);
        plots.push_back(getTrainingTraj(0).getDimensionData(0, 2));
        plots.push_back(newTraj.getDimensionData(0, 2));
        plotTogether(plots, "plots/trajComparison_acc_%d.gp");
    }

    meanError /= statesOrig.size();
    meanError /= maxTrajValue - minTrajValue;
    maxErrorOut /= maxTrajValue - minTrajValue;
    return meanError;
}


void BasicDMP::setConfiguration(string paraID, const paraType& para){
    if (paraID == DMP::SpringFactor)
    {
        setSpringConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::DampingFactor)
    {
        setDampingConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::TemporalFactor)
    {
        setTemporalFactor(boost::get<double>(para));
    }
    else if (paraID == DMP::Goal)
    {
        setGoals(boost::get<DMP::DVec >(para));
    }
    else if (paraID == DMP::StartPosition)
    {
        setStartPositions(boost::get<DMP::DVec>(para));
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
}



void BasicDMP::showConfiguration(string paraID){
    if (paraID == DMP::SpringFactor)
    {
        std::cout << "K: " << K << std::endl;
    }
    else if (paraID == DMP::DampingFactor)
    {
        std::cout << "D: " << D << std::endl;
    }
    else if (paraID == DMP::TemporalFactor)
    {
        std::cout << "tau: " << canonicalSystem->getTau() << std::endl;
    }
    else if (paraID == DMP::Goal)
    {
        std::cout << "goals: " << goals << std::endl;
    }
    else if (paraID == DMP::StartPosition)
    {
        std::cout << "startPositions: " << startPositions << std::endl;
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
}

