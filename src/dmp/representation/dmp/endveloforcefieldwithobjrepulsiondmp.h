/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#ifndef DMP_ENDVELOFORCEFIELDWITHOBJREPULSIONDMP_H
#define DMP_ENDVELOFORCEFIELDWITHOBJREPULSIONDMP_H

#include "endveloforcefielddmp.h"
#include <VirtualRobot/VirtualRobotCommon.h>
#include <dmp/representation/systemstatus.h>

namespace DMP {

    class EndVeloForceFieldWithObjRepulsionDMP : public DMP::EndVeloForceFieldDMP
    {
    public:
        EndVeloForceFieldWithObjRepulsionDMP(double tau =  1);
        void loadObject(std::string name, VirtualRobot::ObstaclePtr object);
        void loadRobotNS(string name, VirtualRobot::RobotNodeSetPtr robot);
        void loadRobot(string name, VirtualRobot::RobotPtr _robot);
        SampledTrajectoryV2 calculateTrajectory(const DVec &timestamps, const DVec &goal, const Vec<DMP::DMPState> &initialStates, const DVec &initialCanonicalValues, double temporalFactor) override;
        void write_text_to_log_file(const string &text) const;
    protected:
        double _getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const override;
        std::map<std::string, VirtualRobot::ObstaclePtr> objects;
        VirtualRobot::SceneObjectSetPtr obstacles;
        VirtualRobot::RobotNodeSetPtr robotNS;
        VirtualRobot::RobotPtr robot;

        void flow(double t, const DMP::DVec &x, DMP::DVec &out) override;


    };

}

#endif
