/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DISCRETEDMP_H
#define DISCRETEDMP_H

#include <dmp/representation/basicode.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/functionapproximation/functionapproximation.h>
#include <dmp/representation/systemstatus.h>

#include "dmpinterface.h"

#include <dmp/functionapproximation/lwprwrapper.h>

#include <map>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>


namespace DMP
{


    class BasicDMP : public DMPInterfaceTemplate<DMPState>
    {
    public:
        using DMPInterfaceTemplate<DMPState>::calculateTrajectory;
        /**
         * @brief BasicDMP
         * @param D is the damping factor
         * @param tau is the temporal length factor used for training
         */
        BasicDMP(int kernelSize = 100, double D = 20, double tau = 1.0);

        std::string getDMPType() override{return "BasicDMP";}

        void learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories) override ;

        Vec<DMPState> calculateTrajectoryPoint(double t, const DVec& goal, double tCurrent, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor) override;
         SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor) override;

        /**
         * @brief evaluateReproduction evaluates the accuracy of the reproduction
         * of an example trajectory with same startPosition, startVelocity and
         * goalPosition.
         * @return returns the mean error of the trajectory scaled to the range
         * of the trajectory values
         * @pre An example trajectory must have been learned in with
         * learnFromTrajectory()
         * @see learnFromTrajectory()
         */
        double evaluateReproduction(double initialCanonicalValue, double& maxErrorOut, bool showPlot = true) override;


        const SampledTrajectoryV2& getTrainingTraj(unsigned int trajSampleIndex) const override;

        void setGoals(const DVec& goals) override
        {
            this->goals = goals;
        }

        DVec getGoals() override
        {
            return goals;
        }

        void setStartPositions(const DVec& startPositions) override
        {
            this->startPositions = startPositions;
        }
        void setSpringConstant(const double& K)
        {
            this->K = K;
        }
        void setDampingConstant(const double& D)
        {
            this->D = D;
        }
        void setDampingAndSpringConstant(const double& D)
        {
            this->D = D;
            K = (D / 2) * (D / 2);
        }
//        void setTemporalFactor(const double& tau)
//        {
//            canonicalSystem->setTau(tau);
//        }
//        double getTemporalFactor() const
//        {
//            return canonicalSystem->getTau();
//        }



        double getDampingFactor() const override { return D; }
        double getSpringFactor() const override { return K; }
        void setStartPositions(const Vec<DMPState> &initialStates);

        DVec getStartPositions() override{return startPositions;}
        void setConfiguration(string paraID,const paraType& para) override;

//        virtual void showdmpDataMemoryPrxConfiguration(int paraID);

        void setAmpl(const unsigned int i, const double ampl) override
        {
            if(i > amplitudes.size() - 1)
            {
                std::cout << "Warning: out of the range of amplitudes" << std::endl;
                amplitudes.push_back(ampl);
                return;
            }
            amplitudes[i] = ampl;
        }

        double getAmpl(const unsigned int i) const override
        {
            return amplitudes[i];
        }

//        virtual void showdmpDataMemoryPrxConfiguration(int paraID);


        Vec<SampledTrajectoryV2 > trainingTraj;

        // inherited from DMPInterfaceTemplate
        double _getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const override;

        void showConfiguration(string paraID) override;
    protected:
        // inherited from ODE
        void flow(double t, const DVec& x, DVec& out) override;

        /**
         * @brief calcPerturbationForceSamples calculates the perturbation function dependent on the canonical system
         * that determines the characteristic shape of the result trajectory.
         * @param t
         * @return
         * @see learnFromTrajectory(), flow()
         */
        virtual DoubleMap calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues,  const SampledTrajectoryV2& exampleTraj) const;



        //        unsigned int dimValuesOfInterest() const{ return 2;}
        //        void projectStateToValuesOfInterest(const double *x, DVec &projection) const;

        //    private:

        void _trainFunctionApproximator(unsigned int trajDimIndex, const DoubleMap& perturbationForceSamples) override;

        /**
         * @brief goal specifies the desired end position of the trajectory.
         *
         * Must differ from \ref startPosition to have a trajectory.
         */
        DVec goals;

        /**
         * @brief startPosition specifies the start position of the trajectory.
         * @see startVelocity
         */
        DVec startPositions;
        /**
         * @brief K is the spring constant of the damped-spring-mass-system.
         */
        double K;

        /**
         * @brief D is damping of the damped-spring-mass-system.
         */
        double D;


        /**
         * @brief amplitudes are a vector of all amplitudes for different dimension of trajectories
         */
        DVec amplitudes;


        DVec training_goals;
        DVec training_startPositions;

        //        DoubleMap perturbationForceSamples;



        /**
         * @brief normalizedLearnedTraj contains the learnedTraj, where the timestamps
         * a normalized to go from 0 to 1.
         */
        //        SampledTrajectory normalizedLearnedTraj;
        //        mutable Vec<FunctionApproximationInterfacePtr> __functionApproximators;

        mutable std::map<unsigned int, bool> scalingActivated;

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {

            ar& boost::serialization::make_nvp("K",K);
            ar& boost::serialization::make_nvp("D",D);
            ar& boost::serialization::make_nvp("goals",goals);
            ar& boost::serialization::make_nvp("startPositions", startPositions);
            ar& boost::serialization::make_nvp("scalingActivated",scalingActivated);
            ar& boost::serialization::make_nvp("trainingTraj", trainingTraj);
            ar& boost::serialization::make_nvp("training_goals", training_goals);
            ar& boost::serialization::make_nvp("training_startPositions", training_startPositions);

            DMPInterface& base = boost::serialization::base_object<DMPInterface>(*this);
            ar& boost::serialization::make_nvp("base",base);

        }
    };




}


#endif // DISCRETEDMP_H
