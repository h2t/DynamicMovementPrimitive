#ifndef UMITSMP_H
#define UMITSMP_H


#include <dmp/representation/basicode.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/functionapproximation/functionapproximation.h>
#include <dmp/representation/systemstatus.h>

#include "dmpinterface.h"
#include <dmp/math/measure.h>
#include <dmp/functionapproximation/lwprwrapper.h>

#include <map>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
//#include "optimization.h"
#include <armadillo>
#include "dmp/representation/viapoint.h"
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
//using namespace alglib;

#include "dmp/representation/dmpconfig.h"

using namespace arma;
namespace DMP
{
 #define UMITSMP_BASE_LINEAR 1
 #define UMITSMP_BASE_MINJERK 2
 #define UMITSMP_BASE_OPTIMIZED 3
    class UMITSMP : public DMPInterfaceTemplate<DMPState>
    {

    public:
        UMITSMP(int kernelSize=10, int baseMode = UMITSMP_BASE_LINEAR, double tau = 1.0, double eps = 1e-5);
        UMITSMP(const std::string& filename)
        {
            load(filename);
        }

        void save(const std::string& filename)
        {
            std::ofstream ofs(filename);
            boost::archive::xml_oarchive ar(ofs);
            ar << boost::serialization::make_nvp("vmp", *this);
        }

        void load(const std::string& filename)
        {
            std::ifstream ifs(filename);
            boost::archive::xml_iarchive ai(ifs);
            ai >>  boost::serialization::make_nvp("vmp", *this);
        }

        string getDMPType() override
        {
            return "UMITSMP";
        }

        void learnFromTrajectories(const Vec<SampledTrajectoryV2> &trainingTrajectories) override;
        DMP::Vec<DMP::DMPState> calculateDirectlyVelocity(const Vec<DMPState> &currentState, double canVal, double deltaT, DVec& targetState);

        DMP::SampledTrajectoryV2 calculateTrajectory(const DMP::DVec &timestamps, const DMP::DVec &goal, const Vec<DMP::DMPState> &initialStates, const DMP::DVec &initialCanonicalValues, double temporalFactor) override;

        void setGoals(const DVec &goals) override
        {
            setViaPoint(canonicalSystem->getUMin(), goals);
        }

        void setStartPositions(const DVec &startPositions) override
        {
            setViaPoint(1, startPositions);
        }

        void setTemporalFactor(const double &tau) override
        {
            this->tau = tau;
            canonicalSystem->setTau(tau);
        }

        void setBaseMode(int baseMode)
        {
            this->baseMode = baseMode;
        }
        double _getPerturbationForce(unsigned int funcID, DVec canonicalStates) const override;
        double _getPerturbationForceDeriv(unsigned int funcID, DVec canonicalStates) const;

        Vec<vec> getStyleParasWithRatio(const DVec& ratios);

        double tau;
        Vec<SampledTrajectoryV2 > trainingTrajs;
        Vec<vec> styleParas;
        std::vector<double> defaultGoal;
        size_t numOfDemos;
        size_t dimOfTraj;

        const SampledTrajectoryV2& getTrainingTraj(unsigned int trajSampleIndex) const override;

        void setViaPoint(double u, const DVec& viaPoseVec);
        void removeViaPoints();
        void prepareExecution(const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor);

        void prepareExecution(const MPConfig& config)
        {
            prepareExecution( config.goal, config.initialState, 1, 1);
        }

        void learnBaseFunction();

        double uprocess(double uval)
        {
            if(canonicalSystem->getName() == "Linear Decay")
            {
                return uval;
            }
            else if(canonicalSystem->getName() == "Exponential Decay")
            {
                return -log(uval)*6.90776;
            }
        }

        double getUMin()
        {
            return canonicalSystem->getUMin();
        }

        ViaPoseSet getViaPoints()
        {
            return viaPoints;
        }
        double overFlyFactor;

        void loadWeightsFromFile(const std::string& fileName);
    protected:
        Vec<mat> scoreMatList;
        int baseMode;
        ViaPoseSet viaPoints;
        void _trainFunctionApproximator(unsigned int funcID, const DoubleMap &forceSamples) override;

        vec getForce(int dim, const DVec& timestamps, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj);
        mat getQuatForce(int dim, const DVec& timestamps, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj);
        Eigen::Quaterniond quatSlerp(double t, const Eigen::Quaterniond& q0, const Eigen::Quaterniond& q1);
        Eigen::Quaterniond dquatSlerp(double t, const Eigen::Quaterniond& q0, const Eigen::Quaterniond& q1);
        DoubleMap createDoubleMap(const DVec& canonicalVals, const vec &sampleVec);

        void flow(double t, const DVec &x, DVec &out) override
        {
            throw DMP::Exception("flow function is not implemented in UMITSMP");
        }

        vec calcForce(double canonicalVal);
        vec calcForceWithDeri(double u, vec& deriForce);
        vec getMinJerkCoef(double terminalTime, const vec& condition);
        DVec canVals;
        mat getMinJerkQuadTerm(int len);
//        real_2d_array mat2real2d(const mat& m);
//        real_1d_array vec2real1d(const vec& v);
        Eigen::Quaterniond quatMultiply(const Eigen::Quaterniond& q2, const Eigen::Quaterniond& q1);
    public:
        Vec<DMP::DMPState> calculateTrajectoryPoint(double t, const DVec &goal, double tCurrent, const Vec<DMP::DMPState> &currentStates, DVec &currentCanonicalValues, double temporalFactor) override
        {
            throw DMP::Exception("calculateTrajectoryPoint: not implemented yet");
            return currentStates;
        }

        void setupWithWeights(const std::vector<std::vector<double> > &weights);
    private:

        template<class T>
        std::string saveArma(T& t) const
        {
            std::stringstream stream(std::stringstream::out | std::stringstream::binary);
            t.save(stream,arma::file_type::raw_ascii);
            return stream.str();
        }

        template<class T>
        void loadArma(T& t,string s)
        {
            std::stringstream stream(s,std::stringstream::in | std::stringstream::binary);
            t.load(stream,arma::file_type::raw_ascii);
        }

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            ar& boost::serialization::make_nvp("tau",tau);
            ar& boost::serialization::make_nvp("trainingTrajs",trainingTrajs);

            if(Archive::is_saving::value)
            {
                std::vector<std::string> vector;
                vector.reserve(styleParas.size());
                for(auto& t:styleParas)
                {
                    vector.push_back(saveArma(t));
                }
                ar& boost::serialization::make_nvp("styleParas",vector);
            }
            else
            {
                std::vector<std::string> vector;
                ar& boost::serialization::make_nvp("styleParas",vector);
                styleParas.resize(vector.size());
                for(std::size_t idx= 0 ; idx < vector.size();idx++)
                {
                    loadArma(styleParas.at(idx),vector.at(idx));
                }
            }

            ar& boost::serialization::make_nvp("dimOfTraj",dimOfTraj);
            ar& boost::serialization::make_nvp("numOfDemos",numOfDemos);

            if(Archive::is_saving::value)
            {
                std::vector<std::string> vector;
                vector.reserve(scoreMatList.size());
                for(auto& t:scoreMatList)
                {
                    vector.push_back(saveArma(t));
                }
                ar& boost::serialization::make_nvp("scoreMatList",vector);
            }
            else
            {
                std::vector<std::string> vector;
                ar& boost::serialization::make_nvp("scoreMatList",vector);
                scoreMatList.resize(vector.size());
                for(std::size_t idx = 0;idx < vector.size();idx++)
                {
                    loadArma(scoreMatList.at(idx),vector.at(idx));
                }
            }

            ar& boost::serialization::make_nvp("baseMode",baseMode);
            ar& boost::serialization::make_nvp("viaPoints",viaPoints);
            ar& boost::serialization::make_nvp("canVals",canVals);
            ar& boost::serialization::make_nvp("overFlyFactor",overFlyFactor);
            ar& boost::serialization::make_nvp("defaultGoal",defaultGoal);

            auto& base = boost::serialization::base_object<DMPInterfaceTemplate<DMPState>>(*this);
            ar& boost::serialization::make_nvp("base",base);
        }

    };

    typedef boost::shared_ptr<UMITSMP> UMITSMPPtr;

}


#endif
