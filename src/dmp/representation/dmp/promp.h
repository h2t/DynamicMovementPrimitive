#ifndef UMITSMP_H
#define UMITSMP_H


#include <dmp/representation/basicode.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/functionapproximation/functionapproximation.h>
#include <dmp/representation/systemstatus.h>

#include "dmpinterface.h"
#include <dmp/math/measure.h>
#include <dmp/functionapproximation/lwprwrapper.h>

#include <map>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <armadillo>

#include "dmp/representation/viapoint.h"

using namespace arma;
namespace DMP
{
    class ProMP
    {
    public:
        ProMP(size_t kernelSize = 30);

        void learnFromTrajectories(const Vec<SampledTrajectoryV2> &trainingTrajectories);
        DMP::Vec<DMP::DMPState> step(double &canVal, double deltaT);
        void reset();

        DMP::DVec2d sampleWeight(bool isPosition=true);
        DMP::SampledTrajectoryV2 generateTrajectory(const DVec& timestamps, const DVec2d &weight, bool isPosition=true);
        double tau;
        DVec goals;
        DVec startPositions;
        Vec<SampledTrajectoryV2 > trainingTrajs;
        Vec<vec> styleParas;
        size_t kerSize;

        const SampledTrajectoryV2& getTrainingTraj(unsigned int trajSampleIndex)
        {
            return trainingTrajs.at(trajSampleIndex);
        }

        void addViaPoint(const SimpleViaPointPtr& viaPoint)
        {
            viaPointSet->push_back(viaPoint);
        }

        void removeViaPoint(double canVal)
        {
            viaPointSet->removeViaPointAt(canVal);
        }

        void updateWeights();

        size_t dimOfTraj;
        size_t numOfDemos;
    protected:
        Vec<LWRPtr > fapprox;
        LinearDecayCanonicalSystemPtr canSys;

        double dtime;
        SimpleViaPointSetPtr viaPointSet;
        void _trainFunctionApproximator(unsigned int funcID, const DMP::DoubleMap &forceSamples);
        LWRPtr _getFunctionApproximator(unsigned int funcID);
        DoubleMap createDoubleMap(const DVec& canonicalVals, const DVec &sampleVec);

    private:
        std::vector<vec > posLWmu;
        std::vector<mat > posLWsigma;
        std::vector<vec > velLWmu;
        std::vector<mat > velLWsigma;

        std::vector<vec > posWmu;
        std::vector<mat > posWsigma;
        std::vector<vec > velWmu;
        std::vector<mat > velWsigma;

        DVec uValues;

        template<class T>
        std::string saveArma(T& t) const
        {
            std::stringstream stream(std::stringstream::out | std::stringstream::binary);
            const bool b=true;
            t.save(stream,arma::file_type::raw_ascii);
            return stream.str();
        }

        template<class T>
        void loadArma(T& t,string s)
        {
            std::stringstream stream(s,std::stringstream::in | std::stringstream::binary);
            t.load(stream,arma::file_type::raw_ascii);
        }

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int /*file_version*/)
        {
           //to be done;
        }

    };

    typedef boost::shared_ptr<ProMP> ProMPPtr;

}


#endif
