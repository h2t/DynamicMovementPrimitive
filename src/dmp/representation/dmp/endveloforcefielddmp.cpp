/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "endveloforcefielddmp.h"

#include <limits>

using namespace DMP;

EndVeloForceFieldDMP::EndVeloForceFieldDMP(int kernelSize, double tau) :
    SimpleEndVeloDMP(kernelSize, tau, false)
{
}



void EndVeloForceFieldDMP::flow(double t, const DMP::DVec& x, DMP::DVec& out)
{
    double u = x.at(0);

    DVec canonicalState(1);
    canonicalState[0] = u;
    canonicalSystem->flow(t, canonicalState, out);

    double tau = canonicalSystem->getTau();


    unsigned int dimensions = __trajDim;

    if (dimensions > goals.size())
    {
        throw Exception("There are less trajectory goals than dimensions");
    }

    if (dimensions > startPositions.size())
    {
        throw Exception("There are less trajectory start positions than dimensions");
    }


    unsigned int offset = canonicalState.size();
    unsigned int curDim = 0;

    while (curDim < dimensions)
    {
        double curGoal = _getPerturbationForce(curDim, u);
        unsigned int i = curDim * 2 + offset;
        const double pos =  x[i];
        const double vel = x[i + 1];
        const double goal = goals[curDim];
        const double start = startPositions[curDim];

        out.at(i++) = 1.0 / tau * (vel);

        const double x0 = getTrainingTraj(0).begin()->getPosition(curDim);
        const double xT = getTrainingTraj(0).rbegin()->getPosition(curDim);
        double goalOffset = fabs(goal - start) - fabs(xT - x0);

        double scaleFactor = goalOffset / (maxAbsDisplacement.at(curDim));
        curGoal *= 1 + (scaleFactor);

        out.at(i)   = 1.0 / tau * (K * (goal + curGoal - pos) - (D * vel));

        curDim++;
    }
}


DoubleMap EndVeloForceFieldDMP::calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj) const
{
    DoubleMap result;


    if (exampleTraj.size() == 0)
    {
        throw Exception("Sample traj is empty");
    }

    if (canonicalValues.size() != exampleTraj.size())
    {
        throw Exception("The amount of the canonicalValues do not match the exampleTrajectory");
    }

    // start position of example traj
    double x0 = exampleTraj.begin()->getPosition(trajDim);
    // end position of example traj
    double xT = exampleTraj.rbegin()->getPosition(trajDim);

    double tau = canonicalSystem->getTau();


    DVec::const_iterator itCanon = canonicalValues.begin();

    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();

    for (itCanon = canonicalValues.begin(), itTraj = exampleTraj.begin(); itCanon != canonicalValues.end(); ++itCanon,  itTraj++)
    {
        // first value is time value, so skip it
        const SampledTrajectoryV2::TrajData& state = *itTraj;
        const double& pos = state.getPosition(trajDim);
        const double& vel = state.getDeriv(trajDim, 1);
        const double& acc = state.getDeriv(trajDim, 2);


        result[*itCanon] = (tau * acc + D * vel) / K - xT + pos;
        //        std::cout << *itCanon << "[" << result[*itCanon] << "] calc. from ( -" << K << " * ("<<xT<<" - " << pos<<" ) +  "<<D<<  "*" << vel << " + "<< tau <<" * "<<acc<<" ) / ( "<< xT <<" - "<< x0 << ")" << std::endl;
        //        std::cout << *itCanon << ": " << result[*itCanon] << std::endl;
    }



    return result;
}

double EndVeloForceFieldDMP::calculateMaxAbsDisplacement(const SampledTrajectoryV2& exampleTraj, unsigned int trajDim) const
{
    double maxAbsDisplacement = 0;
    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();

    const double x0 = exampleTraj.begin()->getPosition(trajDim);

    for (; itTraj != exampleTraj.end(); itTraj++)
    {
        const SampledTrajectoryV2::TrajData& state = *itTraj;
        const double& pos = state.getPosition(trajDim);

        maxAbsDisplacement = std::max(maxAbsDisplacement , fabs(pos - x0));
    }

    return maxAbsDisplacement;
}


void DMP::EndVeloForceFieldDMP::learnFromTrajectories(const Vec<SampledTrajectoryV2> &trainingTrajectories)
{
    if (trainingTrajectories.size() > 1)
    {
        throw Exception() << "this DMP can only be trained with one trajectory (but with several dimensions)";
    }

    if (trainingTrajectories.size() == 0)
    {
        throw Exception() << "atleast one trainingtrajectory must be given";
    }

    const SampledTrajectoryV2& trainingTrajectory = trainingTrajectories[0];

    const size_t dims = trainingTrajectory.dim();

    maxAbsDisplacement.resize(dims, 0.0);

    for (size_t d = 0; d < dims; d++)
    {
        maxAbsDisplacement[d] = calculateMaxAbsDisplacement(trainingTrajectory, d);
        std::cout << d << ": " << maxAbsDisplacement[d] << std::endl;
    }

    BasicDMP::learnFromTrajectories(trainingTrajectories);
}
