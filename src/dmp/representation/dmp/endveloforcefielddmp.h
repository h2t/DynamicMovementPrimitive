/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#ifndef DMP_ENDVELOFORCEFIELDDMP_H
#define DMP_ENDVELOFORCEFIELDDMP_H

#include "simpleendvelodmp.h"

namespace DMP {

    class EndVeloForceFieldDMP : public DMP::SimpleEndVeloDMP
    {
    public:
        EndVeloForceFieldDMP(int kernelSize=50, double tau =  1);
        void learnFromTrajectories(const Vec<SampledTrajectoryV2> &trainingTrajectories) override;
    protected:
        DoubleMap calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj) const override;
        double calculateMaxAbsDisplacement(const SampledTrajectoryV2& exampleTraj, unsigned int trajDim) const;
        void flow(double t, const DVec& x, DVec& out) override;
        DVec maxAbsDisplacement;
        //        mutable DVec lastTurningPointValue;
        //        mutable DVec minTrajValue;

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {

            ar& boost::serialization::make_nvp("maxAbsDisplacement",maxAbsDisplacement);

            SimpleEndVeloDMP& base = boost::serialization::base_object<SimpleEndVeloDMP>(*this);
            ar& boost::serialization::make_nvp("base",base);

        }


    };

}

#endif
