/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef COLLISIONAVOIDANCEDMP_H
#define COLLISIONAVOIDANCEDMP_H

#include "basicdmp.h"

//namespace DMP {
//    class CollisionAvoidanceDMP : public BasicDMP
//    {
//    public:
//        CollisionAvoidanceDMP();
//    protected:
//        void flow(double t, const DVec &x, DVec &out);
//    };


//    class CollisionObject
//    {
//    public:
//        virtual double distance(const DVec& queryPoint) const = 0;
//        virtual DVec distancePerAxis(const DVec& queryPoint) const = 0;
//    };

//    class CollisionBox : public CollisionObject
//    {
//    public:
//        CollisionBox(DVec center, DVec widthVec, DVec heightVec, DVec depthVec);
//        double distance(const DVec &queryPoint) const;
//        DVec distancePerAxis(const DVec &queryPoint) const;
//    private:
//        DVec widthVec, heightVec, depthVec;
//        DVec center;
//    };
//}


#endif // COLLISIONAVOIDANCEDMP_H
