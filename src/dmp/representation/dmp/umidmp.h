#ifndef UMIDMP_H
#define UMIDMP_H


#include <dmp/representation/basicode.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/functionapproximation/functionapproximation.h>
#include <dmp/representation/systemstatus.h>

#include "dmpinterface.h"
#include <dmp/math/measure.h>
#include <dmp/functionapproximation/lwprwrapper.h>

#include <map>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/vector.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
//#include "optimization.h"

#include <armadillo>
#include "dmp/representation/dmpconfig.h"

#include "dmp/representation/viapoint.h"

//using namespace alglib;



using namespace arma;
namespace DMP
{
#define UMIDMP_BASE_LINEAR 1
 #define UMIDMP_BASE_MINJERK 2
 #define UMIDMP_BASE_OPTIMIZED 3
    class UMIDMP : public DMPInterfaceTemplate<DMPState>
    {

    public:
        using DMPInterfaceTemplate<DMPState>::calculateTrajectory;

        UMIDMP(int kernelSize = 100,  int baseMode = UMIDMP_BASE_LINEAR, double tau = 1.0, double eps = 1e-5);


        UMIDMP(const std::string& filename)
        {
            load(filename);
        }

        string getDMPType() override
        {
            return "UMIDMP";
        }

        void save(const std::string& filename)
        {
            std::ofstream ofs(filename);
            boost::archive::xml_oarchive ar(ofs);
            ar << boost::serialization::make_nvp("vmp", *this);
        }

        void load(const std::string& filename)
        {
            std::ifstream ifs(filename);
            boost::archive::xml_iarchive ai(ifs);
            ai >>  boost::serialization::make_nvp("vmp", *this);
        }


        void learnFromTrajectories(const Vec<SampledTrajectoryV2> &trainingTrajectories) override;

        Vec<DMPState> calculateTrajectoryPoint(double t, const DVec& goal, double tCurrent, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor) override;
        Vec<DMPState> calculateDirectlyVelocity(const Vec<DMPState> &currentState, double canVal, double deltaT, DVec& targetState);

        SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor) override;

        void setGoals(const DVec &goals) override
        {
            setViaPoint(canonicalSystem->getUMin(), goals);
        }

        void setStartPositions(const DVec &startPositions) override
        {
            setViaPoint(1, startPositions);
        }

        void setTemporalFactor(const double &tau) override
        {
            this->tau = tau;
            canonicalSystem->setTau(tau);
        }

        void setBaseMode(int baseMode)
        {
            this->baseMode = baseMode;
        }

        double _getPerturbationForce(unsigned int funcID, DVec canonicalStates) const override;
        double _getPerturbationForceDeriv(unsigned int funcID, DVec canonicalStates) const;

        Vec<vec> getStyleParasWithRatio(const DVec& ratios);

        double tau;
        Vec<SampledTrajectoryV2 > trainingTrajs;
        Vec<vec> styleParas;

        void setViaPoint(const ViaPointMap& viapoint, bool CanonicalValueAsKey = true);
        void setViaPoint(double u, const DVec& point)
        {
            ViaPointMap viapoint = std::make_pair(u, point);
            setViaPoint(viapoint);
        }
        size_t dimOfTraj;


        void prepareExecution(const MPConfig& config)
        {
            prepareExecution( config.goal, config.initialState, 1, 1);
        }


        void prepareExecution(const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor);
        size_t numOfDemos;

        std::vector<double> defaultGoal;


        double uprocess(double uval)
        {
            if(canonicalSystem->getName() == "Linear Decay")
            {
                return uval;
            }
            else if(canonicalSystem->getName() == "Exponential Decay")
            {
                return -log(uval)*6.90776;
            }
        }

        double getUMin()
        {
            return canonicalSystem->getUMin();
        }

        ViaPointSet getViaPoints()
        {
            return viaPoints;
        }

        void loadWeightsFromFile(const std::string& fileName);
        void setupWithWeights(const std::vector<std::vector<double> > &weights);
        const SampledTrajectoryV2& getTrainingTraj(unsigned int trajSampleIndex) const override
        {
            return trainingTrajs.at(trajSampleIndex);
        }
    protected:
        Vec<mat> scoreMatList;

        int baseMode;
        ViaPointSet viaPoints;
        void _trainFunctionApproximator(unsigned int funcID, const DoubleMap &forceSamples) override;

        vec getForce(int dim, const DVec& timestamps, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj);
        DoubleMap createDoubleMap(const DVec& canonicalVals, const vec &sampleVec);

        void flow(double t, const DVec &x, DVec &out) override
        {
            throw DMP::Exception("flow function is not implemented in UMIDMP");
        }

        vec calcForce(double canonicalVal);
        vec calcForceWithDeri(double u, vec& deriForce);
        vec getMinJerkCoef(double terminalTime, const vec& condition);

        DVec canVals;

//        real_2d_array mat2real2d(const mat& m);
//        real_1d_array vec2real1d(const vec& v);

    private:

        template<class T>
        std::string saveArma(T& t) const
        {
            std::stringstream stream(std::stringstream::out | std::stringstream::binary);
            t.save(stream,arma::file_type::raw_ascii);
            return stream.str();
        }

        template<class T>
        void loadArma(T& t,string s)
        {
            std::stringstream stream(s,std::stringstream::in | std::stringstream::binary);
            t.load(stream,arma::file_type::raw_ascii);
        }

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            ar& boost::serialization::make_nvp("tau",tau);

            if(Archive::is_saving::value)
            {
                std::vector<std::string> vector;
                vector.reserve(styleParas.size());
                for(auto& t:styleParas)
                {
                    vector.push_back(saveArma(t));
                }
                ar& boost::serialization::make_nvp("styleParas",vector);
            }else
            {
                std::vector<std::string> vector;
                ar& boost::serialization::make_nvp("styleParas",vector);
                styleParas.resize(vector.size());
                for(std::size_t idx= 0 ; idx < vector.size();idx++)
                {
                    loadArma(styleParas.at(idx),vector.at(idx));
                }
            }

            ar& boost::serialization::make_nvp("dimOfTraj",dimOfTraj);
            ar& boost::serialization::make_nvp("numOfDemos",numOfDemos);

            if(Archive::is_saving::value)
            {
                std::vector<std::string> vector;
                vector.reserve(scoreMatList.size());
                for(auto& t:scoreMatList)
                {
                    vector.push_back(saveArma(t));
                }
                ar& boost::serialization::make_nvp("scoreMatList",vector);
            }else
            {
                std::vector<std::string> vector;
                ar& boost::serialization::make_nvp("scoreMatList",vector);
                scoreMatList.resize(vector.size());
                for(std::size_t idx = 0;idx < vector.size();idx++)
                {
                    loadArma(scoreMatList.at(idx),vector.at(idx));
                }
            }

            ar& boost::serialization::make_nvp("baseMode",baseMode);
            ar& boost::serialization::make_nvp("viaPoints",viaPoints);           
            ar& boost::serialization::make_nvp("canVals",canVals);
            ar& boost::serialization::make_nvp("defaultGoal",defaultGoal);

            auto& base = boost::serialization::base_object<DMPInterfaceTemplate<DMPState>>(*this);
            ar& boost::serialization::make_nvp("base",base);
        }
    };

    typedef boost::shared_ptr<UMIDMP> UMIDMPPtr;

}


#endif
