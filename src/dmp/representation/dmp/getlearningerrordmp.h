#ifndef GETLEARNINGERRORDMP_H
#define GETLEARNINGERRORDMP_H

#include <vector>
#include "simpleendvelodmp.h"

namespace DMP
{
    // typedef std::vector<LWR<GaussKernel>::KernelData> GaussKernelVec;

    // TODO: Should probably be renamed some day
    class GetLearningErrorDMP : public SimpleEndVeloDMP
    {
    public:
        GetLearningErrorDMP();
        std::vector<std::pair<DoubleMap, DoubleMap> > getLearningApproximationError(const SampledTrajectoryV2& trainingTrajectory);
    };
}

#endif // GETLEARNINGERRORDMP_H
