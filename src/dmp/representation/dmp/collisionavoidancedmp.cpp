/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "collisionavoidancedmp.h"
#include <dmp/general/helpers.h>

using namespace DMP;

//CollisionAvoidanceDMP::CollisionAvoidanceDMP()
//{
//}

//void CollisionAvoidanceDMP::flow(double t, const DVec &x, DVec &out)
//{

//    BasicDMP::flow(t, x, out);


//    double tau = canonicalSystem->getTau();


//    unsigned int dimensions = trainingTraj.size();



////    out.resize(out.size() + dimensions * 2);
//    unsigned int offset = canonicalSystem->dim();
//    unsigned int curDim = 0;
//    while(curDim < dimensions)
//    {
////        const double force = _getPerturbationForce(curDim, u);
//        unsigned int i = curDim*2+offset;
//        double pos = x[i];
//        double vel = x[i+1];

//        double collisionAvoidanceForce = - 30.0 /pow((200-pos)/30,3);
////        double humanPerturbationForce = 0*-std::min<double>(1000, 5 /pow(fabs(t-0.5),3));

//        out.at(i++) = 1.0/tau * (vel)  + collisionAvoidanceForce;
//        i++;
////        out.at(i++) = 1.0/tau * (K * (goals[curDim] - pos) - (D*vel) + (goals[curDim] - startPositions[curDim]) * force);
////        std::cout << "t: " << t << " u: " << u << " pos: " << pos << " vel: " << vel << " force: " << force <<  " cForce: " << collisionAvoidanceForce << std::endl;
//        curDim++;
//    }
//}


//double CollisionBox::distance(const DVec &queryPoint) const
//{

//}

//DVec CollisionBox::distancePerAxis(const DVec &queryPoint) const
//{
//    DVec result(3,0.0);
////    if(vecLength(queryPoint - widthVec))
//}
