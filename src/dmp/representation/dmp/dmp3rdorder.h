/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DMP3RDORDER_H
#define DMP3RDORDER_H

#include "dmpinterface.h"
#include <dmp/representation/canonicalsystem.h>

#include <boost/serialization/map.hpp>

namespace DMP
{

    class DMP3rdOrder : public DMPInterfaceTemplate<_3rdOrderDMP>
    {
    public:
        using DMPInterfaceTemplate<_3rdOrderDMP>::calculateTrajectory;
        /**
         * @brief DMP3rdOrder
         * @param H filter constant, that controls the smoothness of the trajectory, higher value -> slower reaction of trajectory
         * @param D ODE damping constant
         * @param tau temporal factor
         * @param stopAtEnd if true, the trajectory will stop at the end, whether or not the demonstration stops
         */
        DMP3rdOrder(int kernelSize = 50, double H = 5, double D = 20, double tau = 1.0, bool stopAtEnd = false);
        void learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories) override ;


        Vec<_3rdOrderDMP> calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<_3rdOrderDMP> &initialStates, DVec& canonicalValues, double temporalFactor ) override;
        /**
         * @brief calculateTrajectory
         * @param timestamps
         * @param goal
         * @param initialStates
         * @param initialCanonicalValues
         * @param temporalFactor
         * @return generated trajectory. Note: Does not return the real DMP states. Instead it returns the
         * positions and the from the positions calculated velocities and acclerations.
         * @note
         */
         SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<_3rdOrderDMP> &initialStates, const DVec& initialCanonicalValues, double temporalFactor ) override;

        /**
         * @brief evaluateReproduction evaluates the accuracy of the reproduction
         * of an example trajectory with same startPosition, startVelocity and
         * goalPosition.
         * @return returns the mean error of the trajectory scaled to the range
         * of the trajectory values
         * @pre An example trajectory must have been learned in with
         * learnFromTrajectory()
         * @see learnFromTrajectory()
         */
        double evaluateReproduction(double initialCanonicalValue, double& maxErrorOut, bool showPlot = true) override;


        const SampledTrajectoryV2& getTrainingTraj(unsigned int trajDim) const override;

        double getFilterConstant() const
        {
            return H;
        }

        void setStopAtEnd(bool on = true)
        {
            stopAtEnd = on;
        }
        void setStartPositions(const DVec& startPositions) override
        {
            this->startPositions = startPositions;
        }
        void setGoals(const DVec& goals) override
        {
            this->goals = goals;
        }

        void setSpringConstant(const double& K)
        {
            this->K = K;
        }
        void setDampingConstant(const double& D)
        {
            this->D = D;
        }
        void setTemporalFactor(const double& tau) override
        {
            canonicalSystem->setTau(tau);
        }
        double getTemporalFactor() const override
        {
            return canonicalSystem->getTau();
        }

        /**
         * @brief This function converts a _3rdOrderDMP from pos, vel, acc values
         * to the required abstract values of the 3rd Order DMP.
         * @param realState The real position, velocity, acceleration
         * @return The abstract position, velocity, acceleration used by 3rd Order DMP
         */
        static _3rdOrderDMP convertStateTo3rdOrderDMPState(const _3rdOrderDMP& realState, float filterConstant);

        void setConfiguration(string paraID,const paraType& para) override; //TODO
        void showConfiguration(string paraID) override; //TODO

        Vec<SampledTrajectoryV2 > trainingTraj;

    protected:
        // inherited from ODE
        void flow(double t, const DVec& x, DVec& out) override;


        // inherited from DMPInterfaceTemplate
        double _getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const override;

        /**
         * @brief calcPerturbationForceSamples calculates the perturbation function dependent on the canonical system
         * that determines the characteristic shape of the result trajectory.
         * @param t
         * @return
         * @see learnFromTrajectory(), flow()
         */
        virtual DoubleMap calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues,  const SampledTrajectoryV2& exampleTraj) const;

        void _trainFunctionApproximator(unsigned int trajDim, const DoubleMap& perturbationForceSamples) override;

        bool stopAtEnd;

        /**
         * @brief goal specifies the desired end position of the trajectory.
         *
         * Must differ from \ref startPosition to have a trajectory.
         */
        DVec goals;

        /**
         * @brief startPosition specifies the start position of the trajectory.
         * @see startVelocity
         */
        DVec startPositions;
        /**
         * @brief K is the spring constant of the damped-spring-mass-system.
         */
        double K;

        /**
         * @brief D is damping of the damped-spring-mass-system.
         */
        double D;

        /**
         * @brief H is the filter constant
         */
        double H;

        /**
         * @brief normalizedLearnedTraj contains the learnedTraj, where the timestamps
         * a normalized to go from 0 to 1.
         */
        //        SampledTrajectory normalizedLearnedTraj;
        //        mutable Vec<FunctionApproximationInterfacePtr> __functionApproximators;

        mutable std::map<unsigned int, bool> scalingActivated;

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            //TODO: change marco representation for using serialization in other packages

            ar& boost::serialization::make_nvp("K",K);
            ar& boost::serialization::make_nvp("D",D);
            ar& boost::serialization::make_nvp("H",H);
            ar& boost::serialization::make_nvp("goals",goals);
            ar& boost::serialization::make_nvp("stopAtEnd",stopAtEnd);
            ar& boost::serialization::make_nvp("startPositions", startPositions);
            ar& boost::serialization::make_nvp("scalingActivated",scalingActivated);

            DMPInterface& base = boost::serialization::base_object<DMPInterface>(*this);
            ar& boost::serialization::make_nvp("base",base);
        }
    };
}

#endif // DMP3RDORDER_H
