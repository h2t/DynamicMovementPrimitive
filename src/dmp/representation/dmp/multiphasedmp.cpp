#include "multiphasedmp.h"

#include <dmp/general/exception.h>
#include <dmp/general/helpers.h>

#include <limits>

#include <segmentation/characteristicsegmentation.h>

#include <iostream>

#include <sys/time.h>

using namespace DMP;




///////////////////////////////////////////////////////////////////////////
// Implementation
///////////////////////////////////////////////////////////////////////////


MultiPhaseDMP::MultiPhaseDMP(unsigned int dim, double dt, double D, double tau) :
    K((D / 2) * (D / 2)),
    D(D)
{
    FunctionApproximationInterfacePtr approximater(new RBFInterpolator<GaussRadialBasisFunction>(dim));
    setFunctionApproximator(approximater);

    ODE::setEpsabs(dt);
    canonicalSystemList.clear();
    for(size_t i = 0; i < dim; ++i)
    {
        CanonicalSystemPtr cs(new CanonicalSystem(tau));
        cs->setEpsabs(dt);
        canonicalSystemList.push_back(cs);
    }

    canonicalSystem = canonicalSystemList[0];
    amplitudes.push_back(1.0);
}


void MultiPhaseDMP::learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories)
{
    if (trainingTrajectories.size() > 1)
    {
        throw Exception() << "this DMP can only be trained with one trajectory (but with several dimensions)";
    }

    if (trainingTrajectories.size() == 0)
    {
        return;
    }

    trainingTraj = trainingTrajectories;

    __trajDim = trainingTraj.at(0).dim();
    SampledTrajectoryV2& trainingTrajectory = trainingTraj[0];

    // normalize timestamps so that they start at 0 and end at 1
    trainingTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0.0, 1.0);
    trainingTrajectory.differentiateDiscretly(2);
    DVec normTimestamps = trainingTrajectory.getTimestamps();


    // get 2D phase values matrix (each row is corresponding to one timestamp)
    DVec2d uValues;
    Vec<Vec<DVec> > uValuesOrigList;

    for(size_t i = 0; i < canonicalSystemList.size(); ++i)
    {
        Vec<DVec> uValuesOrig;

        CanonicalSystemPtr cs = canonicalSystemList[i];
        cs->integrate(normTimestamps, cs->getInitValues(), uValuesOrig);

        uValuesOrigList.push_back(uValuesOrig);
    }

    for (unsigned int i = 0; i < uValuesOrigList[0].size(); ++i)
    {
        DVec curValues;
        for(unsigned int j = 0; j < uValuesOrigList.size(); ++j)
        {
            Vec<DVec> uValuesOrig = uValuesOrigList[j];
            curValues.push_back(uValuesOrig[i][0]);
        }
        uValues.push_back(curValues);
    }

    unsigned int trajDim = trainingTrajectory.dim();
    SampledTrajectoryV2 traj;

    // guarantee the size of amplitudes
    while(amplitudes.size() < trajDim)
        amplitudes.push_back(1.0);


    for (unsigned int dim = 0; dim < trajDim; dim++)
    {

        DVecMap perturbationForceSamples = calcPerturbationForceSamples(dim, uValues, trainingTrajectory);
        _trainFunctionApproximator(dim, perturbationForceSamples);
    }


}


void MultiPhaseDMP::_trainFunctionApproximator(unsigned int trajDimIndex, const DVecMap& perturbationForceSamples)
{
    FunctionApproximationInterfacePtr approximator = _getFunctionApproximator(trajDimIndex);
    DVecMap::const_iterator it = perturbationForceSamples.begin();


    DVec2d phases;
    DVec values;

    for (it = perturbationForceSamples.begin(); it != perturbationForceSamples.end(); ++it)
    {
        phases.push_back(it->first);
        values.push_back(it->second);
    }

    approximator->learn(phases, values);

}




const SampledTrajectoryV2& MultiPhaseDMP::getTrainingTraj(unsigned int trajSampleIndex) const
{
    return trainingTraj.at(trajSampleIndex);

}



void MultiPhaseDMP::flow(double t, const DVec& x, DVec& out)
{
    DVec u;

    for(size_t i = 0; i < canonicalSystemList.size(); ++i)
    {
        CanonicalSystemPtr canSys = canonicalSystemList[i];

        DVec outCanVal;
        DVec currCanVal(1);
        currCanVal[0] = x.at(i);
        u.push_back(x.at(i));

        canSys->flow(t, currCanVal, outCanVal);

        if(i >= out.size())
        {
            out.push_back(outCanVal[0]);
        }
        else
        {
            out[i] = outCanVal[0];
        }
    }


    double tau = canonicalSystem->getTau();
    unsigned int dimensions = __trajDim;

    if (dimensions > goals.size())
    {
        std::cout << "goals size: " << goals.size() << std::endl;
        std::cout << "dimensions: " << dimensions << std::endl;
        throw Exception("There are less trajectory goals than dimensions");
    }

    if (dimensions > startPositions.size())
    {
        throw Exception("There are less trajectory start positions than dimensions");
    }

    unsigned int offset = canonicalSystemList.size();
    unsigned int curDim = 0;

    bool scaling = false;

    while (curDim < dimensions)
    {
        double force = _getPerturbationForce(curDim, u);

        double scaleFactor = 1;
        if(scaling)
        {
            double multiu = 1;
            for(size_t k = 0; k < u.size(); ++k)
            {
                multiu *= u[k];
            }
            scaleFactor = amplitudes[curDim] * multiu;
        }

        unsigned int i = curDim * 2 + offset;
        double pos = x[i];
        double vel = x[i + 1];

        out[i++] = 1.0 / tau * (vel);
        out[i++] = 1.0 / tau * (K  * (goals[curDim] - pos) - (D  * vel) + scaleFactor * force);
        curDim++;
    }

}


double MultiPhaseDMP::_getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const
{
    return (*_getFunctionApproximator(trajDim))(canonicalStates).at(0);
}





DVecMap MultiPhaseDMP::calcPerturbationForceSamples(unsigned int trajDim, const DVec2d& canonicalValues, const SampledTrajectoryV2& exampleTraj) const
{

    DVecMap result;


    if (exampleTraj.size() == 0)
    {
        throw Exception("Sample traj is empty");
    }

    double x0 = exampleTraj.begin()->getPosition(trajDim);
    double xT = exampleTraj.rbegin()->getPosition(trajDim);
    bool scaling = false;

    double tau = canonicalSystem->getTau();

    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();


    for (size_t i = 0; itTraj != exampleTraj.end() && i < canonicalValues.size();  ++i, itTraj++)
    {
        DVec canVals = canonicalValues[i];
        const double& pos = itTraj->getPosition(trajDim);
        const double& vel = itTraj->getDeriv(trajDim, 1);
        const double& acc = itTraj->getDeriv(trajDim, 2);


        double value = -K  * (xT - pos) +   D * vel  + tau * acc;
        if(scaling)
        {
            double multiCanVals = 1;
            for(size_t k = 0; k < canVals.size(); ++k)
            {
                multiCanVals *= canVals[k];
            }

            double denom = amplitudes[trajDim] * multiCanVals;

            if(denom < 1e-6)
                value = 1e6;
        }

        checkValue(value);
        result[canVals] =  value;


    }

    return result;
}



Vec<DMPState> MultiPhaseDMP::calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor)
{
    if (startPositions.size() == 0)
    {
        std::cout << "No Start Positions set, assuming current state as start position" << std::endl;
        setStartPositions(currentStates);
    }

    int canDim = canonicalSystemList.size();

    ODE::setDim(canDim + __trajDim * 2);


    DVec initialODEConditions = canonicalValues;
    DVec initialStatesVec = SystemStatus::convertStatesToArray(currentStates);
    initialODEConditions.insert(initialODEConditions.end(), initialStatesVec.begin(), initialStatesVec.end());

    setTemporalFactor(temporalFactor);
    setGoals(goal);


    DVec rawResult;
    ODE::integrate(t, tInit, initialODEConditions, rawResult, false);
//    std::cout << "time : " << t << " canonicalValues: " << canonicalValues[0] << " " << canonicalValues[1] << std::endl;

    canonicalValues.assign(rawResult.begin(), rawResult.begin() + canDim);
    rawResult.erase(rawResult.begin(), rawResult.begin() + canDim);
    Vec<DMPState> result = SystemStatus::convertArrayToStates<DMPState>(rawResult);


    return result;

}

void MultiPhaseDMP::setStartPositions(const Vec<DMPState> &initialStates)
{
    DVec tempStartPositions;

    for (unsigned int i = 0; i < initialStates.size(); i++)
    {
        tempStartPositions.push_back(initialStates[i].pos);
    }

    setStartPositions(tempStartPositions);
}

SampledTrajectoryV2 MultiPhaseDMP::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{

    setStartPositions(initialStates);

    return DMPInterfaceTemplate<DMPState>::calculateTrajectory(timestamps, goal, initialStates, initialCanonicalValues, temporalFactor);
}





double MultiPhaseDMP::evaluateReproduction(DVec initialCanonicalValue, double& maxErrorOut, bool showPlot)
{
    //    DVec timestamps;
    //    double meanError = 0.0;
    //    timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + getTrainingTraj(0).size()));
    //    DVec trainTimestamps = getTrainingTraj(0).getTimestamps();
    //    double goal = getTrainingTraj(0).rbegin()->getPosition(0);
    //    SampledTrajectoryV2 newTraj = calculateTrajectory(
    //                                      timestamps,
    //                                      goal,
    //                                      DMPState(getTrainingTraj(0).begin()->getPosition(0), getTrainingTraj(0).begin()->getDeriv(0, 1)),
    //                                      initialCanonicalValue);
    //    std::cout << "start: " << getTrainingTraj(0).begin()->getPosition(0) << " startVel: " << getTrainingTraj(0).begin()->getDeriv(0, 1) << " goal: " << goal << std::endl;

    //    DVec statesGen = newTraj.getDimensionData(0, 0);
    //    DVec statesOrig = getTrainingTraj(0).getDimensionData(0, 0);

    //    //    if(statesGen.size() != statesOrig.size())
    //    //        throw Exception("generated and original trajectories do not have the same sample count") << statesGen.size() << " vs. " << statesOrig.size();
    //    //    if(statesGen.size() != timestamps.size())
    //    //        throw Exception("trajectory timestamps and positions sample count do not match:") ;
    //    DoubleMap errorMap;
    //    double minTrajValue = std::numeric_limits<double>::max();
    //    double maxTrajValue = std::numeric_limits<double>::min();
    //    maxErrorOut = 0.0;

    //    for (unsigned int i = 0; i < statesGen.size() && i < statesOrig.size(); ++i)
    //    {
    //        const double error = fabs(statesGen[i] - statesOrig[i]);

    //        if (error > maxErrorOut)
    //        {
    //            maxErrorOut = error;
    //        }

    //        errorMap[timestamps[i]] = error;
    //        meanError += error;

    //        if (minTrajValue > statesOrig[i])
    //        {
    //            minTrajValue = statesOrig[i];
    //        }

    //        if (maxTrajValue < statesOrig[i])
    //        {
    //            maxTrajValue = statesOrig[i];
    //        }

    //    }

    //    if (showPlot)
    //    {
    //        Vec< SampledTrajectoryV2 > trajectories;

    //        trajectories.push_back(SampledTrajectoryV2::normalizeTimestamps(getTrainingTraj(0)));
    //        trajectories.push_back(newTraj);
    //        //        SampledTrajectory::plot(trajectories,"plots/trajectories_%d_dim%d.gp", true);
    //        plot(errorMap, "plots/traj_reproduction_error_%d.gp", true, 0);
    //        DoubleMap newTrajData;
    //        DVec timestamps = newTraj.getTimestamps();
    //        DVec positions = newTraj.getDimensionData(0, 0);

    //        Vec<DVec> plots;
    //        plots.push_back(timestamps);
    //        plots.push_back(getTrainingTraj(0).getDimensionData(0, 0));
    //        plots.push_back(newTraj.getDimensionData(0, 0));
    //        plotTogether(plots, "plots/trajComparison_%d.gp");
    //        plots.clear();
    //        plots.push_back(timestamps);
    //        plots.push_back(getTrainingTraj(0).getDimensionData(0, 1));
    //        plots.push_back(newTraj.getDimensionData(0, 1));
    //        plotTogether(plots, "plots/trajComparison_vel_%d.gp");
    //        plots.clear();
    //        plots.push_back(timestamps);
    //        plots.push_back(getTrainingTraj(0).getDimensionData(0, 2));
    //        plots.push_back(newTraj.getDimensionData(0, 2));
    //        plotTogether(plots, "plots/trajComparison_acc_%d.gp");
    //    }

    //    meanError /= statesOrig.size();
    //    meanError /= maxTrajValue - minTrajValue;
    //    maxErrorOut /= maxTrajValue - minTrajValue;
    double meanError = 0;
    return meanError;
}


void MultiPhaseDMP::setConfiguration(string paraID, const paraType& para){
    if (paraID == DMP::SpringFactor)
    {
        setSpringConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::DampingFactor)
    {
        setDampingConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::TemporalFactor)
    {
        setTemporalFactor(boost::get<double>(para));
    }
    else if (paraID == DMP::Goal)
    {
        setGoals(boost::get<DMP::DVec >(para));
    }
    else if (paraID == DMP::StartPosition)
    {
        setStartPositions(boost::get<DMP::DVec>(para));
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
}



void MultiPhaseDMP::showConfiguration(string paraID){
    if (paraID == DMP::SpringFactor)
    {
        std::cout << "K: " << K << std::endl;
    }
    else if (paraID == DMP::DampingFactor)
    {
        std::cout << "D: " << D << std::endl;
    }
    else if (paraID == DMP::TemporalFactor)
    {
        std::cout << "tau: " << canonicalSystem->getTau() << std::endl;
    }
    else if (paraID == DMP::Goal)
    {
        std::cout << "goals: " << goals << std::endl;
    }
    else if (paraID == DMP::StartPosition)
    {
        std::cout << "startPositions: " << startPositions << std::endl;
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
}

