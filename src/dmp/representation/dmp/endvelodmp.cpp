/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "endvelodmp.h"

#include <algorithm>

using namespace DMP;

EndVeloDMP::EndVeloDMP(int kernelSize, double D, double tau) :
    BasicDMP(kernelSize, D, tau)
{
}

void EndVeloDMP::learnFromTrajectories(const Vec<SampledTrajectoryV2 >&trainingTrajectories)
{
    if (trainingTrajectories.size() > 1)
    {
        throw Exception() << "this DMP can only be trained with one trajectory (but with several dimensions)";
    }

    if (trainingTrajectories.size() == 0)
    {
        return;
    }

    //    trainingTrajectories[0].plot(0,0,"plots/trainingTraj");
    //    trainingTrajectories[0].plot(0,1,"plots/trainingTraj_Vel");
    //    SampledTrajectory velocities;
    //    SampledTrajectory accelerations;
    //    trainingTrajectory.getVelocities(velocities);
    //    trainingTrajectory.getAccelerations(accelerations);
    //    trainingTrajectory.plot("plots/originalTraj_%d_dim%d.gp");
    //    velocities.plot("plots/originalTraj_velo_%d_dim%d.gp", true);
    //    accelerations.plot("plots/originalTraj_acc_%d_dim%d.gp");

    SampledTrajectoryV2 reducedTraj = removeEndVelocityFromTrajectory(trainingTrajectories[0]);

    //    reducedTraj.plot("plots/reduced_traj_%d_dim%d.gp", true);
    //    reducedTraj.getVelocities(velocities);
    //    velocities.plot("plots/reduced_traj_velo_%d_dim%d.gp", true);
    //    reducedTraj.getAccelerations(accelerations);
    //    accelerations.plot("plots/reduced_traj_acc_%d_dim%d.gp");
    BasicDMP::learnFromTrajectories(reducedTraj);
    originalTrainingTraj = trainingTrajectories[0];
    __trajDim = originalTrainingTraj.dim();
    //    learnedTraj = exampleTraj;
    //    normalizedLearnedTraj = normalizeTimestamps(exampleTraj);
}

Vec<DMPState> EndVeloDMP::calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<DMPState> &initialStates, DVec& canonicalValues, double temporalFactor)
{
    DVec newGoals;

    for (unsigned int dim = 0; dim < goal.size() && dim < trajOffset.dim(); dim++)
    {
        newGoals.push_back(goal[dim] - trajOffset.rbegin()->getPosition(dim) * offsetTrajScalingFactor[dim]);
    }

    Vec<DMPState> newInitialStates;

    for (unsigned int i = 0; i < initialStates.size() && i < trajOffset.dim(); i++)
    {
        DMPState newState;

        for (unsigned int d = 0; d < initialStates[i].size(); d++)
        {
            newState.getValue(d) = initialStates[i].getValue(d) - trajOffset.getState(tInit, i, d) * offsetTrajScalingFactor[i];
        }

        newInitialStates.push_back(newState);

    }



    Vec<DMPState>   newStates =
        BasicDMP::calculateTrajectoryPoint(t, newGoals, tInit, newInitialStates, canonicalValues, 1);
    reducedGeneratedTraj.setPositionEntry(t, 0, newStates[0].pos);

    Vec<DMPState>   resultingPositions;

    // apply offset trajectory to new generated trajectory

    for (unsigned int dim = 0; dim < newStates.size(); dim++)
    {
        DMPState state;
        state.pos = newStates[dim].pos + trajOffset.getState(t, dim, 0) * offsetTrajScalingFactor[dim];
        state.vel = newStates[dim].vel + trajOffset.getState(t, dim, 1) * offsetTrajScalingFactor[dim];
        resultingPositions.push_back(state);
    }





    return resultingPositions;

}

SampledTrajectoryV2 EndVeloDMP::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{

    if (goal.size() != __trajDim)
    {
        throw Exception() << "Number of goals does not match the trajectory dimensionality:  goals:" << goal.size() << " traj. dim.: " << __trajDim;
    }

    if (initialStates.size() != __trajDim)
    {
        throw Exception() << "Number of initial states does not match the trajectory dimensionality:  states:" << initialStates.size() << " traj. dim.: " << __trajDim;
    }


    unsigned int dim = __trajDim;
    offsetTrajScalingFactor.resize(dim);

    for (unsigned int d = 0; d < dim; d++)
    {
        double diffOrig = getTrainingTraj(0).rbegin()->getPosition(d) - getTrainingTraj(0).begin()->getPosition(d);
        double diffGenerated = goal.at(d) - initialStates.at(d).pos;
        //        double diffOffset = (*trajOffset.rbegin()).data[d]->pos - (*trajOffset.begin()).data[d]->pos;
        offsetTrajScalingFactor[d] = fabs(diffGenerated / diffOrig);
    }


    // reduce start position by offset trajectory (extra setter because the same values are used for the whole trajectory)
    DVec newStartPositions;


    for (unsigned int d = 0; d < initialStates.size() && d < trajOffset.dim(); d++)
    {
        newStartPositions.push_back(initialStates[d].pos - trajOffset.getState(*timestamps.begin(), d) * offsetTrajScalingFactor[d]);
    }

    setStartPositions(newStartPositions);

    return DMPInterfaceTemplate<DMPState>::calculateTrajectory(timestamps, goal, initialStates, initialCanonicalValues, temporalFactor);
}


SampledTrajectoryV2 EndVeloDMP::removeEndVelocityFromTrajectory(const SampledTrajectoryV2& trajectory)
{
    //    const double endVel = *trajectory.getVelocity(0).rbegin();
    //    const double endAcc = *trajectory.getAcceleration(0).rbegin();
    SampledTrajectoryV2 normalizedExampleTraj = SampledTrajectoryV2::normalizeTimestamps(trajectory);
    normalizedExampleTraj.differentiateDiscretly(1);
    normalizedExampleTraj.plot(0, 0, "plots/trainingTraj");
    normalizedExampleTraj.plot(0, 1, "plots/trainingTraj_Vel");

    DVec timestamps  = normalizedExampleTraj.getTimestamps();
    SampledTrajectoryV2 reducedTraj;
    trajOffset = SampledTrajectoryV2();

    for (unsigned int dim = 0; dim < normalizedExampleTraj.dim(); dim++)
    {

        double b, c;
        calcVelocityReductionFunctionParameters(normalizedExampleTraj, dim, b, c);

        // antiderivative of r(x) is: R(x) = b/2*x^2 + c/3 * x^3
        // (r(x) is velocity, so R(x) is position, reduce traj about R(x)
        //        DVec trajOffsets;
        //        DVec positionSamples;
        //    DVec reducedTrajValues;

        //        SampledTrajectory velocities;
        //        SampledTrajectory accelerations;
        //        normalizedExampleTraj.getVelocities(velocities);
        //        normalizedExampleTraj.getAccelerations(accelerations);

        //        Vec<DMPState> positions = normalizedExampleTraj.getDimensionData(dim);
        unsigned int size = normalizedExampleTraj.size();
        //    DVec newAccelerations;
        //    DVec offsetAccelerations;
        DVec newVelocities;
        DVec offsetVelocities;
        //        Vec<DMPState> origStates = normalizedExampleTraj.getDimensionData(dim);
        SampledTrajectoryV2::ordered_view::iterator it = normalizedExampleTraj.begin();

        for (; it != normalizedExampleTraj.end(); ++it)
        {

            const double t =  it->getTimestamp();
            double pos = normalizedExampleTraj.getState(t, dim);
            double vel = normalizedExampleTraj.getState(t, dim, 1);
            double veloValue = b * t + c * t * t;
            double accValue = b + 2 * c * t;
            double positionOffset = b / 2 * t * t + c / 3 * pow(t, 3.0);
            double reducedTrajValue = pos - positionOffset;

            //        offsetAccelerations.push_back(accValue);
            //        newAccelerations.push_back(accelerations[i].at(1) - accValue);

            offsetVelocities.push_back(veloValue);
            newVelocities.push_back(vel - veloValue);

            //        trajOffsets.push_back(positionOffset);
            //        reducedTrajValues.push_back(reducedTrajValue);
        }



        //    SampledTrajectory reducedTraj(timestamps, newAccelerations, *positionSamples.begin(), velocities.begin()->at(1));

        const double t =  normalizedExampleTraj.rbegin()->getTimestamp();
        double trajOffsetStart = b / 2 * t * t + c / 3 * pow(t, 3.0);

        trajOffset.addDimension(timestamps, offsetVelocities);
        trajOffset.reconstructFromDerivativeForDim(trajOffsetStart, dim, 1, 0);
//        trajOffset.plot(dim, 0, "plots/offsetTraj");
//        trajOffset.plot(dim, 1, "plots/offsetTraj_vel");

        reducedTraj.addDimension(timestamps, newVelocities);
        reducedTraj.reconstructFromDerivativeForDim(normalizedExampleTraj.begin()->getPosition(dim) - trajOffsetStart, dim, 1, 0);
        //        Vec<DMPState> reducedStates = reducedTraj.getDimensionData(dim, 0);
//        reducedTraj.plot(0, 0, "plots/reducedTraj");
//        reducedTraj.plot(0, 1, "plots/reducedTraj_vel");
        //double x;
        //x = 5;
    }
    return reducedTraj;

}

void EndVeloDMP::calcVelocityReductionFunctionParameters(SampledTrajectoryV2& traj, unsigned int trajDimension, double& b, double& c)
{
    unsigned int size = traj.size();
    const double endTime = traj.rbegin()->getTimestamp();
    const double endVel = traj.rbegin()->getDeriv(trajDimension, 1);
    const double endAcc = traj.getDiscretDifferentiationForDimAtT(endTime, trajDimension, 1);


    // velocity reduction function : r(x) = b*x + c*x^2
    // calculating the solution for b and c with r'(end) = endAcc && r(end) = endVelo
    c = (endVel - endAcc * endTime) / (-endTime * endTime);

    b = endAcc - 2 * c * endTime;
}
