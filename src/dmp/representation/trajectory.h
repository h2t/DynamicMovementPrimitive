#ifndef TRAJECTORY_H
#define TRAJECTORY_H

#include "../general/helpers.h"
#include "../general/vec.h"

#include "systemstatus.h"

#include <boost/tokenizer.hpp>
#include <boost/unordered_map.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/typeof/typeof.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/split_member.hpp>

#include <Eigen/Core>


#include <fstream>
#include <cmath>
//#include <omp.h>




namespace DMP
{
    enum INTERPOLATION_METHOD
    {
        IM_LINEAR
    };



    //    struct TrajectoryEntry
    //    {
    //        double get(int dim);
    //    private:
    //        DVec entries;
    //    };

    typedef boost::shared_ptr<DVec> DVecPtr;

    class SampledTrajectoryV2
    {
    public:
        typedef std::map<double, Vec< DVecPtr > > TrajMap;


        struct TrajData
        {
            TrajData(SampledTrajectoryV2& traj);
            DVecPtr operator[](unsigned int dim)
            {
                return data.at(dim);
            }
            inline double getTimestamp() const
            {
                return timestamp;
            }
            inline double getPosition(int dim) const
            {
                return getDeriv(dim, 0);
            }
            inline double getDeriv(int dim, int derivation) const
            {
                return trajectory.getState(timestamp, dim, derivation);
            }
            inline const Vec< DVecPtr >& getData() const
            {
                return data;
            }

            double timestamp;
            mutable Vec< DVecPtr > data; // mutable so that it can be changed
            SampledTrajectoryV2& trajectory;

            friend class SampledTrajectoryV2;

        };

        // structs for indices
        struct TagTimestamp {};
        struct TagOrdered {};


        // Trajectory data container
        typedef boost::multi_index::multi_index_container <
        TrajData,
        boost::multi_index::indexed_by <
        boost::multi_index::hashed_unique<boost::multi_index::tag<TagTimestamp>, boost::multi_index::member<TrajData, double, &TrajData::timestamp> >,
        boost::multi_index::ordered_unique<boost::multi_index::tag<TagOrdered>, boost::multi_index::member<TrajData, double, &TrajData::timestamp> > // this index represents timestamp incremental order
        >
        > TrajDataContainer;
        typedef typename boost::multi_index::index<TrajDataContainer, TagTimestamp>::type timestamp_view;
        typedef typename boost::multi_index::index<TrajDataContainer, TagOrdered>::type ordered_view;



        SampledTrajectoryV2() {}
        SampledTrajectoryV2(const SampledTrajectoryV2& source);
        SampledTrajectoryV2(const DVec& x, const DVec& y);
        SampledTrajectoryV2(const std::map<double, DVec> &data);
        template <typename SystemStatus>
        SampledTrajectoryV2(const std::map<double, Vec<SystemStatus> > &data);
        SampledTrajectoryV2(const std::vector<double> &data, double timestep = 0.001);

        virtual ~SampledTrajectoryV2(){}

        SampledTrajectoryV2& operator=(const SampledTrajectoryV2& source);


        unsigned int addDimension(const DVec& x, const DVec& y);
        void addPositionsToDimension(unsigned int dimension, const DVec& x, const DVec& y);
        void addDerivationsToDimension(unsigned int dimension, const double t, const DVec& derivs);
        SampledTrajectoryV2& operator+=(const SampledTrajectoryV2 traj);
        void removeDimension(unsigned int dimension);
        void removeDerivation(unsigned int deriv);
        void removeDerivation(unsigned int dimension, unsigned int deriv);

        // quaternion related
        SampledTrajectoryV2 traj2qtraj(int mode = ROTATION_ORDER_ZYX) const;
        SampledTrajectoryV2 qtraj2traj(int mode = ROTATION_ORDER_ZYX) const;
        SampledTrajectoryV2 traj2dqtraj(int mode = ROTATION_ORDER_ZYX) const;
        SampledTrajectoryV2 dqtraj2traj(int mode = ROTATION_ORDER_ZYX) const;

        // iterators
        typename ordered_view::const_iterator begin() const
        {
            return dataMap.get<TagOrdered>().begin();
        }
        typename ordered_view::const_iterator end() const
        {
            return dataMap.get<TagOrdered>().end();
        }
        typename ordered_view::const_reverse_iterator rbegin() const
        {
            return dataMap.get<TagOrdered>().rbegin();
        }
        typename ordered_view::const_reverse_iterator rend() const
        {
            return dataMap.get<TagOrdered>().rend();
        }

        ///// getters
        DVec getTimestamps() const;
        const Vec<DVecPtr> &getStates(double t);
        Vec<DVecPtr> getStates(double t) const;
        DVec getStates(double t, unsigned int derivation) const;

        double getState(double t, unsigned int dim = 0, unsigned int deriv = 0);
        double getState(double t, unsigned int dim = 0, unsigned int deriv = 0) const;
        DVec getDerivations(double t, unsigned int dimension, unsigned int derivations)  const;

        TrajDataContainer& data(){ return dataMap;}

        /**
         * @brief getDimensionData gets all entries for one dimensions with order of increasing timestamps
         * @param dimension
         * @return
         */
        //        DVec getDimensionData(unsigned int dimension = 0) const;
        DVec getDimensionData(unsigned int dimension, unsigned int deriv = 0) const;
        Eigen::VectorXd getDimensionDataAsEigen(unsigned int dimension, unsigned int derivation) const;
        Eigen::VectorXd getDimensionDataAsEigen(unsigned int dimension, unsigned int derivation, double startTime, double endTime) const;
        Eigen::MatrixXd toEigen(unsigned int derivation, double startTime, double endTime) const;
        Eigen::MatrixXd toEigen(unsigned int derivation = 0) const;
        SampledTrajectoryV2 getPart(double startTime, double endTime, unsigned int numberOfDerivations = 0) const;
        /**
         * @brief dim returns the trajectory dimension count for this trajectory (e.g. taskspace w/o orientation would be 3)
         * @return
         */
        unsigned int dim() const;
        unsigned int size() const
        {
            return dataMap.size();
        }
        double getTimeLength() const;
        double getSpaceLength(unsigned int dimension, unsigned int stateDim) const;
        double getSpaceLength(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        double getSquaredSpaceLength(unsigned int dimension, unsigned int stateDim) const;
        double getSquaredSpaceLength(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        double getMax(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        double getMin(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        double getWithFunc(const double& (*foo)(const double&, const double&), double initValue, unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        double getTrajectorySpace(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        DVec getMinima(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        DVec getMaxima(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        DVec getMinimaPositions(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        DVec getMaximaPositions(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const;
        DVec getAnchorPoint() const;
        DVec calcAnchorPoint();
        std::map<double, DVec> getPositionData() const;

        SampledTrajectoryV2 getSegment(double startTime, double endTime);
        std::vector<SampledTrajectoryV2 > getSegments(int n);

        double getPeriodLength() const {return m_periodLength;}
        double getTransientLength() const {return m_transientLength;}

        SampledTrajectoryV2 normalizeTraj(double min = 0.0, double max = 1.0);
        // calculations

        DVec getDiscretDifferentiationForDim(unsigned int trajDimension, unsigned int derivation) const;
        static DVec DifferentiateDiscretly(const DVec& timestamps, const DVec& values, int derivationCount = 1);
        double getDiscretDifferentiationForDimAtT(double t, unsigned int trajDimension, unsigned int deriv) const;

        void differentiateDiscretlyForDim(unsigned int trajDimension, unsigned int derivation);
        void differentiateDiscretly(unsigned int derivation);
        //        void differentiateDiscretly( unsigned int sourceDimOfSystemState, unsigned int targetDimOfSystemState);

        void reconstructFromDerivativeForDim(double valueAtFirstTimestamp, unsigned int trajDimension, unsigned int sourceDimOfSystemState, unsigned int targetDimOfSystemState);

        /**
         * @brief negateDim changes the sign of all values of the given dimension.
         */
        void negateDim(unsigned int trajDimension);
        /**
         * @brief gaussianFilter smoothes the trajectory
         * @param filterRadius, in easy words: time range to left and right from center that
         * influences the resulting center value
         */
        void gaussianFilter(double filterRadius);

        static SampledTrajectoryV2 normalizeTimestamps(const SampledTrajectoryV2& traj, const double startTime = 0.0,  const double endTime = 1.0);
        static DVec normalizeTimestamps(const DVec& timestamps, const double startTime = 0.0,  const double endTime = 1.0);
        static DVec generateTimestamps(double startTime = 0.0, double endTime = 1.0, double stepSize = 0.001);
        void shiftTime(double shift);
        void shiftValue(const DVec& shift);

        void saveToFile(std::string filename);
        // serialization
        friend class boost::serialization::access;
        template<class Archive>
        void save(Archive& ar, const unsigned int file_version) const
        {

            unsigned int dimensions = dim();
            ar& BOOST_SERIALIZATION_NVP(dimensions);
            std::vector<double> times = getTimestamps();
            ar& BOOST_SERIALIZATION_NVP(times);

            for (size_t t = 0; t < times.size(); t++)
            {
                const Vec<DVecPtr> tData = getStates(times.at(t));

                for (size_t d = 0; d < dimensions; d++)
                {
                    std::vector<double>& derivations = *tData.at(d);
                    ar& BOOST_SERIALIZATION_NVP(derivations);
                }
            }
        }

        template<class Archive>
        void load(Archive& ar, const unsigned int file_version)
        {

            unsigned int dimensions;
            ar& BOOST_SERIALIZATION_NVP(dimensions);
            std::vector<double> times;
            ar& BOOST_SERIALIZATION_NVP(times);

            for (size_t t = 0; t < times.size(); t++)
            {
                for (size_t d = 0; d < dimensions; d++)
                {
                    DVec derivs;
                    std::vector<double> &derivations = derivs;
                    ar& BOOST_SERIALIZATION_NVP(derivations);
                    addDerivationsToDimension(d, times.at(t), derivs);
                }

            }
        }
        BOOST_SERIALIZATION_SPLIT_MEMBER()

        // misc
        double m_phi1;
        double m_periodLength;
        double m_transientLength;
        DVec m_anchorPoint;
        void plot(unsigned int trajDim, unsigned int derivation, string filepathWithoutEnding = "plots/trajectory", bool show = true) const;
        void plotAllDimensions(unsigned int stateDim, string filepathWithoutEnding = "plots/trajectory", bool show = true) const;
        void plotAllDimensions(unsigned int stateDim, std::vector<string> dimNameMap, string filepathWithoutEnding = "plots/trajectory", bool show = true) const;

        //        typedef boost::unordered::unordered_map<double, Vec< DVecPtr > > TrajMap;
        void readFromCSVFileWithQuery(const std::string& filepath, DVec& query, bool withHead = true, int derivations = 0);
        void readFromCSVFile(const std::string& filepath, bool noTimeStamp=false, int derivations = 0, int columnsToSkip = 0, bool containsHeader = true);
        void writeToCSVFile(const std::string& filepath, bool noTimeStamp=true, char delimiter=',');
        void writeTrajToCSVFile(const std::string& filepath, float timestep=0.001,float startTime=0, float endTime=1.0);

        static void CopyData(const SampledTrajectoryV2& source, SampledTrajectoryV2& destination);
        void clear()
        {
            dataMap.erase(dataMap.begin(), dataMap.end());
        }
        void setPositionEntry(const double t, const unsigned int dimIndex, const double& y);
        void setEntries(const double t, const unsigned int dimIndex, const DVec& y);
        bool dataExists(double t, unsigned int dimension = 0, unsigned int deriv = 0) const;
        double getPhi1() const{return m_phi1;}
        void setPhi1(double phi_new){m_phi1 = phi_new;}

        void setAnchorPoint(DVec newAnchorPoint){ m_anchorPoint.push_back(0.0);m_anchorPoint.push_back(0.0);}
        void setPeriodLength(double newPeriodLength){ m_periodLength = newPeriodLength;}
        void setTransientLength(double newTransientLength){ m_transientLength = newTransientLength;}

        std::map<double, DVec> getStatesAround(double t, unsigned int derivation, unsigned int extend) const;

        std::vector<std::string> getDimensionNames()
        {
            return dimNames;
        }

        std::string writeTrajToCSVString(int numOfSamples);

    protected:
        void __addDimension();
        void __fillAllEmptyFields();
        double __interpolate(typename ordered_view::const_iterator itMap, unsigned int dimension, unsigned int deriv) const;
        double __gaussianFilter(double filterRadius , typename ordered_view::iterator centerIt , unsigned int trajNum, unsigned int dim);
        timestamp_view::iterator __fillBaseDataAtTimestamp(const double& t);
        Vec<DVecPtr> __calcBaseDataAtTimestamp(const double& t) const;


        std::string headerString;
        TrajDataContainer dataMap;

        std::vector<std::string> dimNames;

        //        //! Counts the dimensions that were inserted into this trajectory.
        //        unsigned int dimensions;

        virtual double __interpolate(double t, ordered_view::const_iterator itPrev, ordered_view::const_iterator itNext, unsigned int dimension, unsigned int deriv) const;
        double __interpolate(double t, unsigned int dimension, unsigned int deriv) const;
    };

    class SampledTrajectory : public Vec< DVec >
    {
    public:

        SampledTrajectory();
        SampledTrajectory(const DVec& x, const DVec& y);
        SampledTrajectory(const DVec& x, const DVec& velo, double startPosition);
        SampledTrajectory(const DVec& x, const DVec& acc, double startPosition, double startVelocity);

        // TODO
        //    DVec evaluate(double t, INTERPOLATION_METHOD interpolation_method=IM_LINEAR) const;
        //    void evaluate(DVec ts, Vec< DVec > &out, INTERPOLATION_METHOD interpolation_method=IM_LINEAR) const;

        void getPositions(SampledTrajectory& pos) const;
        /**
         * @brief getVelocities returns a trajectory containing the velocities instead of the positions.
         * The timestamps are included.
         * @param vel
         */
        void getVelocities(SampledTrajectory& vel) const;
        void getAccelerations(SampledTrajectory& acc) const;

        // TODO: make more efficient
        void getPosition(unsigned int i, DVec& pos) const;
        void getVelocity(unsigned int i, DVec& vel) const;
        void getAcceleration(unsigned int i, DVec& accel) const;


        DVec getPosition(unsigned int i) const;
        /**
         * @brief getVelocity returns the velocity
         * @param i is the index of the samples
         * @return a Vec, from 0 to dim-1
         */
        DVec getVelocity(unsigned int i) const;
        DVec getAcceleration(unsigned int i) const;

        void differentiateDiscretly(const SampledTrajectory& samples, SampledTrajectory& res) const;

        void timeSamples(DVec& times) const;
        void positionSamples(Vec< DVec > &positions) const;
        void positionSamples(int dim, DVec& positions) const;

        void getPositionsDoubleArray(double** &res) const;
        void getVelocitiesDoubleArray(double** &res) const;

        /**
         * @brief gaussianFilter smoothes the trajectory
         * @param filterRadius, number of items to left and right from center that
         * influence this value
         */
        void gaussianFilter(unsigned int filterRadius);

        void writeGNUPlotFile(const char* filename, unsigned int num = 0) const;

        // TODO: clearify interface! what is the best order of parameters? maybe overload it...
        void plot(const char* tmp_filename = "plots/tmp_traj_num%d_dim%d.gp", bool show = true, unsigned int num = 0) const;
        void plotOverPhi(double phi0, double Omega, const char* tmp_filename = "plots/tmp_traj_num%d_dim%d.gp", bool show = true, unsigned int num = 0) const;
        static void plot(const Vec<SampledTrajectory> &data, const char* tmp_filename = "plots/tmp_double_array_traj_%d.gp",
                         bool show = true);

        void showMultiplePlots(unsigned int min_num, unsigned int max_num, unsigned int step = 1,
                               const char* filename = "plots/tmp_traj_num%d_dim%d.gp") const;

        void showParametricPlot(const char* filename = "plots/tmp_traj_num%d_dim%d.gp",
                                unsigned int min_num = 0, unsigned int max_num = 0, unsigned int step = 1) const;

        unsigned int dim() const;

        void shiftTime(double shift);
        void shiftValue(const DVec& shift);



    private:
        double __gaussianFilter(unsigned int filterRadius , unsigned int centerIndex , int dim);

    };


    //class SampledPeriodicTrajectory : public SampledTrajectory {
    //public:
    //    double m_phi1, m_period_length;
    //  DVec m_anchor_point;

    //    SampledPeriodicTrajectory() : SampledTrajectory() {}
    //  SampledPeriodicTrajectory(const SampledPeriodicTrajectory &src);
    //  SampledPeriodicTrajectory(double phi1, double periodLength,
    //                              double anchor_point=0.0, Vec< DVec > *samples=NULL);
    //  SampledPeriodicTrajectory(double phi1, double periodLength,
    //                              const DVec *anchor_point=NULL, Vec< DVec > *samples=NULL);

    //    virtual double T() const { return 0; }
    //    // TODO: this is not elegant! Maybe let SampledPeriodicTrajectory be an instance
    //    // of SampledComposedPeriodicTrajectory with m_T=0

    //    double omega() const { return 2*M_PI/m_period_length; }

    //    double phi1() const { return m_phi1; }
    //    void setPhi1(double phi1) { m_phi1 = phi1; }
    //  double periodLength() const { return m_period_length; }
    //  DVec anchorPoint() const { return m_anchor_point; }

    //    double getShift(const SampledPeriodicTrajectory &traj) const;
    //    double getShiftError(const SampledPeriodicTrajectory &traj, double shiftTime) const;

    //    double align(const SampledPeriodicTrajectory &reference_traj);

    //  void plot(bool highlight_period_length=true, const char *tmp_filename="plots/tmp_traj_num%d_dim%d.gp",
    //              bool show=true, unsigned int num=0) const;
    //};


    class SampledComposedPeriodicTrajectory : public SampledTrajectory
    {
    public:
        double m_T, m_phi1, m_period_length;
        DVec m_anchor_point;

        SampledComposedPeriodicTrajectory() : SampledTrajectory() {}
        SampledComposedPeriodicTrajectory(const SampledComposedPeriodicTrajectory& src);
        SampledComposedPeriodicTrajectory(const SampledTrajectoryV2 & src);
        SampledComposedPeriodicTrajectory(double T, double phi1, double periodLength,
                                          double anchor_point = 0.0, Vec< DVec > *samples = NULL);
        SampledComposedPeriodicTrajectory(double T, double phi1, double periodLength,
                                          const DVec* anchor_point = NULL, Vec< DVec > *samples = NULL);

        double T() const
        {
            return m_T;
        }
        void setT(double T)
        {
            m_T = T;
        }

        //   void getTransientPart(SampledTrajectory &transient_part) const; // TODO
        void getPeriodicPart(SampledComposedPeriodicTrajectory& periodic_part, bool shift_to_time_0 = false) const;

        double omega() const
        {
            return 2 * M_PI / m_period_length;
        }

        double phi1() const
        {
            return m_phi1;
        }
        void setPhi1(double phi1)
        {
            m_phi1 = phi1;
        }
        double periodLength() const
        {
            return m_period_length;
        }
        DVec anchorPoint() const
        {
            return m_anchor_point;
        }

        double getShift(const SampledComposedPeriodicTrajectory& traj) const;
        double getShiftError(const SampledComposedPeriodicTrajectory& traj, double shiftTime) const;

        double align(const SampledComposedPeriodicTrajectory& reference_traj);

        void plot(bool highlight_entrance_time = true, bool highlight_period_length = true,
                  const char* tmp_filename = "plots/tmp_traj_num%d_dim%d.gp", bool show = true,
                  unsigned int num = 0) const;

        DVec linearInterpolation(double t);

        static double linearInterpolationSingle(double t1, double y1, double t2, double y2, double t);
        static DVec linearInterpolationVec(double t1, const DVec &y1, double t2, const DVec &y2, double t);
        static DVec linearInterpolationBase(const DVec &sample1, const DVec &sample2, double t);
    };




    template <typename SystemStatus>
    SampledTrajectoryV2::SampledTrajectoryV2(const std::map<double, Vec<SystemStatus> > &data)
    {
        if (data.size() == 0)
        {
            return;
        }

        typename  std::map<double, Vec<SystemStatus> >::const_iterator it = data.begin();

        for (; it != data.end(); it++)
        {
            TrajData dataEntry(*this);
            dataEntry.timestamp = it->first;
            const Vec<SystemStatus>& dataVec = it->second;

            for (unsigned int i = 0; i < dataVec.size(); i++)
            {

                dataEntry.data.push_back(DVecPtr(new DVec(dataVec.at(i).getValues())));
            }

            dataMap.insert(dataEntry);
        }
    }






}

#endif // TRAJECTORY_H
