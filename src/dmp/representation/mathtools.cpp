#include "mathtools.h"


vec DMP::MathTools::mvrnorm(const vec &mu, const mat &sigma)
{
    int nrows = sigma.n_cols;
    vec Y;
    Y.randn(nrows);

    mat csigma = sigma;
    if(det(csigma)==0)
    {
        mat eps(size(csigma), fill::eye);
        csigma = csigma + eps * 1e-4;
    }

    vec res = mu + arma::chol(csigma) * Y;
    return res;
}

DMP::DVec DMP::MathTools::vec2dvec(const vec &v)
{
    DVec res;
    for(int i = 0; i < v.n_rows; ++i)
    {
        res.push_back(v.at(i));
    }
    return res;
}

