/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef CANONICALSYSTEM_H
#define CANONICALSYSTEM_H

#include <dmp/representation/basicode.h>
#include <dmp/general/helpers.h>
#include <dmp/functionapproximation/dmp2dperturbationapproximation.h>
#include <boost/shared_ptr.hpp>

namespace DMP
{
    class LinearDecayCanonicalSystem;

    class CanonicalSystem : public ODE
    {
    public:
        CanonicalSystem(double tau = 0.0, double Min = 0.001, double initialXValue = 0.0, double initialYValue = 1.0);
        void flow(double t, const DVec& x, DVec& out) override;

        std::string getName(){return "Exponential Decay";}
        DVec getInitValues();
        double getAlpha()
        {
            return alpha;
        }
        double getUMin()
        {
            return uMin;
        }
        double getTau() const
        {
            return tau;
        }
        void setTau(double tau)
        {
            this->tau = tau;
        }

        virtual double getAmpl(){return 1;}


        virtual void setAmpl(double r){}

        virtual double getPhaseStopValue() {return phaseStopValue;}

        virtual void setPhaseStopValue(double pv) {phaseStopValue = pv;}

        DVec getCanonicalValue(double t);

    protected:

        double phaseStopValue;
    private:
        /**
         * @brief uMin is value of convergence of the canonical system
         */
        double uMin;
        /**
         * @brief tau is the temporal factor.
         */
        double tau;

        /**
         * @brief alpha is a factor for the ODE, that determines the relation
         * between u and u'.
         */
        double alpha;

        double initialXValue;
        double initialYValue;
        /**
         * @brief initialXValue is value of the canonical system at t = initialXValue
         * @see initialXValue
         */

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& boost::serialization::make_nvp("phaseStopValue",phaseStopValue);
            ar& boost::serialization::make_nvp("uMin",uMin);
            ar& boost::serialization::make_nvp("tau",tau);
            ar& boost::serialization::make_nvp("alpha",alpha);
            ar& boost::serialization::make_nvp("initialXValue",initialXValue);
            ar& boost::serialization::make_nvp("initialYValue",initialYValue);

            auto& base = boost::serialization::base_object<ODE>(*this);
            ar& boost::serialization::make_nvp("CanonicalSystem_base",base);
        }

    };
    typedef boost::shared_ptr<CanonicalSystem> CanonicalSystemPtr;
    typedef DMP::Vec<CanonicalSystemPtr> CanonicalSystemList;


    class LinearCanonicalSystem : public CanonicalSystem
    {
    public:
        LinearCanonicalSystem(double tau = 0.0);
        std::string getName(){return "Linear";}

        void flow(double t, const DVec& x, DVec& out) override;
        void setSlowDown(bool on)
        {
            slowDown = on;
        }

    private:
        bool slowDown;

    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            auto& base = boost::serialization::base_object<CanonicalSystem>(*this);
            ar& boost::serialization::make_nvp("LinearCanonicalSystem_base",base);
        }
    };

    class LinearDecayCanonicalSystem : public CanonicalSystem
    {
    public:
        LinearDecayCanonicalSystem(double tau = 0.0, double uMin = 1e-8);
        std::string getName(){return "Linear Decay";}

        void flow(double t, const DVec& x, DVec& out) override;
        double step(double canVal, double deltaT);
    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            auto& base = boost::serialization::base_object<CanonicalSystem>(*this);
            ar& boost::serialization::make_nvp("LinearDecayCanonicalSystem_base",base);
        }
    };
    typedef boost::shared_ptr<LinearDecayCanonicalSystem> LinearDecayCanonicalSystemPtr;


    class LinearSigmoidalCanonicalSystem : public CanonicalSystem
    {
    public:
        LinearSigmoidalCanonicalSystem(double tau = 0.0);
        void flow(double t, const DVec& x, DVec& out) override;

    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            auto& base = boost::serialization::base_object<CanonicalSystem>(*this);
            ar& boost::serialization::make_nvp("LinearSigmoidCanonicalSystem_base",base);
        }
    };


    class SolvedCanonicalSystem : public CanonicalSystem
    {
    public:
        SolvedCanonicalSystem(double tau = 0.0, double uMin = 0.001, double initialXValue = 0.0, double initialYValue = 1.0);
        void flow(double t, const DVec& x, DVec& out) override;
        double inverse(const double u);
    private:
        double odeConstant;

    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            auto& base = boost::serialization::base_object<CanonicalSystem>(*this);
            ar& boost::serialization::make_nvp("SolvedCanonicalSystem_base",base);
        }
    };

    //ToDo: add periodicCanonicalSystem
    class PeriodicCanonicalSystem : public CanonicalSystem
    {
    public:
        PeriodicCanonicalSystem(double tau = 0.0, double r = 0.0, double uMin = 0.001, double initialXValue = 0.0, double initialYValue = 0.0) :
            CanonicalSystem(tau,uMin,initialXValue,initialYValue),
            r(r)
        {
        }
        double getAmpl() override
        {
            return r;
        }

        void setAmpl(double r) override
        {
            this->r = r;
        }

        void flow(double t, const DVec &x, DVec &out) override;

    private:
        double r;


    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            auto& base = boost::serialization::base_object<CanonicalSystem>(*this);
            ar& boost::serialization::make_nvp("PeriodicCanonicalSystem_base",base);
        }
    };


    class PeriodicDiscreteCanonicalSystem : public ODE
    {
    public:
        double m_alpha, m_beta, m_eta;
        double m_Omega, m_mu, m_mu1;

        PeriodicDiscreteCanonicalSystem(double Omega = M_PI,
                                        double alpha = 1.0 / 6, double beta = 1.0 / 1000, double eta = 35,
                                        double mu = 1.0, double mu1 = 1.2);

        double getMu1() const
        {
            return m_mu1;
        }
        double getOmega() const
        {
            return m_Omega;
        }
        double getMu() const
        {
            return m_mu;
        }

        void flow(double t, const DVec& x, DVec& out) override;

        double explicitSolutionForR(double t, const DVec& init);
        double explicitSolutionForPhi(double t, const DVec& init);

        double getTransientLengthForInitialR(double r0, double upper_limit = 10.0);
        void getInitialConditionsForTransientLength(double T, double phi1, DVec& init, double limit_r = -1.0);

        friend ostream& operator << (ostream& out, const PeriodicDiscreteCanonicalSystem& osc);

    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            auto& base = boost::serialization::base_object<CanonicalSystem>(*this);
            ar& boost::serialization::make_nvp("PeriodicDiscreteCanonicalSystem_base",base);
        }
    };


}
#endif // CANONICALSYSTEM_H
