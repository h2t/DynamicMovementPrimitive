#ifndef VICONCSVPARSER_H
#define VICONCSVPARSER_H
#include <dmp/representation/trajectory.h>
#include <dmp/general/helpers.h>



namespace DMP
{
    typedef std::vector<std::string> StringList;
    class ViconCSVParser
    {
    public:
        typedef StringList Row;

        ViconCSVParser();
        void parse(std::string filepath, int lineCount = -1);

        const std::string& get(unsigned int rowIndex, unsigned int columnIndex) const;
        StringList getColumn(int columnIndex) const;
        const Row& getRow(int rowIndex) const;
    protected:

        std::string filepath;
        std::vector< Row > rows;
        std::string emptyString;
    };

    class ViconTrajectoryCSVParser :
        public ViconCSVParser
    {
    public:
        static DVec StringVecToDoubleVec(const StringList& strings);
        void getTrajectory(std::string markerName, SampledTrajectoryV2& result) const;
        int getColumnOfMarker(const std::string& markerName) const;
        std::vector<double> getFrames() const;
        int getEndOfColumn(const StringList& column, int offset) const;
    };
}



#endif // VICONCSVPARSER_H
