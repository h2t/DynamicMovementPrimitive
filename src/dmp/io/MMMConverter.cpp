/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#include "MMMConverter.h"
#include <Eigen/Geometry>

#include "VirtualRobot/VirtualRobot.h"
#include "VirtualRobot/IK/IKSolver.h"
#include "VirtualRobot/IK/GenericIKSolver.h"
#include "MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h"
#include <ios>
#include <sstream>
#include <algorithm>
#include <exception>


namespace DMP {

using namespace MMM;




SampledTrajectoryV2 MMMConverter::fromMMMJoints(MotionPtr motion)
{
    //an empty set will return all dimensions that are being saved in the MMM trajectory file
    std::vector<string> empty;
    return fromMMMJoints(motion, empty, std::numeric_limits<float>::min(), std::numeric_limits<float>::max());
}

SampledTrajectoryV2 MMMConverter::fromMMMJoints(MotionPtr motion, std::vector<string> wantedDimensions, float t_start, float t_stop)
{
    if (!motion->hasSensor("Kinematic"))
    {
        throw std::ios_base::failure("could not convert motion to dmp since it does not have a Kinematic sensor");
    }

    KinematicSensorPtr kinSensor = std::dynamic_pointer_cast<KinematicSensor>(motion->getSensorByType("Kinematic"));

    std::vector<std::string> availableJoints = kinSensor->getJointNames();

    Eigen::MatrixXf dimSelectionMatrix(0,0);

    if (wantedDimensions.size() != 0)
    {
    for (const auto & dimension : wantedDimensions)
    {
        if (std::find(availableJoints.begin(), availableJoints.end(), dimension) == availableJoints.end())
        {
            std::stringstream errorMessage;
            errorMessage << dimension
                         << " is in the wanted dimension set but not in the MMM trajectory!";
            throw std::ios_base::failure(errorMessage.str().c_str());

        }
    }
    //construct a matrix that selects the wanted joint readings by right-hand multiplication of the measurement vector
     dimSelectionMatrix.resize(wantedDimensions.size(), availableJoints.size());
    for(std::size_t r=0; r<wantedDimensions.size(); r++)
        for(std::size_t c=0; c<availableJoints.size(); c++)
        {
            if (wantedDimensions[r] == availableJoints[c])
                dimSelectionMatrix(r,c) = 1;
            else
                dimSelectionMatrix(r,c) = 0;

        }
    }
    else
    {
        dimSelectionMatrix.resize(availableJoints.size(), availableJoints.size());
        //select all sensors
        dimSelectionMatrix = Eigen::MatrixXf::Identity(availableJoints.size(), availableJoints.size());
    }


    auto measurements = kinSensor->getMeasurements();


    std::vector<DVec> jointsValues(dimSelectionMatrix.rows());
    DVec timestamps;
    SampledTrajectoryV2 result;

    for (const auto & measurement : measurements)
    {
        //skip measurements that are not in the timeframe of interest:
        if (measurement.first < t_start || measurement.first > t_stop)
        {
            continue;
        }

        KinematicSensorMeasurementPtr kinMeasurement = std::dynamic_pointer_cast<KinematicSensorMeasurement>(measurement.second);
        if (!kinMeasurement)
        {
            throw std::ios_base::failure("kinematic sensor has a measurement that is not of the type KinematicSensorMeasurement");
        }
        timestamps.push_back(kinMeasurement->getTimestep());
        Eigen::VectorXf jointAngles = kinMeasurement->getJointAngles();
        if (jointAngles.rows() >= 0 && static_cast<std::size_t>(jointAngles.rows()) != availableJoints.size())
        {
            std::stringstream errorMessage;
            errorMessage << "number of joints mismatch!"
                    << " number of available joints:"
                    << jointsValues.size()
                    << " measurement vector rows:"
                    << jointAngles.rows();
            throw std::ios_base::failure(errorMessage.str().c_str());
        }
        else
        {
            if (dimSelectionMatrix.cols() != jointAngles.rows())
            {
                std::stringstream errorMessage;
                errorMessage << "dimensions of selection matrix and joint angle vector are not compatible. "
                        << "dimSelectionMatrix.cols():"
                        << dimSelectionMatrix.cols()
                        << " jointAngles.rows():"
                        << jointAngles.rows();
                throw std::ios_base::failure(errorMessage.str().c_str());

            }

            Eigen::VectorXf selectedReadings = dimSelectionMatrix * jointAngles;

            for (int i=0; i<selectedReadings.rows(); i++)
            {
                jointsValues[i].push_back(selectedReadings[i]);
            }
        }

    }

    for (const auto & jointsValue : jointsValues)
    {
        result.addDimension(timestamps, jointsValue);
    }

    return result;

}

}
