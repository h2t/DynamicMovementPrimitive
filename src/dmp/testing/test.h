/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Nils Adermann <naderman at naderman dot de>
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef DMP_TEST_H
#define DMP_TEST_H

#ifdef DMP_BOOST_TEST
#ifdef WIN32
#include <boost/test/included/unit_test.hpp>
#else
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#endif
#endif

#include <string>
#include <fstream>
#include <iostream>

#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

namespace testout
{
    typedef boost::iostreams::tee_device<std::ostream, std::ostream> ostream_tee_device;
    typedef boost::iostreams::stream<ostream_tee_device> tee_ostream;
}

struct OutputConfiguration
{
    OutputConfiguration()
    {
        // The path where the tests are put into is taken from CMake
        std::string logFileName("");
        logFileName.append(boost::unit_test::framework::master_test_suite().p_name);
        logFileName.append(".xml");
        logFile.open(logFileName.c_str());

        tee = new testout::ostream_tee_device(std::cout, logFile);
        teeStream = new testout::tee_ostream(*tee);

        boost::unit_test::unit_test_log.set_stream(*teeStream);
    }

    ~OutputConfiguration()
    {
        boost::unit_test::unit_test_log.set_stream(std::cout);

        teeStream->flush();

        delete tee;
        delete teeStream;

        logFile << "</TestLog>" << std::flush;
        logFile.close();
    }
    testout::ostream_tee_device* tee;
    testout::tee_ostream* teeStream;
    std::ofstream logFile;
};

#if  BOOST_VERSION  < 106200
    BOOST_GLOBAL_FIXTURE(OutputConfiguration)
#else
    BOOST_GLOBAL_FIXTURE(OutputConfiguration);
#endif

#endif
