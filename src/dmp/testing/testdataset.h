#ifndef TESTDATASET_H
#define TESTDATASET_H


#include "dmp/general/helpers.h"
#include "dmp/representation/trajectory.h"

using namespace DMP;

class TestDataSet
{
public:
    unsigned int m_nPoints, m_periodStartIndex;
    double m_period_length;
    double m_delta_t;
    DVec m_anchorPoint, m_periodStartPoint;

    Vec< DVec > m_positionData, m_orientationData;

    TestDataSet()
    {
        m_delta_t = 1.0 / 100; // TODO: should not be hard coded
    }

    void readFromFile(string filename);

    SampledComposedPeriodicTrajectory trajectoryForPositionDimension(unsigned int d) const;
    SampledComposedPeriodicTrajectory trajectoryForPositions() const;
    SampledComposedPeriodicTrajectory trajectoryForPositions(const Vec <bool> &used_dimensions) const;
    void trajectoryForPositionsV2(SampledTrajectoryV2& traj, std::vector<int> usedDims);
    void cutConstantBeginning(double rel_threshold = 0.006);

    void flip_x();
};

ostream& operator << (ostream& out, const TestDataSet& set);



class MartinTestCase
{
public:
    DVec m_start, m_target;
    unsigned int m_type, m_trial;
    double m_speed;

    MartinTestCase(double sx, double sy,
                   double tx, double ty,
                   unsigned int type, unsigned int trial,
                   double speed)
    {
        m_start.resize(2);
        m_start[0] = sx;
        m_start[1] = sy;
        m_target.resize(2);
        m_target[0] = tx;
        m_target[1] = ty;

        m_speed = speed;
        m_type = type;
        m_trial = trial;
    }
};




#endif // TESTDATASET_H
