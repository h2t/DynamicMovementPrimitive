/**
* This file is part of DMP Lib.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#define BOOST_TEST_MODULE DMP::TrajectoryTest
#define DMP_BOOST_TEST
#include <dmp/testing/test.h>


#include <dmp/functionapproximation/functionapproximationregistration.h>
#include <dmp/functionapproximation/functionapproximation.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/functionapproximation/radialbasisfunctioninterpolator.h>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/tmpdir.hpp>
#include <sys/time.h>
using namespace DMP;


//BOOST_CLASS_EXPORT_GUID(ExactReproduction, "ExactReproduction")
//BOOST_CLASS_EXPORT_GUID(LWR<GaussKernel>, "LWR<GaussKernel>")

BOOST_AUTO_TEST_CASE(testSerializationExactReproduction)
{
    // Load the trajectory
    DVec timestamps;
    timestamps << 0, 1, 2;
    DVec positions;
    positions << 1, 2, 5;

    ExactReproduction func;
    func.learn(timestamps, positions);
    std::stringstream str;
    {
        boost::archive::xml_oarchive ar(str);

        ar << BOOST_SERIALIZATION_NVP(func);
    }
    boost::archive::xml_iarchive ar(str);
    ExactReproduction func2;
    ar >> BOOST_SERIALIZATION_NVP(func2);
    //    std::cout << str.str() << std::endl;
    BOOST_CHECK_EQUAL(func(1), func2(1));
}

BOOST_AUTO_TEST_CASE(testSerializationPointers)
{
    // Load the trajectory
    DVec timestamps;
    timestamps << 0, 1, 2;
    DVec positions;
    positions << 1, 2, 5;

    ExactReproduction func;
    func.learn(timestamps, positions);
    FunctionApproximationInterface* baseFunc = &func;
    std::stringstream str;
    {
        boost::archive::xml_oarchive ar(str);

        ar << BOOST_SERIALIZATION_NVP(baseFunc);
    }
    boost::archive::xml_iarchive ar(str);
    ExactReproduction func2;
    FunctionApproximationInterface* baseFunc2;
    ar >> BOOST_SERIALIZATION_NVP(baseFunc2);
    //    std::cout << str.str() << std::endl;
    BOOST_CHECK_EQUAL((*baseFunc)(1), (*baseFunc2)(1));
}

BOOST_AUTO_TEST_CASE(testSerializationLWR)
{
    // Load the trajectory
    DVec timestamps;
    timestamps << 0, 1, 2;
    DVec positions;
    positions << 1, 2, 5;

    LWR<GaussKernel> func;
    func.learn(timestamps, positions);
    FunctionApproximationInterface* baseFunc = &func;

    std::stringstream str;
    {
        boost::archive::xml_oarchive ar(str);

        ar << BOOST_SERIALIZATION_NVP(baseFunc);
    }
    boost::archive::xml_iarchive ar(str);
    FunctionApproximationInterface* baseFunc2;
    ar >> BOOST_SERIALIZATION_NVP(baseFunc2);
    std::cout << str.str() << std::endl;
    BOOST_CHECK_EQUAL((*baseFunc)(1), (*baseFunc2)(1));
}

BOOST_AUTO_TEST_CASE(testRBFInterpolator)
{
    Vec<DVec > phases;
    DVec2d positions;

    DVec p1;
    p1.resize(2);

    p1[0] = 1;
    p1[1] = 0;
    phases.push_back(p1);

    p1[0] = 0;
    p1[1] = 1;
    phases.push_back(p1);

    p1[0] = 1;
    p1[1] = 1;
    phases.push_back(p1);

    p1[0] = 0;
    p1[1] = 0;
    phases.push_back(p1);

    DVec position;
    position << 1,1,0,0;
    positions.push_back(position);

    std::cout << "finished setup ... " << std::endl;
    RBFInterpolator<GaussRadialBasisFunction> approximator(2);

    struct timeval start, end;
    gettimeofday(&start, nullptr);

    approximator.learn(phases,positions);

    gettimeofday(&end, nullptr);
    double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
                    end.tv_usec - start.tv_usec) / 1.e6;

    std::cout << "time: " << delta << std::endl;


    std::cout << "start testing ... " << std::endl;
    p1[0] = 1;
    p1[1] = 1;
    std::cout << "res: " << approximator(p1) << std::endl;

    FunctionApproximationInterface* baseFunc = &approximator;

    std::stringstream str;
    {
        boost::archive::xml_oarchive ar(str);

        ar << BOOST_SERIALIZATION_NVP(baseFunc);
    }
    boost::archive::xml_iarchive ar(str);
    FunctionApproximationInterface* baseFunc2;
    ar >> BOOST_SERIALIZATION_NVP(baseFunc2);
    std::cout << str.str() << std::endl;
    BOOST_CHECK_EQUAL((*baseFunc)(p1), (*baseFunc2)(p1));

}

//BOOST_AUTO_TEST_CASE(testSerializationLWRToFile)
//{
//    // Load the trajectory
//    DVec timestamps;
//    timestamps << 0,1,2;
//    DVec positions;
//    positions << 1, 2,5;

//    LWR<GaussKernel> func;
//    func.learn(timestamps, positions);
//    FunctionApproximationInterface* baseFunc = &func;

//    std::string file = std::string(boost::archive::tmpdir()) + "/lwr.xml";
//    std::cout << "file: " << file << std::endl;
////    std::stringstream str;
//    std::ofstream ofs(file.c_str());
//    {
//        boost::archive::xml_oarchive ar(ofs);

//        ar << BOOST_SERIALIZATION_NVP(baseFunc);
//    }
////    boost::archive::xml_iarchive ar(str);
////    FunctionApproximationInterface* baseFunc2;
////    ar >> BOOST_SERIALIZATION_NVP(baseFunc2);
////    std::cout << str.str() << std::endl;
////    BOOST_CHECK_EQUAL((*baseFunc)(1), (*baseFunc2)(1));
//}


//BOOST_AUTO_TEST_CASE(testSerializationLWRFromFile)
//{
//    // Load the trajectory
//    DVec timestamps;
//    timestamps << 0,1,2;
//    DVec positions;
//    positions << 1, 2,5;

//    LWR<GaussKernel> func;
//    func.learn(timestamps, positions);
//    FunctionApproximationInterface* baseFunc = &func;

//    std::string file = std::string(boost::archive::tmpdir()) + "/lwr.xml";
//    std::cout << "file: " << file << std::endl;
////    std::stringstream str;
//    std::ifstream ofs(file.c_str());
//    {
//        boost::archive::xml_iarchive ar(ofs);

//        ar >> BOOST_SERIALIZATION_NVP(baseFunc);
//    }
////    FunctionApproximationInterface* baseFunc2;
//    std::stringstream str;
//    {
//        boost::archive::xml_oarchive ar(str);

//        ar << BOOST_SERIALIZATION_NVP(baseFunc);
//    }

////    boost::archive::xml_iarchive ar(str);
////    FunctionApproximationInterface* baseFunc2;
////    ar >> BOOST_SERIALIZATION_NVP(baseFunc2);
//    std::cout << str.str() << std::endl;
////    BOOST_CHECK_EQUAL((*baseFunc)(1), (*baseFunc2)(1));
//}

#define SIGN_FACTOR(x) ((x>=0)?1:-1)

