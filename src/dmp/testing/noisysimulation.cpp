#include "noisysimulation.h"

#include <dmp/general/helpers.h>
#include <cstdlib>
using namespace DMP;

NoisySimulation::NoisySimulation(double minCycleTime, double cycleVariance, double velocityVariance, DVec initialData, DVec initialDataDeriv) :
    ODE(initialData.size() * 2, 1e-2),
    minCycleTime(minCycleTime),
    cycleVariance(cycleVariance),
    velocityVariance(velocityVariance),
    m_fixStepSize(0.001),
    currentData(initialData),
    currentDataDeriv1(initialDataDeriv),
    currentTargetTimestamp(0),
    initialData(initialData),
    initialDataDeriv(initialDataDeriv)
{
}

void NoisySimulation::reset()
{
    currentTargetTimestamp = 0;
    currentData = initialData;
    currentDataDeriv1 = initialDataDeriv;
}

void NoisySimulation::addDataDeriv(double timestamp, const DVec& dataDeriv1)
{
    currentTargetDataDeriv1 = dataDeriv1;
    double derivNoiseFactor = 1 - velocityVariance + (double)(rand()) / RAND_MAX * 5 * velocityVariance;
    currentTargetDataDeriv1 =  derivNoiseFactor * currentTargetDataDeriv1;
    currentTimestamp = timestamp;
    currentTargetTimestamp = timestamp;
    currentTargetTimestamp += minCycleTime + (double)(rand()) / RAND_MAX * cycleVariance;

}

DVec NoisySimulation::getNextData(double& nextTimestamp, DVec& deriv)
{
    //    nextTimestamp = currentTimestamp + minCycleTime + exp((double)(rand()) / RAND_MAX*-60.90776) * cycleVariance;
    nextTimestamp = currentTargetTimestamp;// + minCycleTime + (double)(rand()) / RAND_MAX * cycleVariance;

    //    double derivNoiseFactor = 1 - velocityVariance +   (double)(rand()) / RAND_MAX * 5 * velocityVariance;
    //    deriv =  derivNoiseFactor * currentTargetDataDeriv1;
    deriv = currentTargetDataDeriv1;
    currentData += (currentTargetTimestamp - currentTimestamp) * currentTargetDataDeriv1 ;


    return currentData;
}

bool NoisySimulation::getNextDataStepByStep(double& curTimestamp, DVec& data, DVec& deriv)
{
    if (curTimestamp < currentTargetTimestamp)
    {
        DVec odeInput;

        for (unsigned int i = 0; i < currentDataDeriv1.size(); ++i)
        {
            odeInput.push_back(currentDataDeriv1.at(i));
            odeInput.push_back(0);
        }

        DVec odeOutput;
        integrate(m_fixStepSize, odeInput, odeOutput);
        curTimestamp += m_fixStepSize;
        deriv.clear();

        for (unsigned int i = 0; i < odeOutput.size(); i += 2)
        {
            deriv.push_back(odeOutput.at(i));
        }

        currentDataDeriv1 = deriv;
        currentData += m_fixStepSize * deriv;
        data = currentData;

        return true;
    }
    else
    {
        data = currentData;
        deriv = currentDataDeriv1;
        return false;
    }
}

DVec NoisySimulation::getCurrentData()
{
    return currentData;
}

void NoisySimulation::flow(double t, const DVec& x, DVec& out)
{
    int i = 0;
    double D = 2000;
    double K = pow(D * 0.5, 2.0);

    unsigned int curDim = 0;

    while (curDim < currentTargetDataDeriv1.size())
    {
        double vel = x.at(curDim * 2);
        double acc = x.at(curDim * 2 + 1);

        i = curDim * 2;

        out.at(i++) = x.at(1);
        out.at(i) = K * (currentTargetDataDeriv1[curDim] - vel) - (D * acc);

        curDim++;
    }
}
