#ifndef RADIALBASISFUNCTIONINTERPOLATOR_H
#define RADIALBASISFUNCTIONINTERPOLATOR_H

#include "kernel.h"


#include <dmp/general/helpers.h>
#include <dmp/io/rapidxml/rapidxml.hpp>
#include <dmp/io/rapidxml/rapidxml_print.hpp>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>


#include <map>
#include <armadillo>
using namespace arma;

namespace DMP
{
#define LEARN_MAX_EPOCH 1000
template <typename Kernel = GaussianKernel>
class GPR
{

public:
    GPR()
    {
        kernel = Kernel();
        learnHyperParas = false;
    }

    GPR(double l, double sigma_f, double sigma_n, bool learnHyperParas = false)
    {
        kernel = Kernel(l,sigma_f, sigma_n);
        this->learnHyperParas = learnHyperParas;
        learn_rate = 0.1;
    }

    void learn(const DVec2d &x, const DVec2d &y);
    double evaluate(const DVec2d& x, const DVec2d& y);

    // create kernels

    const DVec operator()(const DVec& x) const;

    unsigned int dim()
    {
        return indim;
    }

    void setLearnRate(double lrate)
    {
        learn_rate = lrate;
    }

protected:

private:
    unsigned int outdim;
    unsigned int indim;

    Kernel kernel;
    mat K_XX_inv;
    mat Z;
    DVec2d X;

    bool learnHyperParas;
    double learn_rate;


public:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int file_version)
    {
        ar& boost::serialization::make_nvp("indim", indim);
        ar& boost::serialization::make_nvp("outdim", outdim);
    }
    template <typename ValueType>
    char* valueToAllocatedCharString(rapidxml::xml_document<> &doc, ValueType value) const
    {
        std::stringstream valueStream;
        valueStream << value;
        return doc.allocate_string(valueStream.str().c_str());
    }
};


template <typename Kernel>
void GPR<Kernel>::learn(const DVec2d &x, const DVec2d &y)
{
    if (x.size() == 0 || y.size() == 0)
    {
        return;
    }

    indim = x[0].size();
    outdim = y[0].size();
    X = x;

    double data_size = x.size();

    if(!learnHyperParas || kernel.toString() != "GaussianKernel")
    {
        mat K_XX = zeros<mat>(x.size(), x.size());
        mat Y = zeros<mat>(y.size(), outdim);
        for(size_t i = 0; i < x.size(); ++i)
        {
            for(size_t j = 0; j <= i; ++j)
            {
                K_XX(i,j) = kernel.calc(x.at(i), x.at(j));

                if(i != j)
                {
                    K_XX(j,i) = K_XX(i,j);
                }
            }

            for(size_t j = 0; j < outdim; ++j)
            {
                Y(i,j) = y.at(i).at(j);
            }
        }

        K_XX_inv = inv(K_XX);
        Z = K_XX_inv * Y;

    }
    else
    {
        mat K_XX = zeros<mat>(x.size(), x.size());
        mat Y = zeros<mat>(y.size(), outdim);
        mat K_DerivL = zeros<mat>(x.size(), x.size());
        mat K_DerivSigmaF = zeros<mat>(x.size(), x.size());
        mat K_DerivSigmaN = zeros<mat>(x.size(), x.size());

        double likelihood;
        double old_likelihood = 0;
        double epoch = 0;
        while(1)
        {
            for(size_t i = 0; i < x.size(); ++i)
            {
                for(size_t j = 0; j <= i; ++j)
                {
                    K_XX(i,j) = kernel.calc(x.at(i), x.at(j));
                    DVec deri_res = kernel.dericalc(x.at(i), x.at(j));
                    K_DerivL(i,j) = deri_res.at(0);
                    K_DerivSigmaF(i,j) = deri_res.at(1);
                    K_DerivSigmaN(i,j) = deri_res.at(2);

                    if(i != j)
                    {
                        K_XX(j,i) = K_XX(i,j);
                        K_DerivL(j,i) = K_DerivL(i,j);
                        K_DerivSigmaF(j,i) = K_DerivSigmaF(i,j);
                        K_DerivSigmaN(j,i) = K_DerivSigmaN(i,j);
                    }
                }

                for(size_t j = 0; j < outdim; ++j)
                {
                    Y(i,j) = y.at(i).at(j);
                }
            }

            K_XX_inv = inv(K_XX);
            Z = K_XX_inv * Y;

            likelihood = 0;
            for(size_t i = 0; i < Y.n_cols; ++i)
            {
                vec yvec = Y.col(i);
                double normK = norm(K_XX);
                mat quadterm = yvec.t() * K_XX_inv * yvec;
                double val = - 0.5 * quadterm(0,0) - 0.5 * log(normK) - data_size * log(2 * M_PI) / 2;
                likelihood += val;
            }

            std::cout << "epoch: " << epoch << " likelihood: " << likelihood << " l: " << kernel.l << " sigma_f: " << kernel.sigma_f << " sigma_n: " << kernel.sigma_n << std::endl;
            if(fabs(likelihood - old_likelihood) < 1e-5 || epoch > LEARN_MAX_EPOCH)
                break;


            double deriL = 0;
            double deriSf = 0;
            double deriSn = 0;

            for(size_t i = 0; i < Z.n_cols; ++i)
            {
                mat upMat = Z.col(i) * Z.col(i).t() - K_XX_inv;

                deriL += 0.5 * trace(upMat * K_DerivL);
                deriSf += 0.5 * trace(upMat * K_DerivSigmaF);
                deriSn += 0.5 * trace(upMat * K_DerivSigmaN);

            }


            // gradient descent to determine learn_rate
            double alpha_l = learn_rate;
            double alpha_sf = learn_rate * 0.01;
            double alpha_sn = learn_rate * 0.01;

            kernel.l += alpha_l * deriL;
            kernel.sigma_f += alpha_sf * deriSf;
            kernel.sigma_n += alpha_sn * deriSn;

            old_likelihood = likelihood;
            epoch++;
        }
    }


}

template <typename Kernel>
const DVec GPR<Kernel>::operator()(const DVec& x) const
{
    checkValues(x);

    mat res = zeros<mat>(1, outdim);
    for(size_t i = 0; i < X.size(); ++i)
    {
        double weight = kernel.calc(x, X.at(i));
        res = res + weight * Z.row(i);
    }

    DVec result;
    for(size_t i = 0; i < outdim; ++i)
    {
        result.push_back(res(0,i));
    }


    return result;
}



template <typename Kernel>
double GPR<Kernel>::evaluate(const DVec2d& x, const DVec2d& y)
{
    if (x.size() == 0 || y.size() == 0)
    {
        return 0;
    }

    double toterr = 0;
    for(size_t i = 0; i < x.size(); ++i)
    {
        DVec cx = x[i];
        DVec cy = operator ()(cx);
        DVec ty = y[i];

        double sqerr = 0;
        for (size_t j = 0; j < outdim; ++j)
        {
            sqerr += pow(ty[j] - cy[j],2);
        }
        sqerr = sqrt(sqerr);
        toterr += sqerr;
    }

    double meanError = toterr / x.size();
    return meanError;
}


//template <typename Kernel>
//void RBFInterpolator<Kernel>::createKernels(const DVec &xmin, const DVec &xmax, int kernelNum)
//{
//    if(xmin.size() != 2 || xmax.size() != 2)
//    {
//        DMP::Exception("createKernels: not implemented for dimension different from 2");
//    }

//    if(xmin.size() != xmax.size())
//    {
//        DMP::Exception("createKernels: cannot create kernels");
//    }

//    if(kernelNum == 0)
//    {
//        DMP::Exception("createKernels: kernel number cannot be zero.");
//    }

//    double xdist = (xmax[0] - xmin[0]) / (double)kernelNum;
//    double ydist = (xmax[1] - xmin[1]) / (double)kernelNum;

//    for(double x = xmin[0]; x <= xmax[0]; x = x + xdist)
//    {
//        for(double y = xmin[1]; y <= xmax[1]; y = y + ydist)
//        {
//            DVec center;
//            DVec width;

//            center.push_back(x);
//            center.push_back(y);

//            width.push_back(xdist * __widthFactor[0] * 2.5);
//            width.push_back(ydist * __widthFactor[1] * 2.5);

//            KernelData kernel;
//            kernel.kernel = Kernel(center, width);
//            kernel.width = width;
//            kernel.center = center;
//            kernel.weight = 0;
//            kernel.P = 1;

//            __kernels.push_back(kernel);
//        }
//    }
//}



}


#endif
