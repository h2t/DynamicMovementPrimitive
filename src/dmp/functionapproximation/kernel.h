#ifndef KERNEL_H
#define KERNEL_H


#include <dmp/general/helpers.h>
#include <dmp/representation/trajectory.h>
#include <string>
#include <sstream>

namespace DMP
{

    class KernelBasisFunction
    {
    public:
        virtual ~KernelBasisFunction() {}

        virtual double calc(const DVec& q1, const DVec& q2) const = 0;

        virtual string toString() const
        {
            return "NOT_IMPLEMENTED";
        }
    };


    class GaussianKernel : public KernelBasisFunction
    {
    public:
        double l;
        double sigma_f;
        double sigma_n;

        GaussianKernel()
        {
            l = 1;
            sigma_f = 1;
            sigma_n = 0.1;
        }

        GaussianKernel(double l, double sigma_f, double sigma_n)
        {
            this->l = l;
            this->sigma_f = sigma_f;
            this->sigma_n = sigma_n;
        }

        string toString() const
        {
            return "GaussianKernel";
        }

        double calc(const DVec &q1, const DVec &q2) const
        {
            if(q1.size() != q2.size())
            {
                std::cerr << "Warning: cannot calculate kernel value of two vectors of different sizes " << std::endl;
                return 0;
            }

            double norm2 = 0;
            for(size_t i = 0; i < q1.size(); i++)
            {
                norm2 += pow(q1.at(i) - q2.at(i), 2);
            }

            double res = sqrt(M_PI) * l * pow(sigma_f,2) * exp(- norm2 / (4 * pow(l,2)));

            if(norm2 == 0)
            {
                res += sigma_n;
            }

            return res;
        }

        DVec dericalc(const DVec& q1, const DVec &q2) const
        {
            if(q1.size() != q2.size())
            {
                std::cerr << "Warning: cannot calculate kernel value of two vectors of different sizes " << std::endl;
                return 0;
            }

            DVec res;
            double norm2 = 0;
            for(size_t i = 0; i < q1.size(); i++)
            {
                norm2 += pow(q1.at(i) - q2.at(i), 2);
            }

            double multfactor = sqrt(M_PI) * pow(sigma_f,2);
            double exp_part = exp(- norm2 / (4 * pow(l,2)));
            double deriv_l_1 = multfactor * exp_part;
            double deriv_l_2 = multfactor * l * exp_part * (norm2 / 2) * pow(l,-3);
            double deriv_l = deriv_l_1 + deriv_l_2;

            res.push_back(deriv_l);

            double deriv_sigma_f = 2 * exp_part * sqrt(M_PI) * sigma_f * l;

            res.push_back(deriv_sigma_f);

            double deriv_sigma_n = 0;
            if(norm2 == 0)
            {
                deriv_sigma_n = 1;
            }

            res.push_back(deriv_sigma_n);

            return res;
        }


    };



} // DMP

#endif // KERNEL_H
