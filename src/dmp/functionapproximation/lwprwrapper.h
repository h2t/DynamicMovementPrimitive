/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef LWPRWRAPPER_H
#define LWPRWRAPPER_H

#ifdef USE_LWPR
#include <lwpr.hh>
#endif
#include "functionapproximation.h"

#include <boost/shared_ptr.hpp>

namespace DMP
{
#ifdef USE_LWPR
    typedef boost::shared_ptr<LWPR_Object> LWPRPtr;
    class LWPRWrapper : public FunctionApproximationInterface
    {
    public:
        LWPRWrapper(int dimIn = 2, int dimOut = 1, double scalingFactor = 0.05, double forget=0.99);

        // inherited from FunctionApproximationInterface
        void learn(const Vec<DVec> &x, const Vec<DVec> &y) override;

        void ilearn(const DVec &x, double y) override
        {
            doubleVec factors;
            for(size_t i = 0; i < x.size(); i++)
            {
                factors.push_back(x.at(i));
            }

            doubleVec responds;
            responds.push_back(y);

            lwpr->update(factors, responds);
        }


        const DVec operator()(const DVec& x) const override;
        bool supportsIncrementalLearning() const
        {
            return true;
        }
        void reset() override;

        FunctionApproximationInterfacePtr clone() override
        {
            return FunctionApproximationInterfacePtr(new LWPRWrapper(*this));
        }
        LWPRPtr getLWPR() const
        {
            return lwpr;
        }
    private:
        LWPRPtr lwpr;
        double scalingFactor;

        int dimIn;
        int dimOut;
        // FunctionApproximationInterface interface
    public:
        string toString() const;
        bool fromString(const string& content);
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            FunctionApproximationInterface& base = boost::serialization::base_object<FunctionApproximationInterface>(*this);
            ar& BOOST_SERIALIZATION_NVP(base);
            throw Exception("NYI");
        }
    };
#else
    class LWPRWrapper : public FunctionApproximationInterface
    {
    public:
        LWPRWrapper();

        // inherited from FunctionApproximationInterface
        void learn(const Vec<DVec> &x, const Vec<DVec> &y) override;
        const DVec operator()(const DVec& x) const override;
        bool supportsIncrementalLearning() const
        {
            return true;
        }
        void reset() override;


        void* getLWPR() const
        {
            throw Exception("LWPR not found, so not wrapper compiled");
        }
    private:
        double scalingFactor;


        // FunctionApproximationInterface interface
    public:
        string toString() const;
        bool fromString(const string& content);
    };
    typedef boost::shared_ptr<LWPRWrapper> LWPRWrapperPtr;
#endif
}

#endif // LWPRWRAPPER_H
