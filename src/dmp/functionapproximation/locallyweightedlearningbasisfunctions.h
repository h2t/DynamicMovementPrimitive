#ifndef LOCALLYWEIGHTEDLEARNINGBASISFUNCTIONS_H
#define LOCALLYWEIGHTEDLEARNINGBASISFUNCTIONS_H


#include <dmp/general/helpers.h>
#include <dmp/representation/trajectory.h>
#include <string>
#include <sstream>

namespace DMP
{

    class LWLAbstractBasisFunction
    {
    public:
        virtual ~LWLAbstractBasisFunction() {}

        virtual double operator()(double t) const = 0;

        void plot(double from, double to, unsigned int count, const char* tmp_filename = "plots/basis_function.gp") const
        {
            double delta_t = (to - from) / (count - 1);
            DVec times(count),
                 values(count);

            for (unsigned int i = 0; i < count; ++i)
            {
                times[i] = from + i * delta_t;
                values[i] = (*this)(times[i]);
            }

            SampledTrajectory traj(times, values);
            traj.plot(tmp_filename);
        }

        virtual string toString() const
        {
            return "NOT_IMPLEMENTED";
        }
    };


    class LWLAbstractSymmetricBasisFunction  : public LWLAbstractBasisFunction
    {
    public:
        double m_center, m_width;
        LWLAbstractSymmetricBasisFunction(){}
        LWLAbstractSymmetricBasisFunction(double center, double width)
        {
            m_center = center;
            m_width = width;
        }
        ~LWLAbstractSymmetricBasisFunction() override {}

        double center() const
        {
            return m_center;
        }
        double width() const
        {
            return m_width;
        }

        virtual double evaluate(double t) const = 0;
        double operator()(double t) const override
        {
            return evaluate((t - m_center) / m_width);
        }
    };


    class LWLAbstractBasisFunctionWithCompactSupport : public LWLAbstractSymmetricBasisFunction
    {
    public:
        LWLAbstractBasisFunctionWithCompactSupport(double center, double width)
            : LWLAbstractSymmetricBasisFunction(center, width) {}

        bool isInSupp(double t) const
        {
            return (fabs(t - m_center) < m_width / 2);
        }

        double operator()(double t) const override
        {
            if (isInSupp(t))
            {
                return LWLAbstractSymmetricBasisFunction::operator()(t);
            }

            return 0.0;
        }
    };

    class RadialBasisFunction
    {
    public:
        DVec m_center;
        RadialBasisFunction(){}
        RadialBasisFunction(DVec center): m_center(center) {}
        RadialBasisFunction(const RadialBasisFunction& source): m_center(source.m_center) {}


        virtual double operator()(DVec x) const
        {
            return evaluate(distance(m_center, x));
        }

        virtual double evaluate(double t) const = 0;

        virtual double distance(DVec a, DVec b) const = 0;

        DVec center() const
        {
            return m_center;
        }



    };

    class GaussRadialBasisFunction : public RadialBasisFunction
    {
    public:
        DVec m_width;
        unsigned int dimension;
        GaussRadialBasisFunction(){}
        GaussRadialBasisFunction(const DVec& center, const DVec& width):
            RadialBasisFunction(center),
            m_width(width)
        {
            dimension = center.size();
        }

        GaussRadialBasisFunction(const GaussRadialBasisFunction& source):
            RadialBasisFunction(source)
        {
            m_width = source.m_width;
            m_center = source.m_center;
            dimension = source.dimension;
        }

        DVec width() const
        {
            return m_width;
        }

        int dim() {return dimension;}

        bool isInSupp(DVec x) const
        {
            bool res = true;
            for(size_t i = 0; i < x.size(); ++i)
            {
                res = res && fabs(x[i] - m_center[i]) <= m_width[i];
            }

            return res;
        }

        double distance(DVec a, DVec b) const override
        {
            if(a.size() != b.size() || a.size() != m_width.size())
            {
                throw DMP::Exception("The dimensions of both vectors are not the same.");
            }

            double res = 0;
            for(size_t i = 0; i < a.size(); ++i)
            {
                res += pow(a[i] - b[i], 2) / pow(2.0 * (m_width[i] / 2.5 / 2.5), 2);
            }

            return res;
        }

        double evaluate(double t) const override
        {
            return exp(-0.5 * t);
        }

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& boost::serialization::make_nvp("m_center", m_center);
            ar& boost::serialization::make_nvp("m_width", m_width);
            ar& boost::serialization::make_nvp("dimension", dimension);
        }

    };

    class RFWRKernel
    {
    public:
        int dim;
        Eigen::VectorXf c;
        Eigen::MatrixXf M;

        RFWRKernel()
        {

        }

        RFWRKernel(const Eigen::VectorXf& center, const Eigen::MatrixXf& Mc)
        {
            if(center.rows() != Mc.rows())
            {
                throw Exception("RFWR Kernel cannot be initialized: the dimension of center does not corresponds to the dimension of covariance matrix");
            }
            c = center;
            M = Mc;
            dim = center.rows();
        }

        double evaluate(const Eigen::VectorXf& x) const
        {
            Eigen::MatrixXf D = M.transpose() * M;
            return exp(-0.5 * (x - c).transpose() * D * (x - c));
        }

        double operator()(const DVec& x) const
        {
            Eigen::VectorXf xv = vec2eigen(x);
            return evaluate(xv);
        }

        double detD()
        {
            Eigen::MatrixXf D = M.transpose() * M;
            return D.determinant();
        }

        bool isInSupp(const DVec& x) const
        {

            return true;
        }

    };

    class GaussKernel : public LWLAbstractBasisFunction
    {
    public:
        double m_center{0}, m_width{0};
        GaussKernel() {}
        GaussKernel(const GaussKernel& source): m_center(source.m_center), m_width(source.m_width) {}
        GaussKernel& operator=(const GaussKernel& source)
        {
            this->m_center = source.m_center;
            this->m_width = source.m_width;
            return *this;
        }
        GaussKernel(double center, double width) :
            m_center(center), m_width(width)
        {
            if (m_width == 0)
            {
                throw Exception("Kernel width must not be zero: ");
            }
        }

        bool isInSupp(double t) const
        {
            return fabs(t - m_center) <= m_width;
        }
        double operator()(double t) const override
        {
            if (!isInSupp(t))
            {
                return 1e-07;
            }

            return evaluate(t - m_center);
        }
        virtual double evaluate(double t) const
        {
            double result = exp(-1.0 / pow(2.0 * (m_width / 2.5 / 2.5), 2) * pow(t, 2.0));
            return result;
        }

        virtual double getDeriv(double t) const
        {
            if (!isInSupp(t))
            {
                return 0;
            }

            return deri_evaluate(t - m_center);
        }

        virtual double deri_evaluate(double t) const
        {
            double a = pow(2.0 * (m_width / 2.5 / 2.5), 2);
            double result = -(2.0 * t / a) * exp(- pow(t,2.0) / a);
            return result;
        }

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& BOOST_SERIALIZATION_NVP(m_center);
            ar& BOOST_SERIALIZATION_NVP(m_width);
        }
    };

    class LWLGaussTestFunction : public LWLAbstractBasisFunctionWithCompactSupport
    {
    public:
        LWLGaussTestFunction(double center, double width)
            : LWLAbstractBasisFunctionWithCompactSupport(center, width) {}

        double evaluate(double t) const override
        {
            return M_E * exp(-1.0 / (1.0 - (2 * t) * (2 * t)));
        }

        virtual double getDeriv(double t) const
        {
            return 0;
        }
    };


    class LWLGaussStandardFunction : public LWLAbstractBasisFunctionWithCompactSupport
    {
    public:
        double m_w0;

        LWLGaussStandardFunction(double center, double width)
            : LWLAbstractBasisFunctionWithCompactSupport(center, width)
        {
            m_w0 = -4 * log(0.003);
        }

        double evaluate(double t) const override
        {
            return exp(-m_w0 * t * t);
        }

        virtual double getDeriv(double t) const
        {
            return 0;
        }

        //    string toString() const {
        //        stringstream out;
        //        out << "LWLGaussStandardFunction (" << m_center << ", " << m_width << ", " << m_w0 << ")";
        //        return out.str();
        //    }
    };


    class LWLPhaseSpaceBasisFunction
    {
    public:
        virtual double operator()(double phi, double r) const = 0;
        virtual ~LWLPhaseSpaceBasisFunction() {}

        friend ostream& operator << (ostream& out, const LWLPhaseSpaceBasisFunction& fct);

        virtual string toString() const = 0;
        static LWLPhaseSpaceBasisFunction* newFromString(string str);
    };


    class LWLDistanceBasedPhaseSpaceBasisFunction : public LWLPhaseSpaceBasisFunction
    {
    public:
        DVec m_center;
        double m_delta;
        LWLAbstractBasisFunction* m_function;

        LWLDistanceBasedPhaseSpaceBasisFunction(const LWLDistanceBasedPhaseSpaceBasisFunction& src)
        {
            m_center = src.m_center;
            m_delta = src.m_delta;
            //m_function = new LWLGaussTestFunction(0.0, m_delta);
            m_function = new LWLGaussStandardFunction(0.0, m_delta);
        }

        LWLDistanceBasedPhaseSpaceBasisFunction(const DVec& center, double delta)
        {
            m_center = center;
            m_delta = delta;
            //      m_function = new LWLGaussTestFunction(0.0, delta);
            m_function = new LWLGaussStandardFunction(0.0, m_delta);
        }
        ~LWLDistanceBasedPhaseSpaceBasisFunction() override
        {
            delete m_function;
        }

        double operator()(double phi, double r) const override
        {
            double x, y;
            pol2cart(phi, r, x, y);
            return callWithCartesian(x, y);
        }

        double callWithCartesian(double x, double y) const
        {
            double dx = x - m_center[0],
                   dy = y - m_center[1];
            double dist = sqrt(dx * dx + dy * dy);
            return (*m_function)(dist);
        }

        string toString() const override
        {
            return "NOT_IMPLEMENTED!";
        }
    };


    class LWLGaussZeroToOne : public LWLAbstractBasisFunction
    {
    public:
        double m_mu1, m_mu2;
        unsigned int m_k;
        double m_w0;

        LWLGaussZeroToOne(double mu1, double mu2, unsigned int k = 4)
        {
            m_mu1 = mu1;
            m_mu2 = mu2;
            m_k = k;

            double delta = m_mu2 - m_mu1;
            m_w0 = -log(0.003) / pow(delta, int(m_k));
        }

        double operator()(double t) const override
        {
            if (t <= m_mu1)
            {
                return 0.0;
            }

            if (t >= m_mu2)
            {
                return 1.0;
            }

            return exp(-m_w0 * pow(t - m_mu2, int(m_k)));
        }

        virtual double getDeriv(double t) const
        {
            return 0;
        }

        //    string toString() const {
        //        stringstream out;
        //        out << "LWLGaussZeroToOne (" << m_mu1 << ", " << m_mu2<< ", " << m_k << ", " << m_w0 << ")";
        //        return out.str();
        //    }
    };


    class JDistanceAndSeparationBasedPhaseSpaceBasisFunction : public LWLPhaseSpaceBasisFunction
    {
    public:
        DVec m_center;
        double m_delta, m_mu1, m_mu2;
        LWLAbstractBasisFunction* m_distancefunction;
        LWLAbstractBasisFunction* m_radiusfunction;

        JDistanceAndSeparationBasedPhaseSpaceBasisFunction(const JDistanceAndSeparationBasedPhaseSpaceBasisFunction& src)
        {
            m_center = src.m_center;
            m_delta = src.m_delta;
            m_mu1 = src.m_mu1;
            m_mu2 = src.m_mu2;
            m_distancefunction = new LWLGaussTestFunction(0.0, m_delta);
            //      m_distancefunction = new LWLGaussStandardFunction(0.0, m_delta);
            m_radiusfunction = new LWLGaussZeroToOne(m_mu1, m_mu2);
        }

        JDistanceAndSeparationBasedPhaseSpaceBasisFunction(const DVec& center, double delta, double mu1, double mu2)
        {
            m_center = center;
            m_delta = delta;
            m_mu1 = mu1;
            m_mu2 = mu2;
            //      m_function = new LWLGaussTestFunction(0.0, delta);
            m_distancefunction = new LWLGaussStandardFunction(0.0, m_delta);
            m_radiusfunction = new LWLGaussZeroToOne(m_mu1, m_mu2);
        }
        ~JDistanceAndSeparationBasedPhaseSpaceBasisFunction() override
        {
            delete m_distancefunction;
            delete m_radiusfunction;
        }

        double operator()(double phi, double r) const override
        {
            double x, y;
            pol2cart(phi, r, x, y);
            return callWithCartesian(x, y) * (*m_radiusfunction)(r);
        }

        double callWithCartesian(double x, double y) const
        {
            double dx = x - m_center[0],
                   dy = y - m_center[1];
            double dist = sqrt(dx * dx + dy * dy);
            return (*m_distancefunction)(dist);
        }

        string toString() const override;

        static LWLPhaseSpaceBasisFunction* newFromString(const string& str);
    };


    class LWLPeriodicGaussGAMS : public LWLAbstractSymmetricBasisFunction
    {
    public:
        double m_w0;
        LWLPeriodicGaussGAMS(){}
        LWLPeriodicGaussGAMS(double center, double width)
            : LWLAbstractSymmetricBasisFunction(center, width)
        {
            if (width > 2 * M_PI)
                cout << "WARNING: Invalid support width = " << width
                     << " for LWLPeriodicGaussGAMS" << endl;

            m_w0 = log(0.003) / (cos(width / 2) - 1.0);
        }

        bool isInSupp(double phi) const
        {
            return true;
        }

        double operator()(double phi) const override
        {
            phi -= floor(phi / (2*M_PI)) * 2 * M_PI;
            return evaluate(phi - m_center);
        }

        double evaluate(double phi) const override
        {
            return exp(m_w0 * (cos(phi) - 1));
        }


        virtual double getDeriv(double t) const
        {
            return 0;
        }
        //    string toString() const {
        //        stringstream out;
        //        out << "LWLPeriodicGaussGAMS (" << m_center << ", " << m_width << ")";
        //        return out.str();
        //    }

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& BOOST_SERIALIZATION_NVP(m_center);
            ar& BOOST_SERIALIZATION_NVP(m_width);
            ar& BOOST_SERIALIZATION_NVP(m_w0);

        }
    };


    class LWLGaussAndIndicator : public LWLAbstractBasisFunction
    {
    public:
        double m_mu, m_mu1, m_mu2;
        unsigned int m_k;
        double m_w0;

        LWLGaussAndIndicator(double mu, double mu1, double mu2, unsigned int k = 4)
        {
            m_mu = mu;
            m_mu1 = mu1;
            m_mu2 = mu2;
            m_k = k;

            double delta = m_mu2 - m_mu1;
            m_w0 = -log(0.003) / pow(delta, int(m_k));
        }

        double operator()(double t) const override
        {
            double delta = m_mu2 - m_mu1;

            if (t <= m_mu - delta || t >= m_mu2)
            {
                return 0.0;
            }

            if (m_mu - delta < t && t < m_mu)
            {
                return exp(-m_w0 * pow(t - m_mu, int(m_k)));
            }

            if (m_mu <= t && t <= m_mu1)
            {
                return 1.0;
            }

            //      if (m_mu1 < t && t < m_mu2)
            return exp(-m_w0 * pow(t - m_mu1, int(m_k)));
        }
        virtual double getDeriv(double t) const
        {
            return 0;
        }
        //    string toString() const {
        //        stringstream out;
        //        out << "LWLGaussAndIndicator (" << m_mu << ", " << m_mu1 << ", " << m_mu2 << ", "
        //                                        << m_k << ", " << m_w0 << ")";
        //        return out.str();
        //    }
    };

    class JGAMSCombinedWithGaussTest : public LWLPhaseSpaceBasisFunction
    {
    public:
        double m_center, m_width, m_mu, m_eps, m_mu2, m_height;

        LWLGaussTestFunction* m_functionForR;
        LWLPeriodicGaussGAMS* m_functionForPhi;

        JGAMSCombinedWithGaussTest(const JGAMSCombinedWithGaussTest& src)
        {
            m_center = src.m_center;
            m_width = src.m_width;
            m_mu = src.m_mu;
            m_eps = src.m_eps;
            m_mu2 = src.m_mu2;
            m_height = src.m_height;

            m_functionForR = new LWLGaussTestFunction(m_mu, m_height);
            m_functionForPhi = new LWLPeriodicGaussGAMS(m_center, m_width);
        }

        JGAMSCombinedWithGaussTest(double center, double width, double mu, double eps)
        {
            m_center = center;
            m_width = width;
            m_mu = mu;
            m_eps = eps;
            m_mu2 = (1 + eps) * mu; // TODO
            m_height = 2 * (m_mu2 - mu);

            m_functionForR = new LWLGaussTestFunction(m_mu, m_height);
            m_functionForPhi = new LWLPeriodicGaussGAMS(m_center, m_width);
        }

        ~JGAMSCombinedWithGaussTest() override
        {
            delete m_functionForR;
            delete m_functionForPhi;
        }

        double operator()(double phi, double r) const override
        {
            return (*m_functionForR)(r) * (*m_functionForPhi)(phi);
        }

        virtual double getDeriv(double t) const
        {
            return 0;
        }
    };


    class JGAMSCombinedWithGaussAndIndicator : public LWLPhaseSpaceBasisFunction
    {
    public:
        double m_center, m_width, m_mu, m_mu1, m_mu2;
        unsigned int m_k;

        LWLGaussAndIndicator* m_functionForR;
        LWLPeriodicGaussGAMS* m_functionForPhi;

        JGAMSCombinedWithGaussAndIndicator(const JGAMSCombinedWithGaussAndIndicator& src)
        {
            m_center = src.m_center;
            m_width = src.m_width;
            m_mu = src.m_mu;
            m_mu1 = src.m_mu1;
            m_mu2 = src.m_mu2;
            m_k = src.m_k;

            m_functionForR = new LWLGaussAndIndicator(m_mu, m_mu1, m_mu2, m_k);
            m_functionForPhi = new LWLPeriodicGaussGAMS(m_center, m_width);
        }

        JGAMSCombinedWithGaussAndIndicator(double center, double width, double mu, double mu1,
                                           double mu2, unsigned int k = 4)
        {
            m_center = center;
            m_width = width;
            m_mu = mu;
            m_mu1 = mu1;
            m_mu2 = mu2;
            m_k = k;

            m_functionForR = new LWLGaussAndIndicator(m_mu, m_mu1, m_mu2, m_k);
            m_functionForPhi = new LWLPeriodicGaussGAMS(m_center, m_width);
        }

        ~JGAMSCombinedWithGaussAndIndicator() override
        {
            delete m_functionForR;
            delete m_functionForPhi;
        }

        double operator()(double phi, double r) const override
        {
            //      cout << "(*m_functionForR)(" << r << ") = " << (*m_functionForR)(r) << endl;
            return (*m_functionForR)(r) * (*m_functionForPhi)(phi);
        }

        virtual double getDeriv(double t) const
        {
            return 0;
        }

        string toString() const override
        {
            stringstream out;
            out << "JGAMSCombinedWithGaussAndIndicator (" << m_center << ", " << m_width << ", " << m_mu << ", " << m_mu1 << ", " << m_mu2 << ", "
                << m_k << ")";
            return out.str();
        }

        static JGAMSCombinedWithGaussAndIndicator* newFromString(const string& str);
    };
} // DMP

#endif // LOCALLYWEIGHTEDLEARNINGBASISFUNCTIONS_H
