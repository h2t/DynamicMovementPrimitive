/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "functionapproximation.h"

#include <dmp/general/exception.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
//#include <omp.h>

using namespace DMP;




//FunctionApproximation::FunctionApproximation(int basisFunctionCount,
//                                             double basisFunctionCenter,
//                                             double basisFunctionWidth) :
//    basisFunctionCount(basisFunctionCount),

//    basisFunctionWidth(basisFunctionWidth)
//{
//    basisFunctions.resize(basisFunctionCount, NULL);
//}

//FunctionApproximation::~FunctionApproximation()
//{
//    deleteBasisFunctions();
//}

//void FunctionApproximation::calc(const DoubleMap &dataPoints)
//{
//    if(dataPoints.size() == 0)
//        throw Exception("datapoints is empty");

//    createBasisFunctions(dataPoints);

//    Vec< LWLAbstractSymmetricBasisFunction* >::iterator it = basisFunctions.begin();
//    for ( ; it != basisFunctions.end(); ++it)
//    {
//        LWLAbstractSymmetricBasisFunction* func = *it;
//        //weights = calcWeight(func, dataPoints, dataPoints);
//    }


//}


//class calcWeight_target_params {
//public:
//    const LWLAbstractSymmetricBasisFunction *basis_function;
//    const DoubleMap dataPoints;
//    const DVec uValues;

//    calcWeight_target_params(const LWLAbstractSymmetricBasisFunction *f, const DVec &uValues,
//                            const DoubleMap &dataPoints)
//        : basis_function(f), dataPoints(dataPoints), uValues(uValues){
//    }
//};

//double calcWeight_target (double w, void *params) {
//    calcWeight_target_params *p = (calcWeight_target_params*)(params);

//    if(!p)
//        throw Exception("Could not cast into calcWeight_target_params");

//    double error = 0.0;

//    DoubleMap::const_iterator it = p->dataPoints.begin();

//    for(unsigned int i=0; i<p->uValues.size() && it != p->dataPoints.end(); ++i, ++it) {
//        // value of canonical system
//        double u = p->uValues.at(i);
//        // example traj value
//        double y = it->second;

//        double diff = (y - w);
//        error += (*p->basis_function)(u)*diff*diff;
//    }
//    return error;
//}


//double FunctionApproximation::calcWeight(const LWLAbstractSymmetricBasisFunction *basis_function,
//                                         const DVec & uValues,
//                                         const DoubleMap &dataPoints) {
//    double res = 1337.0;
//    int status;
//    int iter = 0, max_iter = 200;
//    const gsl_min_fminimizer_type *T;
//    gsl_min_fminimizer *s;
//    double lowerBoundary = -500000.0, upperBoundary = 500000.0;
//    gsl_function F;

//    F.function = &calcWeight_target;
//    F.params = new calcWeight_target_params(basis_function, uValues, dataPoints);

//    T = gsl_min_fminimizer_brent;
//    s = gsl_min_fminimizer_alloc (T);
//    gsl_min_fminimizer_set (s, &F, 0, lowerBoundary, upperBoundary);

//    do
//    {
//        iter++;
//        status = gsl_min_fminimizer_iterate (s);
//        lowerBoundary = gsl_min_fminimizer_x_lower (s);
//        upperBoundary = gsl_min_fminimizer_x_upper (s);

//        status = gsl_min_test_interval (lowerBoundary, upperBoundary, 0.001, 0.0);

//        if (status == GSL_SUCCESS) {
//            res = gsl_min_fminimizer_x_minimum (s);
//        }
//    }
//    while (status == GSL_CONTINUE && iter < max_iter);

//    gsl_min_fminimizer_free (s);
//    delete (calcWeight_target_params*)F.params;

////    double best_result = gsl_min_fminimizer_x_minimum (s);
//    return res;
//}

//void FunctionApproximation::deleteBasisFunctions()
//{
//    Vec< LWLAbstractSymmetricBasisFunction* >::iterator it = basisFunctions.begin();
//    for ( ; it != basisFunctions.end(); ++it)
//    {
//        delete *it;
//        *it = NULL;
//    }
//}

//void FunctionApproximation::createBasisFunctions(const DoubleMap &dataPoints)
//{

//    double leftBoundary = dataPoints.begin()->first;
//    double rightBoundary = dataPoints.rbegin()->first;

//    double centerStepSize = (rightBoundary - leftBoundary ) / dataPoints.size();

//    double currentCenter = leftBoundary;

//    deleteBasisFunctions();
//    for (Vec<LWLAbstractSymmetricBasisFunction>::size_type i = 0; i < basisFunctions.size(); ++i)
//    {
//        basisFunctions.at(i) = new LWLGaussStandardFunction(currentCenter, basisFunctionWidth);

//        currentCenter += centerStepSize;
//    }
//}


string DMP::ExactReproduction::toString() const
{
    return "NYI";
}

bool DMP::ExactReproduction::fromString(const string& /*content*/)
{
    throw std::logic_error{"Not yet implemented!"};
}
