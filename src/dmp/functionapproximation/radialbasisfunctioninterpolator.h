#ifndef RADIALBASISFUNCTIONINTERPOLATOR_H
#define RADIALBASISFUNCTIONINTERPOLATOR_H

#include "functionapproximation.h"
#include "locallyweightedlearningbasisfunctions.h"


#include <dmp/general/helpers.h>
#include <dmp/io/rapidxml/rapidxml.hpp>
#include <dmp/io/rapidxml/rapidxml_print.hpp>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>


#include <map>
namespace DMP
{
template <typename Kernel = GaussRadialBasisFunction>
class RBFInterpolator
        :  public DMP::FunctionApproximationInterface
{

#define LOW_WIDTH_LIMIT 0.01
public:
    struct KernelData
    {
        Kernel kernel;
        DVec center;
        DVec width;
        double weight;
        double P;

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& boost::serialization::make_nvp("center", center);
            ar& boost::serialization::make_nvp("width", width);
            ar& boost::serialization::make_nvp("weight", weight);
            ar& boost::serialization::make_nvp("kernel", kernel);
        }
    };

    RBFInterpolator(){}

    RBFInterpolator(unsigned int dim, DVec widthFactor = DVec())
    {
        dimension = dim;
        __widthFactor.resize(dimension);

        for(size_t i = 0; i < dim; ++i)
        {
            if(i >= widthFactor.size())
                __widthFactor[i] = 1;
            else
                __widthFactor[i] = widthFactor[i];
        }
        lambda = 0.99;
    }

    // inherited from FunctionApproximationInterface
    void learn(const Vec<DVec>&x, const Vec<DVec> &y) override;

    // incremental learning
    void ilearn(const DVec &x, double y) override;

    // create kernels
//    void createKernels(const DVec &xmin, const DVec &xmax, int kernelNum);

    const DVec operator()(const DVec& x) const override;
    void reset() override;
    FunctionApproximationInterfacePtr clone() override
    {
        return FunctionApproximationInterfacePtr(new RBFInterpolator<Kernel>(*this));
    }

    // evaluation
    double evaluate(const Vec<DVec> &x, const DVec& y, double* maxError = NULL, DVec* predictedValues = NULL);

    const Vec<KernelData>& getKernels()const
    {
        return __kernels;
    }

    void setKernels(const Vec<KernelData>& kernels) {
        if(kernels.size() != getKernelSize())
        {
            throw DMP::Exception("The given kernels' size is different from the required kernel size.");
        }
        __kernels = kernels;
    }

    vector<double> getWeights() override;

    size_t getKernelSize() override{
        return __kernels.size();
    }

    unsigned int dim()
    {
        return dimension;
    }
    bool supportsIncrementalLearning() override
    {
        return true;
    }
protected:
    double calcWeight(const Kernel& kernel, const Vec<DVec>& x, const DVec& y, double guessedMinimum = 0);
    double getWeightValue(unsigned int kernelIndex) const;
private:
    DVec __widthFactor;

    Vec<KernelData> __kernels;

    unsigned int dimension;

public:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int file_version)
    {
        FunctionApproximationInterface& base = boost::serialization::base_object<FunctionApproximationInterface>(*this);
        ar& boost::serialization::make_nvp("base", base);
        ar& boost::serialization::make_nvp("__widthFactor", __widthFactor);
        ar& boost::serialization::make_nvp("__kernels", __kernels);
    }
    template <typename ValueType>
    char* valueToAllocatedCharString(rapidxml::xml_document<> &doc, ValueType value) const
    {
        std::stringstream valueStream;
        valueStream << value;
        return doc.allocate_string(valueStream.str().c_str());
    }
};

template <typename Kernel>
void RBFInterpolator<Kernel>::reset()
{
    __kernels.clear();
}

void getXTream(Vec<DVec> x, DVec& x0, DVec& xT);


template <typename Kernel>
void RBFInterpolator<Kernel>::learn(const Vec<DVec> &x, const Vec<DVec> &y)
{
    if (x.size() == 0 || y.size() == 0)
    {
        return;
    }

    reset();

    const DVec& y1 = y.at(0);

    __kernels.resize(x.size());
    #pragma omp parallel for schedule(dynamic,1)
    for(size_t i = 0; i < x.size(); ++i)
    {
        DVec center;
        center.resize(dimension);

        DVec cx = x[i];

        for(unsigned int j = 0; j < cx.size(); ++j)
        {
            center[j] = cx[j];
        }

        KernelData kernel;

        DVec width;

        #pragma omp parallel
        for(size_t j = 0; j < center.size(); ++j)
        {
            double closestDistance = std::numeric_limits<double>::max();

            for (size_t k = 0; k < x.size(); ++k)
            {
                if(k == i)
                    continue;

                if(fabs(x.at(k).at(j) - cx[j]) == 0)
                    continue;

                if (fabs(x.at(k).at(j) - cx[j]) < closestDistance)
                {
                    closestDistance = fabs(x.at(k).at(j) - cx[j]);
                }
            }

            double cwidth = closestDistance * __widthFactor[j] * 2.5;
            width.push_back(cwidth);
        }


        kernel.kernel = Kernel(center, width);
        kernel.width = width;
        kernel.center = center;
        kernel.weight = calcWeight(kernel.kernel, x, y1);
        kernel.P = 1;

        __kernels[i] = kernel;

    }

    typename Vec<KernelData>::iterator it = __kernels.begin();
    for(; it != __kernels.end(); ++it)
    {
        if(it->weight == 0)
        {
            __kernels.erase(it);
        }
    }
    std::cout << __kernels.size() << std::endl;

}

template <typename Kernel>
void RBFInterpolator<Kernel>::ilearn(const DVec &x, double y)
{

    bool createNew = true;
    for (size_t i = 0; i < __kernels.size(); i++)
    {
        if (__kernels.at(i).kernel.isInSupp(x))
        {
            KernelData kernel = __kernels.at(i);

            // update width
            DVec center = kernel.center;
            double val = kernel.kernel(x);

            if(val > 0.9)
            {
                createNew = false;
            }

            for(size_t j = 0; j < dimension; ++j)
            {
                double dist = fabs(x.at(j) - center.at(j));

                double closestDistance = kernel.width[j] / (2.5 * __widthFactor[j]);
                if(dist > LOW_WIDTH_LIMIT && dist < closestDistance)
                {
                    closestDistance = dist;
                }

                kernel.width[j] = closestDistance * __widthFactor[j] * 2.5;
            }

            kernel.kernel.m_width = kernel.width;


            double err = y - kernel.weight;
            kernel.weight += val * kernel.P * err;
            kernel.P = 1/lambda * (kernel.P - (kernel.P * kernel.P) / (lambda/val + kernel.P));

            __kernels.at(i) = kernel;
        }
    }

    if(createNew)
    {
        KernelData kernel;

        DVec width;
        DVec center;
        center.resize(x.size());
        width.resize(x.size());

        for(unsigned int j = 0; j < x.size(); ++j)
        {
            center[j] = x[j];
            width[j] = 100;

        }

        kernel.kernel = Kernel(center, width);
        kernel.width = width;
        kernel.center = center;
        kernel.weight = 0;
        kernel.P = 1;

        __kernels.push_back(kernel);

        ilearn(x,y);
    }

}


template <typename Kernel>
double RBFInterpolator<Kernel>::calcWeight(const Kernel& kernel, const Vec<DVec>& x, const DVec& y, double guessedMinimum)
{

    double numerator = 0;
    double denominator = 0;
    for(size_t i = 0; i < x.size(); ++i)
    {
        DVec u = x.at(i);
        numerator += kernel(u) * y[i];
        denominator += kernel(u);
    }

    double res;

    if(denominator != 0)
        res = numerator / denominator;
    else
        res = 0;

    return res;
}




template <typename Kernel>
const DVec RBFInterpolator<Kernel>::operator()(const DVec& x) const
{
    DVec result(1, 0.0);
    double weightedSum = 0;
    double sum = 0;

    checkValues(x);

    for (size_t i = 0; i < __kernels.size(); ++i)
    {

        if (__kernels.at(i).kernel.isInSupp(x)) // compact description
        {
            double value = (__kernels.at(i).kernel)(x);
            double weight;
            weight = getWeightValue(i);

            checkValue(value);
            checkValue(weight);

            weightedSum += value * weight;
            sum += value;
        }

    }

    if(sum == 0)
    {
        result.at(0) = 0;
    }
    else
    {
        result.at(0) = weightedSum / sum;
    }
    //checkValues(result);
    return result;
}

template <typename Kernel>
double RBFInterpolator<Kernel>::getWeightValue(unsigned int kernelIndex) const
{

    return __kernels.at(kernelIndex).weight;
}


template <typename Kernel>
double RBFInterpolator<Kernel>::evaluate(const Vec<DVec>& x, const DVec& y, double* maxError, DVec* predictedValues)
{
    if (predictedValues)
    {
        predictedValues->clear();
    }

    if (maxError)
    {
        *maxError = 0;
    }

    double errorSum = 0.0;
    unsigned int size = std::min(x.size(), y.size());
    double minValue = std::numeric_limits<double>::max();
    double maxValue = std::numeric_limits<double>::min();

    for (unsigned int i = 0; i < size; i++)
    {
        DVec cx = x[i];
        double prediction = operator()(cx).at(0);

        if (predictedValues)
        {
            predictedValues->push_back(prediction);
        }

        double error = fabs(y[i] - prediction);

        if (maxError)
        {
            *maxError = std::max(error, *maxError);
        }

        minValue = std::min(y[i], minValue);
        maxValue = std::max(y[i], maxValue);

        errorSum += error;
    }

    double meanError = errorSum / size;
    meanError /= maxValue - minValue;

    if (maxError)
    {
        *maxError /= maxValue - minValue;
    }

    return meanError;
}

template <typename Kernel>
vector<double> RBFInterpolator<Kernel>::getWeights()
{
    vector<double> res;

    for(size_t i = 0; i < __kernels.size(); i++){
        res.push_back(getWeightValue(i));
    }

    return res;
}

//template <typename Kernel>
//void RBFInterpolator<Kernel>::createKernels(const DVec &xmin, const DVec &xmax, int kernelNum)
//{
//    if(xmin.size() != 2 || xmax.size() != 2)
//    {
//        DMP::Exception("createKernels: not implemented for dimension different from 2");
//    }

//    if(xmin.size() != xmax.size())
//    {
//        DMP::Exception("createKernels: cannot create kernels");
//    }

//    if(kernelNum == 0)
//    {
//        DMP::Exception("createKernels: kernel number cannot be zero.");
//    }

//    double xdist = (xmax[0] - xmin[0]) / (double)kernelNum;
//    double ydist = (xmax[1] - xmin[1]) / (double)kernelNum;

//    for(double x = xmin[0]; x <= xmax[0]; x = x + xdist)
//    {
//        for(double y = xmin[1]; y <= xmax[1]; y = y + ydist)
//        {
//            DVec center;
//            DVec width;

//            center.push_back(x);
//            center.push_back(y);

//            width.push_back(xdist * __widthFactor[0] * 2.5);
//            width.push_back(ydist * __widthFactor[1] * 2.5);

//            KernelData kernel;
//            kernel.kernel = Kernel(center, width);
//            kernel.width = width;
//            kernel.center = center;
//            kernel.weight = 0;
//            kernel.P = 1;

//            __kernels.push_back(kernel);
//        }
//    }
//}



}


#endif
