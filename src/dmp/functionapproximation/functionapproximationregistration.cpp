#include "functionapproximationregistration.h"

BOOST_CLASS_EXPORT_IMPLEMENT(DMP::FunctionApproximationInterface)
BOOST_CLASS_EXPORT_IMPLEMENT(DMP::ExactReproduction)
BOOST_CLASS_EXPORT_IMPLEMENT(DMP::LWR<DMP::GaussKernel>)
BOOST_CLASS_EXPORT_IMPLEMENT(DMP::LWR<DMP::LWLPeriodicGaussGAMS>)
BOOST_CLASS_EXPORT_IMPLEMENT(DMP::RBFInterpolator<DMP::GaussRadialBasisFunction>)
