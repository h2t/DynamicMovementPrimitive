#ifndef LOCALLYWEIGHTEDLEARNING_H
#define LOCALLYWEIGHTEDLEARNING_H

#include <dmp/general/helpers.h>
#include "locallyweightedlearningbasisfunctions.h"

//#include <dmp/representation/newdmpformulation.h>
#include <dmp/io/rapidxml/rapidxml.hpp>
#include <dmp/io/rapidxml/rapidxml_print.hpp>
#include "functionapproximation.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>


#include <map>

namespace DMP
{

    template <typename Kernel = GaussKernel>
    class LWR
        : public FunctionApproximationInterface
    {
    public:
        struct KernelData
        {
            Kernel kernel;
            double center{0};
            double width{0};
            double weight{0};
            double weightGradient{0};
            double P{0};

            friend class boost::serialization::access;
            template<class Archive>
            void serialize(Archive& ar, const unsigned int file_version)
            {
                ar& BOOST_SERIALIZATION_NVP(center);
                ar& BOOST_SERIALIZATION_NVP(width);
                ar& BOOST_SERIALIZATION_NVP(weight);
                ar& BOOST_SERIALIZATION_NVP(weightGradient);
                ar& BOOST_SERIALIZATION_NVP(kernel);
                ar& BOOST_SERIALIZATION_NVP(P);
            }
        };


        LWR(int baseKernelCount = 5, double widthFactor = 1, bool useWeightGradients = false):
            __widthFactor(widthFactor),
            __useWeightGradients(useWeightGradients), __convertToZeroOutOfBoundaries(true),
            __baseKernelCount(baseKernelCount)
        {
            __kernels.resize(__baseKernelCount);
            lambda = 0.99;
        }

        // inherited from FunctionApproximationInterface
        void learn(const Vec<DVec>&x, const Vec<DVec> &y) override;
        void ilearn(const DVec &x, double y) override;

        bool supportsIncrementalLearning() override
        {
            return true;
        }

        const DVec operator()(const DVec& x) const override;
        const DVec getDeriv(const DVec &x) const override;
        void reset() override;
        FunctionApproximationInterfacePtr clone() override
        {
            return FunctionApproximationInterfacePtr(new LWR<Kernel>(*this));
        }

        // evaluation
        double evaluate(const DVec& x, const DVec& y, double* maxError = NULL, DVec* predictedValues = NULL);
        void plotKernels(bool weighted);

        // getters
        const Vec<KernelData>& getKernels()const
        {
            return __kernels;
        }

        void setKernels(const Vec<KernelData>& kernels) {
            __kernels = kernels;
            __baseKernelCount = kernels.size();
        }


        // setters
        void setWeightGradients(bool activated)
        {
            __useWeightGradients = activated;
        }
        void setConvertToZeroOutOfBoundaries(bool activated)
        {
            __convertToZeroOutOfBoundaries = activated;
        }

        vector<double> getWeights() override;

        void setWeights(const std::vector<double> &weights) override;
        void setupKernels(double xmin, double xmax) override;
        size_t getKernelSize() override{
            return __baseKernelCount;
        }

        void createKernels(const DVec &xminv, const DVec &xmaxv, int kernelNum = 0) override;

        vector<double> getBasisFunctionVal(const DVec &x) override;
        Eigen::VectorXf getBasisFunctionVec(const DVec& x) override;

    protected:
        double calcWeight(const Kernel& kernel, const DVec& x, const DVec& y);
        double calcWeightGradient(const Kernel& kernel, double center, double weight, const DVec& x, const DVec& y);
        double getWeightValue(unsigned int kernelIndex, double offset) const;
    private:
        double __widthFactor;

        bool __useWeightGradients;
        bool __convertToZeroOutOfBoundaries;
        int __baseKernelCount;
        Vec<KernelData> __kernels;

        // FunctionApproximationInterface interface
    public:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            FunctionApproximationInterface& base = boost::serialization::base_object<FunctionApproximationInterface>(*this);
            ar& BOOST_SERIALIZATION_NVP(base);

            ar& BOOST_SERIALIZATION_NVP(__widthFactor);
            ar& BOOST_SERIALIZATION_NVP(__useWeightGradients);
            ar& BOOST_SERIALIZATION_NVP(__convertToZeroOutOfBoundaries);
            ar& BOOST_SERIALIZATION_NVP(__baseKernelCount);
            std::vector<KernelData>& kernels = __kernels;
            ar& BOOST_SERIALIZATION_NVP(kernels);
        }
        //        char *intToAllocatedCharString(rapidxml::xml_document<> &doc, int value);
        void insertKernelInNode(rapidxml::xml_document<> &doc, rapidxml::xml_node<> *kernelNode, const KernelData& kernelData) const;
        template <typename ValueType>
        char* valueToAllocatedCharString(rapidxml::xml_document<> &doc, ValueType value) const
        {
            std::stringstream valueStream;
            valueStream << value;
            return doc.allocate_string(valueStream.str().c_str());
        }
    };
    typedef boost::shared_ptr<LWR<GaussKernel> > LWRPtr;


    class getLWRWeight_target_params
    {
    public:
        const LWLAbstractBasisFunction* basis_function;
        const DVec&  x;
        const DVec& y;

        getLWRWeight_target_params(const LWLAbstractBasisFunction* f, const DVec& x,
                                   const  DVec& y)
            : basis_function(f), x(x), y(y)
        {
        }
    };


    double getLWRWeight_target(double w, void* params);




    template <typename T> int sgn(T val)
    {
        return (T(0) < val) - (val < T(0));
    }

    template <typename Kernel>
    void LWR<Kernel>::learn(const Vec<DVec> &x, const Vec<DVec> &y)
    {
        if (x.size() == 0 || y.size() == 0)
        {
            return;
        }

        DVec x1;

        for(size_t i = 0; i < x.size(); ++i)
        {
            for(size_t j = 0; j < x[i].size(); ++j)
                x1.push_back(x[i].at(j));
        }

        const DVec& y1 = y.at(0);

        SampledTrajectoryV2 traj(x1, y1);
        traj.differentiateDiscretly(2);

        SampledTrajectoryV2 gaussFiltered(traj);
        gaussFiltered.gaussianFilter(0.02 * (*traj.getTimestamps().rbegin() - *traj.getTimestamps().begin()));

        DVec xminv;
        DVec xmaxv;
        xminv.push_back(*x1.begin());
        xmaxv.push_back(*x1.rbegin());
        createKernels(xminv, xmaxv, __baseKernelCount);

        for (int i = 0; i < __baseKernelCount; i++)
        {
            __kernels.at(i).weight = calcWeight(__kernels.at(i).kernel, x1, y1);
            if (__useWeightGradients)
            {
                __kernels.at(i).weightGradient = gaussFiltered.getState(__kernels.at(i).center, 0);
            }
            else
            {
                __kernels.at(i).weightGradient = 0;
            }

        }
        plotKernels(false);
    }

    template <typename Kernel>
    void LWR<Kernel>::createKernels(const DVec& xminv, const DVec& xmaxv, int kernelNum)
    {
        reset();

        double xmin = xminv.at(0);
        double xmax = xmaxv.at(0);
        double kernelDistance, width;

        double center;

        if(typeid(Kernel).name() == typeid(GaussKernel).name())
        {
            kernelDistance = (xmax - xmin) / (kernelNum - 1);
            //    double kernelDistance = trajLength/(__baseKernelCount-1);
            width = kernelDistance * __widthFactor * 2.5;
            center = xmin;
        }
        else if(typeid(Kernel).name() == typeid(LWLPeriodicGaussGAMS).name())
        {
            kernelDistance = 2 * M_PI / (kernelNum - 1);
            width = kernelDistance * __widthFactor * 2.5;

            center = 0;
        }

        __kernels.resize(kernelNum);

        for (int i = 0; i < kernelNum; i++)
        {
            KernelData kernel;
            kernel.kernel = Kernel(center, width);
            kernel.width = width;
            kernel.center = center;
            kernel.weight = 0;
            kernel.P = 1;


            __kernels.at(i) = (kernel);
            center += kernelDistance;
        }


    }

    template <typename Kernel>
    void LWR<Kernel>::ilearn(const DVec &x, double y)
    {
        double x1 = x.at(0);
        for (size_t i = 0; i < __kernels.size(); i++)
        {
            if (__kernels.at(i).kernel.isInSupp(x1))
            {
                KernelData kernel = __kernels.at(i);
                double val = kernel.kernel(x1);
                kernel.P = 1/lambda * (kernel.P - (kernel.P * kernel.P) / (lambda/val + kernel.P));
                kernel.weight += val * kernel.P * (y - kernel.weight);
                __kernels.at(i) = kernel;
            }
        }

    }

    template <typename Kernel>
    const DVec
    LWR<Kernel>::operator()(const DVec& x) const
    {
        DVec result(1, 0.0);
        double weightedSum = 0;
        double sum = 0;
        double x1 = x.at(0);

        if (std::isnan(x1))
        {
            throw Exception("Input is NaN");
        }

        for (int i = __kernels.size() - 1; i >= 0; i--)
        {
            double weight = getWeightValue(i,x1 - __kernels.at(i).center);

            if (__kernels.at(i).kernel.isInSupp(x1)) // compact description
            {
                double value = (__kernels.at(i).kernel)(x1);

                checkValue(value);
                checkValue(weight);

                weightedSum += value * weight;
                sum += value;
            }
            else
            {
                double value = 1e-7;
                weightedSum += value * weight;
                sum += value;
            }
        }

        if(sum == 0)
        {
            throw Exception("sum is zero");
        }

        double factor = 1;

        if (__convertToZeroOutOfBoundaries && __kernels.size() > 0 && x1 > __kernels.rbegin()->center)
        {
            factor = (__kernels.rbegin()->kernel)(x1);
        }
        else if (__convertToZeroOutOfBoundaries && __kernels.size() > 0 && x1 < __kernels.begin()->center)
        {
            factor = (__kernels.begin()->kernel)(x1);
        }

        result.at(0) = weightedSum / sum * factor;

        checkValue(result);
        return result;
    }

    template <typename Kernel>
    const DVec
    LWR<Kernel>::getDeriv(const DVec& x) const
    {
        DVec result(1, 0.0);
        double weightedSum = 0;
        double sum = 0;
        double weightedDeriv = 0;
        double deriv = 0;
        double x1 = x.at(0);

        if (std::isnan(x1))
        {
            throw Exception("Input is NaN");
        }


        for (int i = __kernels.size() - 1; i >= 0; i--)
        {
            double weight = getWeightValue(i,x1 - __kernels.at(i).center);
            if (__kernels.at(i).kernel.isInSupp(x1)) // compact description
            {
                double value = (__kernels.at(i).kernel)(x1);
                double derivValue = (__kernels.at(i).kernel).getDeriv(x1);

                checkValue(value);
                checkValue(derivValue);
                checkValue(weight);

                weightedSum += value * weight;
                sum += value;

                weightedDeriv += derivValue * weight;
                deriv += derivValue;
            }
            else
            {
                double value = 1e-7;
                weightedSum += value * weight;
                sum += value;

                double derivValue = 0;
                weightedDeriv += derivValue * weight;
                deriv += derivValue;
            }
        }

        if(sum == 0)
        {
            throw Exception("sum is zero");
        }


//        double factor = 1;

//        if (__convertToZeroOutOfBoundaries && __kernels.size() > 0 && x1 > __kernels.back().center)
//        {
//            factor = (__kernels.back().kernel)(x1);
//        }
//        else if (__convertToZeroOutOfBoundaries && __kernels.size() > 0 && x1 < __kernels.front().center)
//        {
//            factor = (__kernels.front().kernel)(x1);
//        }

        result.at(0) = ((weightedDeriv * sum) - (weightedSum * deriv)) / pow(sum,2);
        checkValue(result);

        return result;

    }

    template <typename Kernel>
    std::vector<double> LWR<Kernel>::getBasisFunctionVal(const DVec &x)
    {
        std::vector<double> res;
        for(size_t i = 0; i < __kernels.size(); ++i)
        {
            double val = __kernels.at(i).kernel(x[0]);
            res.push_back(val);
        }
        return res;
    }

    template <typename Kernel>
    Eigen::VectorXf LWR<Kernel>::getBasisFunctionVec(const DVec &x)
    {
        Eigen::VectorXf res;
        res.resize(__kernels.size());

        for(int i = 0; i < res.rows(); ++i)
        {
            res[i] = __kernels.at(i).kernel(x[0]);
        }

        return res;
    }

    template <typename Kernel>
    void LWR<Kernel>::reset()
    {
        __kernels.clear();
    }



    template <typename Kernel>
    double LWR<Kernel>::calcWeight(const Kernel& kernel, const DVec& x, const DVec& y)
    {
//        double res = 1337.0;
//        int status;
//        int iter = 0, max_iter = 200;
//        const gsl_min_fminimizer_type* T;
//        gsl_min_fminimizer* s;
//        double a = -50000000000.0, b = 5000000000.0;
//        gsl_function F;

//        F.function = &getLWRWeight_target;
//        F.params = new getLWRWeight_target_params(&kernel, x, y);

//        T = gsl_min_fminimizer_brent;
//        s = gsl_min_fminimizer_alloc(T);
//        gsl_min_fminimizer_set(s, &F, guessedMinimum, a, b);

//        do
//        {
//            iter++;
//            status = gsl_min_fminimizer_iterate(s);
//            a = gsl_min_fminimizer_x_lower(s);
//            b = gsl_min_fminimizer_x_upper(s);

//            status = gsl_min_test_interval(a, b, 0.001, 0.0001);

//            if (status == GSL_SUCCESS)
//            {
//                res = gsl_min_fminimizer_x_minimum(s);
//            }
//            else if (status == GSL_FAILURE)
//            {
//                throw Exception("calc weight failed!");
//            }

//        }
//        while (status == GSL_CONTINUE && iter < max_iter);

//        gsl_min_fminimizer_free(s);
//        delete(getLWRWeight_target_params*)F.params;

//        //  double best_result = gsl_min_fminimizer_x_minimum (s);
//        return res;

        double numerator = 0;
        double denominator = 0;
        for(size_t i = 0; i < x.size(); ++i)
        {
            double u = x.at(i);
            numerator += kernel(u) * y[i];
            denominator += kernel(u);
        }

        double res;

        if(denominator != 0)
            res = numerator / denominator;
        else
            res = 0;

        return res;
    }


    class getLWRWeightGradient_target_params
    {
    public:
        const LWLAbstractBasisFunction* basis_function;
        const double center;
        const double weight;
        const DVec&  x;
        const DVec& y;

        getLWRWeightGradient_target_params(const LWLAbstractBasisFunction* f, double center, double weight, const DVec& x,
                                           const  DVec& y)
            : basis_function(f), center(center), weight(weight), x(x), y(y)
        {
        }
    };

    double getLWRWeightGradient_target(double w, void* params);

    template <typename Kernel>
    double LWR<Kernel>::calcWeightGradient(const Kernel& kernel, double center, double weight, const DVec& x, const DVec& y)
    {
        double res = 1337.0;
        int status;
        int iter = 0, max_iter = 200;
        const gsl_min_fminimizer_type* T;
        gsl_min_fminimizer* s;
        double a = -500000.0, b = 500000.0;
        gsl_function F;
        F.function = &getLWRWeightGradient_target;
        F.params = new getLWRWeightGradient_target_params(&kernel, center, weight, x, y);

        T = gsl_min_fminimizer_brent;
        s = gsl_min_fminimizer_alloc(T);
        gsl_min_fminimizer_set(s, &F, 0, a, b);

        do
        {
            iter++;
            status = gsl_min_fminimizer_iterate(s);
            a = gsl_min_fminimizer_x_lower(s);
            b = gsl_min_fminimizer_x_upper(s);

            status = gsl_min_test_interval(a, b, 0.001, 0.00001);

            if (status == GSL_SUCCESS)
            {
                res = gsl_min_fminimizer_x_minimum(s);
            }
        }
        while (status == GSL_CONTINUE && iter < max_iter);

        gsl_min_fminimizer_free(s);
        delete(getLWRWeightGradient_target_params*)F.params;

        //  double best_result = gsl_min_fminimizer_x_minimum (s);
        return res;
    }

    template <typename Kernel>
    double LWR<Kernel>::getWeightValue(unsigned int kernelIndex, double offset) const
    {
        //    if(weightIndex == __weights.size()-1 && offset>0
        //            || weightIndex == 0 && offset<0)
        //        offset = 0;
        return __kernels.at(kernelIndex).weight + offset * __kernels.at(kernelIndex).weightGradient;
    }



    template <typename Kernel>
    double LWR<Kernel>::evaluate(const DVec& x, const DVec& y, double* maxError, DVec* predictedValues)
    {
        if (predictedValues)
        {
            predictedValues->clear();
        }

        if (maxError)
        {
            *maxError = 0;
        }

        double errorSum = 0.0;
        unsigned int size = std::min(x.size(), y.size());
        double minValue = std::numeric_limits<double>::max();
        double maxValue = std::numeric_limits<double>::min();

        for (unsigned int i = 0; i < size; i++)
        {
            double prediction = operator()(x[i]).at(0);

            if (predictedValues)
            {
                predictedValues->push_back(prediction);
            }

            double error = fabs(y[i] - prediction);

            if (maxError)
            {
                *maxError = std::max(error, *maxError);
            }

            minValue = std::min(y[i], minValue);
            maxValue = std::max(y[i], maxValue);

            errorSum += error;
            //        std::cout << "value at " << x[i] << ": " << y[i] << " prediction: " << prediction << " error: <<  " << error << std::endl;
        }

        double meanError = errorSum / size;
        meanError /= maxValue - minValue;

        if (maxError)
        {
            *maxError /= maxValue - minValue;
        }

        return meanError;
    }

    template <typename Kernel>
    void LWR<Kernel>::plotKernels(bool weighted)
    {
        Vec<DVec> plots;

        if (__kernels.size() == 0)
        {
            return;
        }

        double distance = __kernels.rbegin()->center - __kernels.begin()->center;
        double stepSize = distance / 500;
        DVec timestamps;

        for (double i = __kernels.begin()->center; i < __kernels.rbegin()->center; i += stepSize)
        {
            timestamps.push_back(i);
        }

        timestamps.push_back(__kernels.rbegin()->center);
        plots.push_back(timestamps);

        for (unsigned int i = 0; i < __kernels.size(); i++)
        {
            DVec values;

            for (DVec::iterator  t = timestamps.begin(); t < timestamps.end(); t++)
            {
                if (!weighted)
                {
                    values.push_back(__kernels[i].kernel(*t));
                }
                else
                {
                    values.push_back(getWeightValue(i, *t - __kernels[i].center) *__kernels[i].kernel(*t));
                }
            }

            plots.push_back(values);
        }

//        plotTogether(plots, "plots/kernels_%d.gp");
    }

    template <typename Kernel>
    vector<double> LWR<Kernel>::getWeights()
    {
        vector<double> res;

        for(size_t i = 0; i < __kernels.size(); i++){
            res.push_back(getWeightValue(i,0));
        }

        return res;
    }

    template <typename Kernel>
    void LWR<Kernel>::setWeights(const std::vector<double> &weights)
    {
        if(weights.size() != __kernels.size())
        {
            std::cout << "weights dim: " << weights.size() << " kernels size: " << __kernels.size() << std::endl;
           throw DMP::Exception("setWeights: the given weights vector has different length from the number of kernels");
        }

        for(size_t i = 0; i < __kernels.size(); ++i)
        {
            __kernels[i].weight = weights[i];
        }
    }

    template <typename Kernel>
    void LWR<Kernel>::setupKernels(double xmin, double xmax)
    {
        DVec xminv;
        DVec xmaxv;
        xminv.push_back(xmin);
        xmaxv.push_back(xmax);
        createKernels(xminv, xmaxv, __baseKernelCount);
    }


    //template <typename Kernel>
    //char* LWR<Kernel>::intToAllocatedCharString(rapidxml::xml_document<> &doc, int value)
    //{
    //    std::stringstream valueStream;
    //    valueStream << value;
    //    return doc.allocate_string(valueStream.str().c_str());
    //}



    //    template <typename Kernel>
    //    std::string LWR<Kernel>::toString() const
    //    {
    //        using namespace rapidxml;
    //        xml_document<> doc;
    //        //    doc.allocate_string("widthFactor");
    //        char *node_name = doc.allocate_string("DMP");
    //        xml_node<> *mainNode = doc.allocate_node(node_element, node_name);

    //        xml_attribute<> *attr = doc.allocate_attribute("widthFactor", valueToAllocatedCharString(doc, __widthFactor));
    //        mainNode->append_attribute(attr);

    //        attr = doc.allocate_attribute("useWeightGradients", valueToAllocatedCharString(doc, __useWeightGradients));
    //        mainNode->append_attribute(attr);

    //        attr = doc.allocate_attribute("baseKernelCount", valueToAllocatedCharString(doc, __baseKernelCount));
    //        mainNode->append_attribute(attr);

    //        //    char *node_name = doc.allocate_string("DMP");
    //        xml_node<> *KernelsNode = doc.allocate_node(node_element, "Kernels");

    //        typename Vec<KernelData>::const_iterator it = __kernels.begin();
    //        for(; it != __kernels.end(); it++)
    //            insertKernelInNode(doc, KernelsNode, *it);

    //        std::stringstream result;

    //        result << doc;
    //        return result.str();
    //    }

    template <typename Kernel>
    void LWR<Kernel>::insertKernelInNode(rapidxml::xml_document<> &doc,
                                         rapidxml::xml_node<> *kernelNode,
                                         const KernelData& kernelData) const
    {
        using namespace rapidxml;
        xml_node<> *node = doc.allocate_node(node_element, "Kernel");
        kernelNode->append_node(node);
        xml_attribute<> *attr = doc.allocate_attribute("center", valueToAllocatedCharString(doc, kernelData.center));
        node->append_attribute(attr);

        attr = doc.allocate_attribute("width", valueToAllocatedCharString(doc, kernelData.width));
        node->append_attribute(attr);

        attr = doc.allocate_attribute("weight", valueToAllocatedCharString(doc, kernelData.weight));
        node->append_attribute(attr);

        attr = doc.allocate_attribute("weightGradient", valueToAllocatedCharString(doc, kernelData.weightGradient));
        node->append_attribute(attr);

    }

    //    template <typename Kernel>
    //    bool LWR<Kernel>::fromString(const string &content)
    //    {

    //    }

    template <typename Kernel>
    ostream& operator << (ostream& out, const LWR<Kernel>& lwr)
    {
        out << "Kernel count: " << lwr.getKernels().size() << std::endl;
        out << "Kernel width: " << lwr.getKernelWidth() << std::endl;
        out << "Kernels:" << std::endl;

        for (unsigned int i = 0; i < lwr.getKernels().size(); i++)
        {
            out << "center: " << lwr.getKernels()[i].center << " width: " << lwr.getKernels()[i].kernel.m_width << " weight: " << lwr.getKernels().at(i).weight << " weight gradient: " << lwr.getKernels().at(i).weightGradient << std::endl;
        }

        return out;
    }

}


#endif
