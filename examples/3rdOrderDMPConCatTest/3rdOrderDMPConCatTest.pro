#-------------------------------------------------
#
# Project created by QtCreator 2012-12-19T15:25:44
#
#-------------------------------------------------

QT       += core

QT       -= gui

OBJECTS_DIR=build

TARGET = 3rdOrderDMPConCatTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../DMPLib/src ../lwpr/include

LIBS += -ldmp -llwpr -lgsl -lblas -lgslcblas -lgomp -L../lwpr/lib -L../DMPLib/build


QMAKE_CXXFLAGS +=

SOURCES += main.cpp
