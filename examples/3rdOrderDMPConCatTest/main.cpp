/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include <QCoreApplication>

#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/dmp3rdorderforcefield.h>

using namespace DMP;


int main(int argc, char *argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
    if(res)
    {
        throw std::logic_error{"Killing all plot windows returned " + std::to_string(res)};
    }
    Vec<DVec> plots;

    SampledTrajectoryV2<_3rdOrderDMP> traj, traj2;
//    traj.readFromCSVFile("../DMPConCatTest/pouring_x_short.csv");
    traj.readFromCSVFile("../3rdOrderDMPTest/concat_traj_1.csv");
//    traj.readFromCSVFile("line.csv");
//    traj.gaussianFilter(0.05* *traj.getTimestamps().rbegin());
    traj2.readFromCSVFile("../3rdOrderDMPTest/concat_traj_2.csv");
//    traj2.gaussianFilter(0.05* *traj.getTimestamps().rbegin());


    traj = SampledTrajectoryV2<_3rdOrderDMP>::normalizeTimestamps(traj,0,1);
    traj2 = SampledTrajectoryV2<_3rdOrderDMP>::normalizeTimestamps(traj2,1.00549,2);

    traj.differentiateDiscretely(0,1);
    traj2.differentiateDiscretely(0,1);
    plots.push_back(traj.getTimestamps());
    plots.push_back(traj.getDimensionData(0,0));
    plots.push_back(traj2.getDimensionData(0,0));
    plotTogether(plots,"plots/originalTraj%d.gp");

    DMP3rdOrderForceField dmp(4);
    DMP3rdOrderForceField dmp2(4);
//    BasicDMP dmp(1);
    dmp.learnFromTrajectories(traj);
    dmp2.learnFromTrajectories(traj2);


//    double maxError;
//    double error = dmp.evaluateReproduction(0,maxError);
//    std::cout << "Mean relative reproduction error: " << error << " max error: " << maxError << std::endl;

    // Configuration
    DVec timestamps = SampledTrajectoryV2<DMPState>::generateTimestamps(0,1, 1.0/(traj.getTimestamps().size()-1));
    DVec timestamps2 = SampledTrajectoryV2<DMPState>::generateTimestamps(1,2.0, 1.0/(traj2.getTimestamps().size()-1));
    DVec accs = SampledTrajectoryV2<_3rdOrderDMP>::differentiateDiscretely(traj.getTimestamps(), traj.getDimensionData(0,1));


    _3rdOrderDMP initialState;    
    initialState.pos = traj.begin()->data[0]->pos ;
    initialState.vel = traj.begin()->data[0]->vel;
    initialState.acc = *accs.begin();
    double goal = traj.rbegin()->data[0]->pos;
    SampledTrajectoryV2<_3rdOrderDMP> newTraj = dmp.calculateTrajectory(timestamps, DVec(1,goal), Vec<_3rdOrderDMP>(1,initialState), 0.0, 1);


    DVec newAccs = SampledTrajectoryV2<_3rdOrderDMP>::differentiateDiscretely(newTraj.getTimestamps(), newTraj.differentiateDiscretelyForDim(0, 0));

    DVec pos = newTraj.getDimensionData(0,0);
    DVec vel = newTraj.getDimensionData(0,1);
    DVec fPos = newTraj.getDimensionData(0,2);
    initialState.pos = newTraj.rbegin()->data[0]->pos;
    initialState.vel = *newTraj.differentiateDiscretelyForDim(0, 0).rbegin();
    initialState.acc = *newAccs.rbegin();
    goal = traj2.rbegin()->data[0]->pos;
    SampledTrajectoryV2<_3rdOrderDMP> newTraj2 = dmp2.calculateTrajectory(timestamps2, DVec(1,goal), Vec<_3rdOrderDMP>(1,initialState), 0.0, 1);

    DVec fPosBefore = traj.getDimensionData(0,0);
    newTraj.addDataToDimension(0, newTraj2.getTimestamps(), newTraj2.getDimensionData(0));
    traj.addDataToDimension(0, traj2.getTimestamps(), traj2.getDimensionData(0));
    DVec mergedTimestamps = traj.getTimestamps();
    fPos = traj.getDimensionData(0, 0);
    // Plotting
    newTraj.plot(0,0, "plots/newTraj");
    newTraj.plot(0,1, "plots/newTraj_vel");
    plots.clear();
    plots.push_back(newTraj.getTimestamps());
    plots.push_back(traj.getDimensionData(0,0));
    plots.push_back(newTraj.getDimensionData(0,0));
    plotTogether(plots,"plots/trajComparison_%d.gp");

    plots.clear();
    plots.push_back(newTraj.getTimestamps());
    plots.push_back(traj.getDimensionData(0,1));
//    plots.push_back(dmp.getTrainingTraj(0).differentiateDiscretelyForDim(0,0));
//    plots.push_back(dmp2.getTrainingTraj(0).differentiateDiscretelyForDim(0,0));
    plots.push_back(newTraj.differentiateDiscretelyForDim(0,0));
    plotTogether(plots,"plots/trajComparison_vel_%d.gp");

    plots.clear();
    plots.push_back(newTraj.getTimestamps());
//    plots.push_back(dmp.getTrainingTraj(0).differentiateDiscretelyForDim(0,1));
//    plots.push_back(dmp2.getTrainingTraj(0).differentiateDiscretelyForDim(0,1));
    plots.push_back(traj.differentiateDiscretely(traj.getTimestamps(),traj.getDimensionData(0,0), 2));
    plots.push_back(newTraj.differentiateDiscretely(newTraj.getTimestamps(),newTraj.getDimensionData(0,0), 2));
    plotTogether(plots,"plots/trajComparison_acc_%d.gp");

    plots.clear();
    plots.push_back(newTraj.getTimestamps());
    plots.push_back(dmp.getTrainingTraj(0).differentiateDiscretely(dmp.getTrainingTraj(0).getTimestamps(),dmp.getTrainingTraj(0).getDimensionData(0,0),3));
//    plots.push_back(dmp2.getTrainingTraj(0).differentiateDiscretelyForDim(0,1));
//    plots.push_back(newTraj.differentiateDiscretely(newTraj.getTimestamps(),newTraj.getDimensionData(0,0),3));
//    plotTogether(plots,"plots/trajComparison_jerk_%d.gp");


    QCoreApplication a(argc, argv);
}


