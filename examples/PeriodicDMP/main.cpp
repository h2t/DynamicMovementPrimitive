/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>
#include <dmp/representation/dmp/periodicdmp.h>
#include <boost/archive/xml_oarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>

using namespace DMP;


int main(int argc, char* argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
    if(res)
    {
        throw std::logic_error{"Killing all plot windows returned " + std::to_string(res)};
    }
    res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }
    Vec<DVec> plots;

    // Load the trajectory
    SampledTrajectoryV2 traj;
    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/SimplePeriodicMovement.csv";
//    std::string path = "periodicWiping.csv";

    traj.readFromCSVFile(path);
    traj.gaussianFilter(0.01);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);


    // train the DMP
    DMPInterfacePtr dmp;
    dmp.reset(new PeriodicDMP());
    dmp->learnFromTrajectory(traj);

    DMPInterface* dmpPtr = dmp.get();
    std::ofstream ofs("PeriodicDMP.xml");
    boost::archive::xml_oarchive ar(ofs);
    ar << boost::serialization::make_nvp("dmp",dmpPtr);
    ofs.close();

    //
    DMP::DVec2d initialState;
    DMP::DVec goals;
    for(size_t i = 0; i < traj.dim(); i++)
    {
        DMP::DVec state;
        state.push_back(traj.begin()->getPosition(i));
        state.push_back(traj.begin()->getDeriv(i,1));
        initialState.push_back(state);

  //      goals.push_back(traj.rbegin()->getPosition(i));
        goals.push_back(0);
    }
    DMP::DVec timestamps = DMP::SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj.getTimestamps().size()));
    DMP::SampledTrajectoryV2 newTraj = dmp->calculateTrajectory(timestamps, goals, initialState, 0.0, 1);

    plots.clear();
    plots.push_back(timestamps);
    plots.push_back(traj.getDimensionData(1, 0));
    plots.push_back(newTraj.getDimensionData(1, 0));
    plotTogether(plots, "plots/trajComparison_%d.gp");


    // test the DMP
//    double maxErrorOut;
//    dmp->evaluateReproduction(0.0, maxErrorOut);

    // store


}


