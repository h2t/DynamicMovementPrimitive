/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/dualquaterniondmp.h>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <Eigen/Core>
#include <Eigen/Geometry>


using namespace Eigen;
using namespace DMP;


int main(int argc, char* argv[])
{
    Vec<DVec> plots;

    // Load the trajectory
    DMP::SampledTrajectoryV2 qtraj;
    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/dualquaternion.csv";

    qtraj.readFromCSVFile(path);
    qtraj = SampledTrajectoryV2::normalizeTimestamps(qtraj, 0, 1);

    //test traj to qtraj transformation
//    SampledTrajectoryV2 qtraj = traj.traj2qtraj();
//    traj.plotAllDimensions(0, "/home/zhou/dmp/data/results/angletrajectory");
//    qtraj.plotAllDimensions(0, "/home/zhou/dmp/data/results/quattrajectory");


    // test QuaternionDMP
    DMP::DualQuaternionDMP qdmp;
    qdmp.learnFromTrajectory(qtraj);

    SampledTrajectoryV2 traj = qtraj.dqtraj2traj();

    Vec<DMPState> initialState;

    double rw = qtraj.begin()->getPosition(0);
    double rx = qtraj.begin()->getPosition(1);
    double ry = qtraj.begin()->getPosition(2);
    double rz = qtraj.begin()->getPosition(3);
    double dw = qtraj.begin()->getPosition(4);
    double dx = qtraj.begin()->getPosition(5);
    double dy = qtraj.begin()->getPosition(6);
    double dz = qtraj.begin()->getPosition(7);

    initialState.push_back(DMPState(rw, 0));
    initialState.push_back(DMPState(rx, 0));
    initialState.push_back(DMPState(ry, 0));
    initialState.push_back(DMPState(rz, 0));
    initialState.push_back(DMPState(dw, 0));
    initialState.push_back(DMPState(dx, 0));
    initialState.push_back(DMPState(dy, 0));
    initialState.push_back(DMPState(dz, 0));

    DVec goal;

    goal.push_back(qtraj.rbegin()->getPosition(0));
    goal.push_back(qtraj.rbegin()->getPosition(1));
    goal.push_back(qtraj.rbegin()->getPosition(2));
    goal.push_back(qtraj.rbegin()->getPosition(3));
    goal.push_back(qtraj.rbegin()->getPosition(4));
    goal.push_back(qtraj.rbegin()->getPosition(5));
    goal.push_back(qtraj.rbegin()->getPosition(6));
    goal.push_back(qtraj.rbegin()->getPosition(7));

    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj.getTimestamps().size()));

    SampledTrajectoryV2 newqTraj = qdmp.calculateTrajectory(timestamps, goal, initialState, 1.0, 1);

    SampledTrajectoryV2 newaTraj = newqTraj.dqtraj2traj();

    std::string dir = DATA_DIR;
    std::string curpath = dir + "/results/oldDQtrajectory";
    qtraj.plotAllDimensions(0, curpath);

    curpath = dir + "/results/newDQtrajectory";
    newqTraj.plotAllDimensions(0, curpath);

    curpath = dir + "/results/oldDAtrajectory";
    traj.plotAllDimensions(0, curpath);

    curpath = dir + "/results/newDAtrajectory";
    newaTraj.plotAllDimensions(0, curpath);

    curpath = dir + "/results/vel";
    dlmwrite(curpath, qdmp.vels_tr);
}




