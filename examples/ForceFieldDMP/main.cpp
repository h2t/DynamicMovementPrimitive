/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <representation/dmp/endveloforcefielddmp.h>
#include <dmp/math/pca.h>
#include <io/MMMConverter.h>
#include <MMM/Motion/MotionReaderXML.h>

using namespace DMP;

void PCA2(const Eigen::MatrixXd dataPoints)
{
    using namespace Eigen;
    size_t bestNDims = 3;
    MatrixXd dataPointsT = dataPoints;
    VectorXd meanVec = dataPointsT.rowwise().mean();
    dataPointsT = dataPointsT.colwise() - meanVec;
//    std::cout << "PCA2:datapoints:\n " << dataPointsT << std::endl;
    MatrixXd covariance = dataPointsT * dataPointsT.adjoint();
    SelfAdjointEigenSolver<MatrixXd> eig(covariance, Eigen::EigenvaluesOnly);

//    std::cout << eig.eigenvectors().rightCols(bestNDims) << std::endl;
    std::cout << eig.eigenvalues() << std::endl;
}

void PCAOld(Eigen::MatrixXd dataPoints)
{
    using namespace Eigen;

    unsigned int dimSpace = 3; // dimension space
    unsigned int m = dataPoints.rows();   // dimension of each point
    unsigned int n = dataPoints.cols();  // number of points

    typedef std::pair<double, int> myPair;
    typedef std::vector<myPair> PermutationIndices;


    // substract centroid of all data points
    VectorXd meanVec = dataPoints.rowwise().mean();
    dataPoints = dataPoints.colwise() - meanVec;

//    std::cout << DataPoints << std::endl;

    // get the covariance matrix
    MatrixXd covariance = dataPoints * dataPoints.transpose();
  //  std::cout << Covariance ;

    // compute the eigenvalue on the Cov Matrix
    EigenSolver<MatrixXd> solver(covariance, false);
    VectorXd eigenvalues = solver.eigenvalues().real();

//    MatrixXd eigenVectors = MatrixXd::Zero(n, m);  // matrix (n x m) (points, dims)
//    eigenVectors = solver.eigenvectors().real();

    // sort and get the permutation indices
    PermutationIndices pi;
    for (int i = 0 ; i < m; i++)
        pi.push_back(std::make_pair(eigenvalues(i), i));

    sort(pi.begin(), pi.end());

    for (unsigned int i = 0; i < m ; i++)
        std::cout << "eigen=" << pi[i].first << " pi=" << pi[i].second << std::endl;

//    // consider the subspace corresponding to the top-k eigenvectors
//    unsigned int i = pi.size()-1;
//    unsigned int howMany = i - dimSpace;
//    for (; i > howMany; i--)
//    {
//        std::cout << pi.at(i).second << "-eigenvector " << eigenVectors.row(pi.at(i).second) << "\n";
//    }
}

template <typename dmpType>
void testTrajectory(dmpType& dmp, std::string trajectoryFilePath, double startOffset, double goalOffset, double temporalFactor)
{
    SampledTrajectoryV2 traj;
    if(trajectoryFilePath.find(".xml") != std::string::npos)
    {
        MMM::MotionReaderXML reader;
        MMM::MotionPtr motion = reader.loadMotion(trajectoryFilePath, "greenwhisk");
        traj = MMMConverter::fromMMM(motion);

//        std::cout << "mat:\n" << mat << std::endl;
//        traj.plot(0,0);
//        traj.plot(0,2, "plots/trajectory_acc");
        traj.gaussianFilter(0.1);
        Eigen::MatrixXd mat = traj./*getPart(0, 1).*/toEigen();
//        traj.plot(0,0, "plots/trajectory_gauss_filtered");
//        traj.plot(0,2, "plots/trajectory_gauss_filtered_acc");
        traj.removeDimension(6);
        traj.removeDimension(5);
        traj.removeDimension(4);
        traj.removeDimension(3);
        traj.removeDimension(2);
        traj.removeDimension(1);
    }
    else
    {
        traj.readFromCSVFile(trajectoryFilePath);
        traj.gaussianFilter(10);
        traj.removeDimension(1);
    }
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    dmp.setFunctionApproximator(FunctionApproximationInterfacePtr(new ExactReproduction()));
    dmp.learnFromTrajectories(traj);

    DMPState initialState;
    initialState.pos = traj.begin()->getPosition(0) + startOffset;
    initialState.vel = traj.begin()->getDeriv(0, 1) ;
    double goal = traj.rbegin()->getPosition(0) + goalOffset;
    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj.getTimestamps().size()));

    SampledTrajectoryV2 newTraj = dmp.calculateTrajectory(timestamps, DVec(1, goal), Vec<DMPState>(1, initialState), 0.0, temporalFactor);
    std::cout << " Goal: " << goal << " reached position: " << newTraj.getState(1.0,0) << std::endl;
    Vec<DVec> plots;
    plots.clear();
    plots.push_back(timestamps);
    plots.push_back(traj.getDimensionData(0, 0));
    plots.push_back(newTraj.getDimensionData(0, 0));
    plotTogether(plots, "plots/trajComparison_%d.gp");
}




int main(int argc, char* argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
    if(res)
    {
        throw std::logic_error{"Killing all plot windows returned " + std::to_string(res)};
    }
    res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }
    Vec<DVec> plots;
    // Load the trajectory
    SampledTrajectoryV2 traj;
    std::string path = DATA_DIR;
//    path += "/ExampleTrajectories/sampletraj.csv";
//    path += "/ExampleTrajectories/pouring_x_short.csv";
//    path += "/ExampleTrajectories/CosMotion.csv";

//    traj.readFromCSVFile(path);
//    traj.removeDimension(1);
//    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);





    // train the DMP
    // No Mirroring, No unreasonably large amplitudes
    EndVeloForceFieldDMP dmp;
    dmp.setShowPlots(true);
//    ForceFieldDMP dmp; //!!! need to change initial canonical value from 1 to 0 for SimpleEndVeloDMP!!!
//    BasicDMP dmp;
    dmp.setFunctionApproximator(FunctionApproximationInterfacePtr(new ExactReproduction()));
//    dmp.learnFromTrajectories(traj);
//    testTrajectory(dmp, "/home/waechter/projects/MMMTools/data/Motions/Trial12_whisk_lying_MMM.xml", 0, 0, 1);
    testTrajectory(dmp, path + "/ExampleTrajectories/sampletraj.csv", 0, 0, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/sampletraj.csv", 0, 84, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/sampletraj.csv", 50, -300, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/pouring_x_short.csv", 0, -326.549, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/pouring_x_short.csv", 0, 326.549, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/CosMotion.csv", 50, 30, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/CosMotion.csv", 0, -30, 1);
    return 0;




    // Reproduction

//        double maxError;
//        double error = dmp.evaluateReproduction(0,maxError);
//        std::cout << "Mean relative reproduction error: " << error << " max error: " << maxError << std::endl;

    // Configuration
    DMPState initialState;
    initialState.pos = traj.begin()->getPosition(0) ;
    initialState.vel = traj.begin()->getDeriv(0, 1) ;
    double goal = traj.rbegin()->getPosition(0) + 300;
    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 2.0, 1.0 / (-0.5 + traj.getTimestamps().size()));

    SampledTrajectoryV2 newTraj = dmp.calculateTrajectory(timestamps, DVec(1, goal), Vec<DMPState>(1, initialState), 0.0, 1);
    std::cout << " Goal: " << goal << " reached position: " << newTraj.getState(1.0,0) << std::endl;
    plots.clear();
    plots.push_back(timestamps);
    plots.push_back(traj.getDimensionData(0, 0));
    plots.push_back(newTraj.getDimensionData(0, 0));
    plotTogether(plots, "plots/trajComparison_%d.gp");
//    plots.clear();
//    plots.push_back(timestamps);
//    plots.push_back(dmp.getTrainingTraj(0).getDimensionData(0, 1));
//    plots.push_back(newTraj.getDimensionData(0, 1));
//    plotTogether(plots, "plots/trajComparison_vel_%d.gp");
//    plots.clear();
//    plots.push_back(timestamps);
//    plots.push_back(dmp.getTrainingTraj(0).getDimensionData(0, 2));
//    plots.push_back(newTraj.getDimensionData(0, 2));
//    plotTogether(plots, "plots/trajComparison_acc_%d.gp");

}



