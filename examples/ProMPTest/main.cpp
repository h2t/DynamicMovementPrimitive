#include <fstream>

#include <boost/unordered_map.hpp>

#include <sys/time.h>

#include <iostream>

#include "dmp/representation/dmp/promp.h"

using namespace DMP;

int main(int argc, char* argv[])
{
//    int res = KILL_ALL_PLOT_WINDOWS;
//    res = system("mkdir plots");
    Vec<DVec> plots;

    // Load the trajectory
    SampledTrajectoryV2 traj0;
    std::string path0 = DATA_DIR;
    path0 += "/ExampleTrajectories/sampletraj.csv";
    traj0.readFromCSVFile(path0);
    traj0 = SampledTrajectoryV2::normalizeTimestamps(traj0, 0, 1);
    traj0.gaussianFilter(0.005);


    Vec<SampledTrajectoryV2 > trajs;
    trajs.push_back(traj0);

    ProMP promp(100);
    promp.learnFromTrajectories(trajs);
    std::cout << "learned promp" << std::endl;
    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj0.getTimestamps().size()));

    DVec2d sampledWeights = promp.sampleWeight();
    std::cout << "sampled promp" << std::endl;

    SampledTrajectoryV2 newTraj = promp.generateTrajectory(timestamps, sampledWeights);

    for(size_t i = 0; i < traj0.dim(); i++)
    {
        plots.clear();
        plots.push_back(traj0.getTimestamps());
        plots.push_back(traj0.getDimensionData(i, 0));
        plots.push_back(newTraj.getDimensionData(i, 0));
        plotTogether(plots, "plots/trajComparison_%d.gp");
    }


}


