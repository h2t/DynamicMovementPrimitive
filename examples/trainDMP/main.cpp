/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include <boost/unordered_map.hpp>

#include <MMM/Motion/MotionReaderXML.h>
#include <dmp/io/MMMConverter.h>

#include <dmp/representation/dmpfactory.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/RuntimeEnvironment.h>

#include <MMMSimoxTools/MMMSimoxTools.h>
#include <VirtualRobot/EndEffector/EndEffector.h>
#include <database/database.h>

#include <boost/cast.hpp>

#include <iostream>
#include <string>

using namespace DMP;

struct trainDMPConfiguration
{
    trainDMPConfiguration()
    {
        motionID = 0;
        DMPType = "BasicDMP";
        DMPOutputFile = std::string("dmp_output.xml");
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("motion");
        VirtualRobot::RuntimeEnvironment::considerKey("trajectory");
        VirtualRobot::RuntimeEnvironment::considerKey("DMPType");
        VirtualRobot::RuntimeEnvironment::considerKey("DMPOutputFile");
        VirtualRobot::RuntimeEnvironment::considerKey("motionID");


        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);

        if (VirtualRobot::RuntimeEnvironment::hasValue("motion"))
            motionFile = VirtualRobot::RuntimeEnvironment::getValue("motion");

        if (VirtualRobot::RuntimeEnvironment::hasValue("trajectory"))
            trajFile = VirtualRobot::RuntimeEnvironment::getValue("trajectory");

        if (VirtualRobot::RuntimeEnvironment::hasValue("DMPType"))
            DMPType = VirtualRobot::RuntimeEnvironment::getValue("DMPType");

        if (VirtualRobot::RuntimeEnvironment::hasValue("DMPOutputFile"))
            DMPOutputFile = VirtualRobot::RuntimeEnvironment::getValue("DMPOutputFile");

        if (VirtualRobot::RuntimeEnvironment::hasValue("motionID"))
        {
            std::string id = VirtualRobot::RuntimeEnvironment::getValue("motionID");
            motionID = std::atoi(id.c_str());
        }

        if (!VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(motionFile)
           && !VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(trajFile))
        {
            MMM_ERROR << "Could not find MMM motion file or trajectory file" << std::endl;
            return false;
        }
        return true;

    }

    int motionID;
    std::string motionFile;
    std::string DMPType;
    std::string DMPOutputFile;
    std::string trajFile;
};


int main(int argc, char* argv[])
{

    cout << " --- trainDMP --- " << endl;
    if ((argc==2) && (std::string(argv[1]).compare("--help") == 0) )
    {
        std::cout << "Use the following syntax: " << std::endl;
        std::cout << "trainDMP --<parameter> <value>" << std::endl
                  << "where" << std::endl
                  << "<parameter> \t <value>" << std::endl
                  << "motion \t\tXML file containing the MMM motion" << std::endl
                  << "DMPType \t\tDMP type" << std::endl
                  << "DMPOutputFile \t\tXML file containing output dmp" << std::endl;
        return 0;
    }

    trainDMPConfiguration c;
    if (!c.processCommandLine(argc,argv))
    {
        cout << "Error while processing command line, aborting..." << endl;
        return -1;
    }

    // specify dmp
    DMPFactory dmpFactory;
    DMPInterfacePtr dmp = dmpFactory.getDMP(c.DMPType,500);

    // load motion or trajectory
    SampledTrajectoryV2 trainingTraj;

    if (VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(c.motionFile))
    {
        cout << "Use motion file " << c.motionFile << endl;

        MMM::MotionReaderXML reader;
        MMM::MotionList motions = reader.loadAllMotions(c.motionFile);
        MMM::MotionPtr motion = motions[c.motionID];
        trainingTraj = MMMConverter::fromMMMJoints(motion);

        std::vector<std::string> jointNames = motion->getJointNames();
        Eigen::VectorXf jointValues = motion->getMotionFrame(motion->getNumFrames()-1)->joint;
        Eigen::VectorXf jointValues0 = motion->getMotionFrame(0)->joint;

        ofstream ofjn("jointNames");
        ofstream ofjv("jointGoals");
        ofstream ofjv0("jointStarts");
        for(size_t i = 0; i < jointNames.size(); ++i)
        {
            ofjn << "\""<< jointNames[i] <<"\""<< ",\n";
            ofjv <<  "\""<< jointNames[i] <<"\"" << ":" << jointValues(i) << ",\n";
            ofjv0 <<  "\""<< jointNames[i] <<"\"" << ":" << jointValues0(i) << ",\n";
        }
        ofjn.close();
        ofjv.close();
        ofjv0.close();
    }
    else
    {
        cout << "Use trajectory file " << c.trajFile << endl;

        trainingTraj.readFromCSVFile(c.trajFile);
    }

    trainingTraj = SampledTrajectoryV2::normalizeTimestamps(trainingTraj, 0, 1);

    // specify the anchor point
    DVec goal;
    double initCanonicalValue;
    if(c.DMPType == "PeriodicDMP")
    {
        goal.clear();
        std::cout << "You need to specify the anchor point for each trajectory" << std::endl;
        for(size_t i = 0; i < trainingTraj.dim(); i++)
        {
            double apoint;
            std::cin >> apoint;
            goal.push_back(apoint);
        }

        dmp->setGoals(goal);
        initCanonicalValue = 0.0;
    }
    else
    {
        initCanonicalValue = 1.0;
    }
    // train DMP
    dmp->learnFromTrajectory(trainingTraj);

    trainingTraj.writeTrajToCSVFile("pushsoft01.csv");
    // store DMP
    DMPInterface* dmpPtr = dmp.get();
    ofstream ofs(c.DMPOutputFile);
    boost::archive::xml_oarchive ar(ofs);
    ar << boost::serialization::make_nvp("dmp", dmpPtr);
    ofs.close();


    ofstream ofs1("fa_pushsoft02.xml");
    boost::archive::xml_oarchive ar1(ofs1);
    std::vector<FunctionApproximationInterface*> approximators;
    for(int i = 0; i < trainingTraj.dim(); i++)
    {
        approximators.push_back(dmp->getFunctionApproximatorPtr(i).get());
    }
    ar1 << boost::serialization::make_nvp("funcAppList", approximators);
    ofs1.close();


//    ifstream ifs(c.DMPOutputFile);
//    boost::archive::xml_iarchive ari(ifs);
//    std::vector<FunctionApproximationInterface*> appIns;

//    ari >> boost::serialization::make_nvp("funcAppList", appIns);
//    ifs.close();

//    std::cout << appIns.size() << std::endl;

    return 1;
}

