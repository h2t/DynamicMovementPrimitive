#include <fstream>

#include <boost/unordered_map.hpp>

#include <sys/time.h>

#include <iostream>

#include "dmp/representation/dmp/umitsmp.h"
#include "dmp/representation/dmp/umidmp.h"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
using namespace DMP;


int main(int argc, char* argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
    res = system("mkdir plots");
    Vec<DVec> plots;

    // Load the trajectory
    SampledTrajectoryV2 traj0;
    std::string path0 = DATA_DIR;

    int kernel_num = 100;
    if(argc < 2)
    {
        path0 += "/ExampleTrajectories/joint-trajectory-throwball.csv";

    }
    else
    {
        path0 += argv[1];

        if (argc >= 3)
        {
            kernel_num = atoi(argv[2]);
        }
    }

    traj0.readFromCSVFile(path0);
    traj0 = SampledTrajectoryV2::normalizeTimestamps(traj0, 0, 1);
    traj0.gaussianFilter(0.005);


    Vec<SampledTrajectoryV2 > trajs;
    trajs.push_back(traj0);

    // train the DMP
    UMIDMPPtr dmp(new UMIDMP(kernel_num));
    Vec<DMPState> initialState;
    DVec goals;

    for(size_t i = 0; i < traj0.dim(); i++)
    {
        DMPState state;
        state.pos = traj0.begin()->getPosition(i) ;
        state.vel = 0;
        initialState.push_back(state);
        goals.push_back(traj0.rbegin()->getPosition(i));
    }

    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj0.getTimestamps().size()));

    dmp->learnFromTrajectories(trajs);
    dmp->prepareExecution(goals, initialState, 1, 1);
    SampledTrajectoryV2 newTraj1 = dmp->calculateTrajectory(timestamps, goals, initialState, 1.0, 1.0);

    // test serialization
    std::stringstream ss;
    boost::archive::text_oarchive oa{ss};
    oa << dmp.get();
    UMIDMP* dmpPtr;
    boost::archive::text_iarchive ia{ss};
    ia >> dmpPtr;
    UMIDMPPtr dmp2(dmpPtr);
    dmp2->prepareExecution(goals, initialState, 1, 1);
    SampledTrajectoryV2 newTraj2 = dmp2->calculateTrajectory(timestamps, goals, initialState, 1.0, 1.0);


    // test weights
    UMIDMP dmp3(kernel_num);
    std::cout << dmp->getWeights();
    dmp3.setupWithWeights(dmp->getWeightsVec2d());
    dmp3.prepareExecution(goals, initialState, 1, 1);
    SampledTrajectoryV2 newTraj3 = dmp3.calculateTrajectory(timestamps, goals, initialState, 1.0, 1.0);

    for(size_t i = 0; i < traj0.dim(); i++)
    {
        plots.clear();
        plots.push_back(traj0.getTimestamps());

        for(size_t j = 0; j < trajs.size(); ++j)
        {
            plots.push_back(trajs[j].getDimensionData(i, 0));
        }
        plots.push_back(newTraj1.getDimensionData(i, 0));
        plots.push_back(newTraj2.getDimensionData(i, 0));
        plots.push_back(newTraj3.getDimensionData(i, 0));
        plotTogether(plots, "plots/trajComparison_%d.gp");
    }


}


