#include <fstream>

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

#include <boost/unordered_map.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <sys/time.h>

#include <iostream>

#include "dmp/representation/dmp/umitsmp.h"
#include "dmp/representation/dmp/umidmp.h"

#include "dmp/representation/dmpfactory.h"
#include "dmp/representation/dmpconfig.h"
#include <armadillo>

using namespace DMP;

using namespace boost::filesystem;

Vec<SampledTrajectoryV2 > getTrajs(const std::string& filepath)
{
    path datapath(filepath);
    Vec<SampledTrajectoryV2 > trajs;
    if(is_directory(datapath))
    {
        for(auto& entry : boost::make_iterator_range(directory_iterator(datapath), {}))
        {
            std::string filepath = entry.path().string();
            SampledTrajectoryV2 traj0;
            traj0.readFromCSVFile(filepath);
            traj0 = SampledTrajectoryV2::normalizeTimestamps(traj0, 0, 1);
            traj0.gaussianFilter(0.005);
            trajs.push_back(traj0);
        }
    }
    else
    {
        SampledTrajectoryV2 traj0;
        traj0.readFromCSVFile(filepath);
        traj0 = SampledTrajectoryV2::normalizeTimestamps(traj0, 0, 1);
        traj0.gaussianFilter(0.005);
        trajs.push_back(traj0);
    }

    return trajs;
}

int main(int argc, char* argv[])
{
    arma::arma_version ver;
    std::cout << "ARMA version: " << ver.as_string() << std::endl;

    int res = KILL_ALL_PLOT_WINDOWS;
    res = system("mkdir plots");
    Vec<DVec> plots;

    std::string path0 = DATA_DIR;
    path0 += "/ExampleTrajectories/pickplace7d.csv";
    Vec<SampledTrajectoryV2 > trajs = getTrajs(path0);

    // train the DMP
//UMITSMPPtr dmp(new UMITSMP(100,1));

    DMPFactory dmpFactory;
    DMPInterfacePtr dmp = dmpFactory.getDMP("UMITSMP", 100);

    Vec<DMPState> initialState;
    DVec goals;

    SampledTrajectoryV2 traj0 = trajs[0];
    for(size_t i = 0; i < traj0.dim(); i++)
    {
        DMPState state;
        state.pos = traj0.begin()->getPosition(i) ;
        state.vel = 0;
        initialState.push_back(state);
        goals.push_back(traj0.rbegin()->getPosition(i));
    }

    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj0.getTimestamps().size()));
    dmp->learnFromTrajectories(trajs);
    dmp->save("test_dmp.xml");


    MPConfig mpConfig(dmp);
    mpConfig.save("test_dmp_config.xml");

    MPConfig mpConfig2;
    mpConfig2.load("test_dmp_config.xml");


    // Test serialization
    DMPInterfacePtr dmp2 = dmpFactory.getDMP("UMITSMP");
    dmp2->load("test_dmp.xml");
    dmp2->prepareExecution(mpConfig2.goal, mpConfig2.initialState, 1.0, 1.0);
    SampledTrajectoryV2 newTraj = dmp2->calculateTrajectory(timestamps, goals, initialState, 1.0, 1.0);

    for(size_t i = 0; i < 7; i++)
    {

        plots.clear();
        plots.push_back(traj0.getTimestamps());

        for(size_t j = 0; j < trajs.size(); ++j)
        {
            plots.push_back(trajs[j].getDimensionData(i, 0));
        }
        plots.push_back(newTraj.getDimensionData(i, 0));
        plotTogether(plots, "plots/trajComparison_%d.gp");
    }


}


